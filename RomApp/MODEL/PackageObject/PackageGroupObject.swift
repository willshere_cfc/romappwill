//
//  PackageGroupObject.swift
//  RomApp
//
//  Created by Chanachon Pruchayahmaytha on 1/31/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit
import SwiftyJSON

class GetPackageGroupObject: NSObject {
    let getPackageGroupObject_status : String
    let getPackageGroupObject_message : String
    let getPackageGroupObject_responseCode : String
    let getPackageGroupObject_responseMessage : String
    let getPackageGroupObject_datetime : String
    let getPackageGroupObject_systemType : String
    let getPackageGroupObject_paymentMethod:String
    let getPackageGroupObject_networkType : String
    let getPackageGroupObject_category:JSON
    
    init(
        getpackagegroupobject_status:String,
        getpackagegroupobject_message:String,
        getpackagegroupobject_responseCode:String,
        getpackagegroupobject_responseMessage:String,
        getpackagegroupobject_datetime:String,
        getpackagegroupobject_systemType:String,
        getpackagegroupobject_paymentMethod:String,
        getpackagegroupobject_networkType:String,
        getpackagegroupobject_category:JSON
        ){
        self.getPackageGroupObject_status = getpackagegroupobject_status
        self.getPackageGroupObject_message = getpackagegroupobject_message
        self.getPackageGroupObject_responseCode = getpackagegroupobject_responseCode
        self.getPackageGroupObject_responseMessage = getpackagegroupobject_responseMessage
        self.getPackageGroupObject_datetime = getpackagegroupobject_datetime
        self.getPackageGroupObject_systemType = getpackagegroupobject_systemType
        self.getPackageGroupObject_paymentMethod = getpackagegroupobject_paymentMethod
        self.getPackageGroupObject_networkType = getpackagegroupobject_networkType
        self.getPackageGroupObject_category = getpackagegroupobject_category
    }
    
    required convenience init?(value:JSON){
        self.init(
            getpackagegroupobject_status:value["status"].stringValue,
            getpackagegroupobject_message:value["message"].stringValue,
            getpackagegroupobject_responseCode:value["response_code"].stringValue,
            getpackagegroupobject_responseMessage:value["response_message"].stringValue,
            getpackagegroupobject_datetime:value["datetime"].stringValue,
            getpackagegroupobject_systemType:value["systemtype"].stringValue,
            getpackagegroupobject_paymentMethod:value["paymentmode"].stringValue,
            getpackagegroupobject_networkType:value["networktype"].stringValue,
            getpackagegroupobject_category:value["package_category"]
        )
    }
}

class PackageGroupObject : NSObject{

    let packageGroupObject_id : String
    let packageGroupObject_code : String
    let packageGroupObject_name:String
    
    init(
        packagegroupobject_id:String,
        packagegroupobject_code:String,
        packagegroupobject_name:String
        ){
        self.packageGroupObject_id = packagegroupobject_id
        self.packageGroupObject_code = packagegroupobject_code
        self.packageGroupObject_name = packagegroupobject_name
    }
    
    required convenience init?(value:JSON){
        self.init(
            packagegroupobject_id:value["category_id"].stringValue,
            packagegroupobject_code:value["category_code"].stringValue,
            packagegroupobject_name:value["category_name"].stringValue
        )
    }
    
}
