//
//  UIAlertAction.swift
//  RomApp
//
//  Created by Administrator on 1/31/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import Foundation
import UIKit

extension UIAlertAction {
    
    static var cancel: UIAlertAction {
        return UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
    }
    class func sharePhoto(handler: ((UIAlertAction) -> Void)?) -> UIAlertAction {
        return UIAlertAction(title: "Share", style: .default, handler: handler)
    }
    
    class func cameraPhoto(handler: ((UIAlertAction) -> Void)?) -> UIAlertAction {
        return UIAlertAction(title: "Camera", style: .default, handler: handler)
    }
    
    class func libraryPhoto(handler: ((UIAlertAction) -> Void)?) -> UIAlertAction {
        return UIAlertAction(title: "Library", style: .default, handler: handler)
    }
    
}

extension UIAlertController {
    static func alertWithTitle(title: String, message: String, buttonTitle: String) -> UIAlertController {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        
        return alertController
    }
}
