//
//  PackageTransactionViewController.swift
//  RomApp
//
//  Created by Administrator on 1/5/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit

class PackageTransactionViewController: UIViewController {

    var sellPackageObj : SellPackageObject? = nil
    var packageListObj : PackageListObject? = nil
    var topupOfferedPackageObj : TopupOfferedPackageObject? = nil
    
    @IBOutlet weak var lbl_PhoneNumber: UILabel!
    
    @IBOutlet weak var lbl_Price: UILabel!
    
    @IBOutlet weak var lbl_Date: UILabel!
    
    @IBOutlet weak var lbl_Transaction: UILabel!
    
    var phoneNumber : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupUI()
        // Do any additional setup after loading the view.
    }
    
    func setupUI()
    {
        self.lbl_PhoneNumber.text = "\(self.phoneNumber!)"
        
        if self.packageListObj != nil
        {
           self.lbl_Price.text = "\(self.packageListObj!.packageListObject_price)"
        }
        else
        {
           self.lbl_Price.text = "\(self.topupOfferedPackageObj!.topupOfferedPackageObject_amount)"
        }
       
        self.lbl_Date.text = "\(self.sellPackageObj!.sellPackageObject_transaction_date)"
        self.lbl_Transaction.text = "\(self.sellPackageObj!.sellPackageObject_transaction_id)"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
        
    }
    

    // MARK: IBAction
    
    @IBAction func confirmDidSelect(_ sender: Any)
    {
         _ = navigationController?.popToRootViewController(animated: true)
    }


}
