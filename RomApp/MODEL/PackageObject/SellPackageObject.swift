//
//  SellPackageObject.swift
//  RomApp
//
//  Created by Chanachon Pruchayahmaytha on 2/1/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit
import SwiftyJSON

class SellPackageObject: NSObject {
    let sellPackageObject_status : String
    let sellPackageObject_message : String
    let sellPackageObject_responseCode : String
    let sellPackageObject_responseMessage:String
    let sellPackageObject_transaction_date : String
    let sellPackageObject_transaction_id : String
    
    init(sellpackageobject_status : String,
         sellpackageobject_message: String,
         sellpackageobject_responsecode : String,
         sellpackageobject_responsemessage : String,
         sellpackageobject_transaction_date:String,
         sellpackageobject_transaction_id:String){
        
        self.sellPackageObject_status = sellpackageobject_status
        self.sellPackageObject_message = sellpackageobject_message
        self.sellPackageObject_responseCode = sellpackageobject_responsecode
        self.sellPackageObject_responseMessage = sellpackageobject_responsemessage
        self.sellPackageObject_transaction_date = sellpackageobject_transaction_date
        self.sellPackageObject_transaction_id = sellpackageobject_transaction_id
    }
    
    required convenience init?(value:JSON){
        self.init(sellpackageobject_status:value["status"].stringValue,
                  sellpackageobject_message:value["message"].stringValue,
                  sellpackageobject_responsecode:value["response_code"].stringValue,
                  sellpackageobject_responsemessage:value["response_message"].stringValue,
                  sellpackageobject_transaction_date:value["datetime"].stringValue,
                  sellpackageobject_transaction_id:value["transactionid"].stringValue
        )
    }

}
