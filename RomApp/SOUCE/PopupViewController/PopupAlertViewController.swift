//
//  DemoPopupViewController1.swift
//  PopupController
//
//  Created by 佐藤 大輔 on 2/4/16.
//  Copyright © 2016 Daisuke Sato. All rights reserved.
//

import UIKit
import PopupController

class PopupAlertViewController : UIViewController, PopupContentViewController {
    
    var closeHandler: (() -> Void)?

    @IBOutlet weak var button: UIButton! {
        didSet {
            button.layer.borderColor = UIColor(red: 242/255, green: 105/255, blue: 100/255, alpha: 1.0).cgColor
            button.layer.borderWidth = 1.5
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.frame.size = CGSize(width: 300,height: 300)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    class func instance() -> PopupAlertViewController {
        let storyboard = UIStoryboard(name: "PopupAlertViewController", bundle: nil)
        return storyboard.instantiateInitialViewController() as! PopupAlertViewController
    }
    
    func sizeForPopup(_ popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSize(width: 300,height: 300)
    }
    
  
    @IBAction func didTapCloseButton(_ sender: AnyObject) {
        closeHandler?()
    }
    @IBAction func testDidSelect(_ sender: Any)
    {
         closeHandler?()
    }

    @IBAction func addContactDidSelect(_ sender: Any)
    {
        let storyboard = UIStoryboard(name: "TransferStoryboard", bundle: nil)
        let addContectNameViewController = storyboard.instantiateViewController(withIdentifier: "AddContectNameViewControllerID") as! AddContectNameViewController
        navigationController?.pushViewController(addContectNameViewController, animated: true)
        
        closeHandler?()
    }
    
   
    @IBAction func pullContactDidSelect(_ sender: Any)
    {
        let storyboard = UIStoryboard(name: "ContactStoryboard", bundle: nil)
        let contactPhoneBookViewController = storyboard.instantiateViewController(withIdentifier: "ContactPhoneBookViewControllerID") as! ContactPhoneBookViewController
        navigationController?.pushViewController(contactPhoneBookViewController, animated: true)
        
        closeHandler?()
    }
    
    @IBAction func cancalDidSelect(_ sender: Any)
    {
         closeHandler?()
    }
    
}
