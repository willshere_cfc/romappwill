//
//  RegisterAddressViewController.swift
//  RomApp
//
//  Created by Administrator on 1/17/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import JGProgressHUD
import CFAlertViewController

class RegisterAddressViewController: UIViewController {

    
    @IBOutlet weak var txt_Address_address: UITextField!
    
    @IBOutlet weak var txt_Address_road: UITextField!
    
    @IBOutlet weak var txt_Address_province: UITextField!
    
    @IBOutlet weak var txt_Address_district: UITextField!
    
    @IBOutlet weak var txt_Address_zone: UITextField!
    
    @IBOutlet weak var txt_Address_zipcode: UITextField!
    
    
    @IBOutlet weak var btn_CopyAddress: UIButton!
    
    @IBOutlet weak var txt_Live_address: UITextField!
    
    @IBOutlet weak var txt_Live_road: UITextField!
    
    @IBOutlet weak var txt_Live_province: UITextField!
    
    @IBOutlet weak var txt_Live_district: UITextField!
    
    @IBOutlet weak var txt_Live_zone: UITextField!
    
    @IBOutlet weak var txt_Live_zipcode: UITextField!
    
    var getProvinceObj : GetProvinceObject! = nil
    var getCityObj : GetCityObject! = nil
    
    var provinceObj : ProvinceObject!
    var cityObj : CityObject!
    
    
    var provinceArray : NSMutableArray = []
    var regioncodeArray : NSMutableArray = []
    var provincecodeArray : NSMutableArray = []
    
    var regioncodeLiveArray : NSMutableArray = []
    var provincecodeLiveArray : NSMutableArray = []
    
    var cityArray : NSMutableArray = []
    var districtArray : NSMutableArray = []
    
    var selectProvicePickerIndex: NSInteger = 0
    var selectCityPickerIndex: NSInteger = 0
    var selectdistrictPickerIndex: NSInteger = 0
    
    var selectProviceLivePickerIndex: NSInteger = 0
    var selectCityLivePickerIndex: NSInteger = 0
    var selectdistrictLivePickerIndex: NSInteger = 0
    
    var typeRegis : String!
    
    //MARK: - Pass Params
    var param_NameTH: String!
    var param_LastNameTH: String!
    var param_NameEN: String!
    var param_LastNameEN: String!
    var param_BirthDate: String!
    var param_CardID: String!
    var param_PhoneNumber: String!
    var param_Job: String!
    var param_Email: String! = ""
    
     var param_region_code: String!
    var param_province_code: String!
    
    var param_region_code_live: String!
    var param_province_code_live: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.requestServiceGetProvince()
        // Do any additional setup after loading the view.
    }
    
    //MARK: - Service
    
    func requestServiceGetProvince()
    {

//        print("params => \(parameters)")
        
        let strURL = "http://stg-etopupservices.ais.co.th/dev-ROMMobileServiceAPI/ROMService.svc/GetProvince/"
        
        JGProgressHUD.loadingProgress(view: self.view)
        AFWrapper.requestPOSTURL(strURL, params: nil, headers: nil, success: { (JSONResponse) ->  Void in
            
            JGProgressHUD.dismisProgress()
            
            print(JSONResponse)
            
            print("GetProvince => \(JSONResponse)")
            if JSONResponse.count != 0
            {
                 self.getProvinceObj = GetProvinceObject.init(value: JSONResponse)
               
                
                for i in 0...self.getProvinceObj!.getProvinceObject_province.count-1
                {
                      self.provinceObj = ProvinceObject.init(value: self.getProvinceObj!.getProvinceObject_province[i])
                     print("provin gg = > \(self.provinceObj!.provinceObject_province_name)")
                    self.provinceArray.add(self.provinceObj!.provinceObject_province_name)
                    self.regioncodeArray.add(self.provinceObj!.provinceObject_region_code)
                self.provincecodeArray.add(self.provinceObj!.provinceObject_province_short)
                    
                self.regioncodeLiveArray.add(self.provinceObj!.provinceObject_region_code)
              self.provincecodeLiveArray.add(self.provinceObj!.provinceObject_province_short)
                }
                
                print("provinceArray \(self.provincecodeArray)")
               
            }
            
            
        }) { (error) -> Void in
            print(error)
            JGProgressHUD.dismisProgress()
        }
    }

    func requestServiceGetCity(province_code:String)
    {
        
        let parameters: [String: Dictionary] =
            [
                "data":
                    [
                        "province_code": province_code
                    ]
        ]
        
                print("params provinceObj => \(parameters)")
        
        let strURL = "http://stg-etopupservices.ais.co.th/dev-ROMMobileServiceAPI/ROMService.svc/GetCity/"
        
        JGProgressHUD.loadingProgress(view: self.view)
        AFWrapper.requestPOSTURL(strURL, params: parameters as [String : AnyObject]?, headers: nil, success: { (JSONResponse) ->  Void in
            
            JGProgressHUD.dismisProgress()
            
            print(JSONResponse)
            
            print("GetProvince => \(JSONResponse)")
            if JSONResponse.count != 0
            {
                 self.getCityObj = GetCityObject.init(value: JSONResponse)
                
                
                for i in 0...self.getCityObj!.getCityObject_City.count-1
                {
                    let cityObj = CityObject.init(value: self.getCityObj!.getCityObject_City[i])
                    print("provin gg = > \(cityObj!.cityObject_city_name)")
                    self.cityArray.add(cityObj!.cityObject_city_name)
                    
                }
                
                print("provinceArray \(self.cityArray)")
            
            }
            
            
        }) { (error) -> Void in
            print(error)
            JGProgressHUD.dismisProgress()
        }
    }

    
    func requestServiceGetZone(province_code:String , city_code:String)
    {
        
        let parameters: [String: Dictionary] =
            [
                "data":
                    [
                        "province_code": province_code,
                        "city_code": city_code
                ]
        ]
        
        print("params GetDistrictAndPostcode => \(parameters)")
        
        let strURL = "http://stg-etopupservices.ais.co.th/dev-ROMMobileServiceAPI/ROMService.svc/GetDistrictAndPostcode/"
        
        JGProgressHUD.loadingProgress(view: self.view)
        AFWrapper.requestPOSTURL(strURL, params: parameters as [String : AnyObject]?, headers: nil, success: { (JSONResponse) ->  Void in
            
            JGProgressHUD.dismisProgress()
            
            print(JSONResponse)
            
            print("GetDistrictAndPostcode => \(JSONResponse)")
            if JSONResponse.count != 0
            {
                let getZoneObj = GetDistrictAndPostcodeObject.init(value: JSONResponse)
                
                
                for i in 0...getZoneObj!.getDistrictAndPostcodeObject_district.count-1
                {
                    let zoneObj = DistrictObject.init(value: getZoneObj!.getDistrictAndPostcodeObject_district[i])
                    print("districtArray gg = > \(zoneObj!.districtObject_tumbol_name)")
                    self.districtArray.add(zoneObj!.districtObject_tumbol_name)
                    
                }
                
                print("districtArray \(self.districtArray)")
                
            }
            
            
        }) { (error) -> Void in
            print(error)
            JGProgressHUD.dismisProgress()
        }
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - IBAction
    
    @IBAction func leftNavbarDidSelect(_ sender: Any)
    {
         _ = self.navigationController?.popViewController(animated: true)
    }

    @IBAction func confirmDidSelect(_ sender: Any)
    {
        if self.txt_Live_address.text?.length == 0 ||
        self.txt_Address_address.text?.length == 0 ||
        self.txt_Live_province.text?.length == 0 ||
        self.txt_Address_province.text?.length == 0 ||
        self.txt_Live_district.text?.length == 0 ||
        self.txt_Address_district.text?.length == 0 ||
        self.txt_Live_zone.text?.length == 0 ||
        self.txt_Address_zone.text?.length == 0 ||
        self.txt_Live_zipcode.text?.length == 0 ||
        self.txt_Address_zipcode.text?.length == 0
        {
            self.alertControllWith(title: "กรุณาใส่ข้อมูลให้ครบ", button: "ตกลง")
        }
        else
        {
           self.pushViewController()
        }
        
       
    }
    @IBAction func copyAddressDidSelect(_ sender: Any)
    {
        self.btn_CopyAddress.isSelected = !self.btn_CopyAddress.isSelected
        
        if(self.btn_CopyAddress.isSelected == true)
        {
         
            self.btn_CopyAddress.setBackgroundImage(UIImage(named:"checkbox-check"), for: .normal)
            
            self.txt_Live_address.text =  self.txt_Address_address.text
            self.txt_Live_road.text = self.txt_Address_road.text
            self.txt_Live_province.text = self.txt_Address_province.text
            self.txt_Live_district.text = self.txt_Address_district.text
            self.txt_Live_zone.text =  self.txt_Address_zone.text
            self.txt_Live_zipcode.text =  self.txt_Address_zipcode.text
            
              self.param_province_code_live = self.param_province_code
             self.param_region_code_live = self.param_region_code
        }
        else
        {
          
            self.btn_CopyAddress.setBackgroundImage(UIImage(named:"checkbox-uncheck"), for: .normal)
            
            self.txt_Live_address.text = ""
            self.txt_Live_road.text = ""
            self.txt_Live_province.text = ""
            self.txt_Live_district.text = ""
            self.txt_Live_zone.text = ""
            self.txt_Live_zipcode.text = ""
        }

    }
    
    //MARK: Push View Controller
    
    func pushViewController()
    {
        let storyboard = UIStoryboard(name: "RegisterStoryboard", bundle: nil)
        let registerCameraViewController = storyboard.instantiateViewController(withIdentifier: "RegisterCameraViewControllerID") as! RegisterCameraViewController
        
        registerCameraViewController.param_NameTH = self.param_NameTH
        registerCameraViewController.param_LastNameTH = self.param_LastNameTH
        registerCameraViewController.param_NameEN = self.param_NameEN
        registerCameraViewController.param_LastNameEN = self.param_LastNameEN
        registerCameraViewController.param_BirthDate = self.param_BirthDate
        registerCameraViewController.param_CardID = self.param_CardID
        registerCameraViewController.param_PhoneNumber = self.param_PhoneNumber
        registerCameraViewController.param_Job = self.param_Job
        registerCameraViewController.param_Email = self.param_Email
        registerCameraViewController.param_region_code = self.param_region_code
        registerCameraViewController.param_province_code = self.param_province_code
        registerCameraViewController.param_region_code_live = self.param_region_code_live
        registerCameraViewController.param_province_code_live = self.param_province_code_live
        
        registerCameraViewController.param_Address_address = self.txt_Address_address.text
        registerCameraViewController.param_Address_road = "\(self.txt_Address_road.text!)"
        registerCameraViewController.param_Address_province = self.txt_Address_province.text
        registerCameraViewController.param_Address_district = self.txt_Address_district.text
        registerCameraViewController.param_Address_zone = self.txt_Address_zone.text
        registerCameraViewController.param_Address_zipcode = self.txt_Address_zipcode.text
        
        registerCameraViewController.param_Live_address = self.txt_Live_address.text
        registerCameraViewController.param_Live_road = "\(self.txt_Live_road.text!)"
        registerCameraViewController.param_Live_province = self.txt_Live_province.text
        registerCameraViewController.param_Live_district = self.txt_Live_district.text
        registerCameraViewController.param_Live_zone = self.txt_Live_zone.text
        registerCameraViewController.param_Live_zipcode = self.txt_Live_zipcode.text

        registerCameraViewController.typeRegis = self.typeRegis
        
        navigationController?.pushViewController(registerCameraViewController, animated: true)
        

    }
    
    //MARK: - Alert
    func alertControllWith(title:String, button:String)
    {
        let alert = CFAlertViewController.alert(withTitle: title, message: nil, textAlignment: NSTextAlignment.center, preferredStyle: CFAlertControllerStyle.alert) {
            
            print("Dismiss")
            
        }
        
        let actionDefault = CFAlertAction.init(title: button, style: CFAlertActionStyle.default, alignment: CFAlertActionAlignment.justified, color: UIColor().hexStringToUIColor(hex: "#F7941D")) { action  in
            
            
        }
        
        alert .addAction(actionDefault!)
        
        self.present(alert, animated: true, completion: nil)
    }

    
    
    //MARK: - Picker
    //MARK: - Province
    @IBAction func provincePickerDidSelect(_ sender: Any)
    {
        let swiftArray = provinceArray as NSArray as! [String]
        ActionSheetStringPicker.show(withTitle: "จังหวัด", rows: swiftArray, initialSelection: self.selectProvicePickerIndex, target: self, successAction: #selector(RegisterAddressViewController.provinceWasSelected(selectedIndex:element:)), cancelAction: #selector(TopupSelectPriceViewController.canclePicker), origin: sender)
    }

    func provinceWasSelected(selectedIndex :NSNumber, element:Any)
    {
        self.selectProvicePickerIndex = selectedIndex.intValue
        
        self.txt_Address_province.text = "\(provinceArray[self.selectProvicePickerIndex])"
        self.param_region_code = "\(regioncodeArray[self.selectProvicePickerIndex])"
        self.param_province_code = "\(provincecodeArray[self.selectProvicePickerIndex])"
        
        print("province code \(self.param_province_code)")
        
        self.cityArray.removeAllObjects()
        self.districtArray.removeAllObjects()
        self.txt_Address_district.text = ""
        self.txt_Address_zone.text = ""
        self.provinceObj = ProvinceObject.init(value: getProvinceObj!.getProvinceObject_province[selectedIndex.intValue])
        
        self.requestServiceGetCity(province_code: self.provinceObj.provinceObject_province_code)
        
        self.selectCityPickerIndex = 0
        self.selectdistrictPickerIndex = 0
        
    }
    
    //MARK: - City
    @IBAction func cityWasPickerDidSelect(_ sender: Any)
    {
        let swiftArray = cityArray as NSArray as! [String]
        ActionSheetStringPicker.show(withTitle: "เขต/อำเภอ", rows: swiftArray, initialSelection: self.selectCityPickerIndex, target: self, successAction: #selector(RegisterAddressViewController.cityWasSelected(selectedIndex:element:)), cancelAction: #selector(TopupSelectPriceViewController.canclePicker), origin: sender)
    }
    
    func cityWasSelected(selectedIndex :NSNumber, element:Any)
    {
        self.selectCityPickerIndex = selectedIndex.intValue
        
        self.txt_Address_district.text = "\(cityArray[self.selectCityPickerIndex])"

        self.districtArray.removeAllObjects()
        self.txt_Address_zone.text = ""
        
        print("sele 1 \(selectProvicePickerIndex)")
                print("sele 2 \(selectCityPickerIndex)")
        
        self.provinceObj = ProvinceObject.init(value: getProvinceObj!.getProvinceObject_province[selectProvicePickerIndex])
        self.cityObj = CityObject.init(value: self.getCityObj.getCityObject_City[selectCityPickerIndex])
        self.requestServiceGetZone(province_code: self.provinceObj.provinceObject_province_code, city_code: self.cityObj.cityObject_city_code)
        
        self.selectdistrictPickerIndex = 0
        
    }
    
    //MARK: - Zipcode
    @IBAction func zipcodeWasPickerDidSelect(_ sender: Any)
    {
        let swiftArray = districtArray as NSArray as! [String]
        ActionSheetStringPicker.show(withTitle: "ตำบล", rows: swiftArray, initialSelection: self.selectdistrictPickerIndex, target: self, successAction: #selector(RegisterAddressViewController.districtWasSelected(selectedIndex:element:)), cancelAction: #selector(TopupSelectPriceViewController.canclePicker), origin: sender)
    }
    
    func districtWasSelected(selectedIndex :NSNumber, element:Any)
    {
        self.selectdistrictPickerIndex = selectedIndex.intValue
        
        self.txt_Address_zone.text = "\(districtArray[self.selectdistrictPickerIndex])"
     
    }
    
    //MARK: - Province Live
    
    @IBAction func provinceLiveWasPickerDidSelect(_ sender: Any)
    {
        
        let swiftArray = provinceArray as NSArray as! [String]
        ActionSheetStringPicker.show(withTitle: "จังหวัด", rows: swiftArray, initialSelection: self.selectProvicePickerIndex, target: self, successAction: #selector(RegisterAddressViewController.provinceLiveWasSelected(selectedIndex:element:)), cancelAction: #selector(TopupSelectPriceViewController.canclePicker), origin: sender)
    }
    
    func provinceLiveWasSelected(selectedIndex :NSNumber, element:Any)
    {
        self.selectProviceLivePickerIndex = selectedIndex.intValue
        
        self.txt_Live_province.text = "\(provinceArray[self.selectProviceLivePickerIndex])"
        
        self.param_region_code_live = "\(regioncodeLiveArray[self.selectProviceLivePickerIndex])"
        self.param_province_code_live = "\(provincecodeLiveArray[self.selectProviceLivePickerIndex])"
        
        self.cityArray.removeAllObjects()
        self.districtArray.removeAllObjects()
        self.txt_Live_district.text = ""
        self.txt_Live_zone.text = ""
        self.provinceObj = ProvinceObject.init(value: getProvinceObj!.getProvinceObject_province[selectedIndex.intValue])
        
        self.requestServiceGetCity(province_code: self.provinceObj.provinceObject_province_code)
        
        self.selectCityLivePickerIndex = 0
        self.selectdistrictLivePickerIndex = 0
        
    }

   
    
    //MARK: - City Live
    
    @IBAction func cityLiveWasPickerDidSelect(_ sender: Any)
    {
        
        let swiftArray = cityArray as NSArray as! [String]
        ActionSheetStringPicker.show(withTitle: "เขต/อำเภอ", rows: swiftArray, initialSelection: self.selectCityPickerIndex, target: self, successAction: #selector(RegisterAddressViewController.cityLiveWasSelected(selectedIndex:element:)), cancelAction: #selector(TopupSelectPriceViewController.canclePicker), origin: sender)
    }
    
    func cityLiveWasSelected(selectedIndex :NSNumber, element:Any)
    {
        self.selectCityLivePickerIndex = selectedIndex.intValue
        
        self.txt_Live_district.text = "\(cityArray[self.selectCityLivePickerIndex])"
        
        self.districtArray.removeAllObjects()
        self.txt_Live_zone.text = ""
        
        print("sele 1 \(selectProviceLivePickerIndex)")
        print("sele 2 \(selectCityLivePickerIndex)")
        
        self.provinceObj = ProvinceObject.init(value: getProvinceObj!.getProvinceObject_province[selectProviceLivePickerIndex])
        self.cityObj = CityObject.init(value: self.getCityObj.getCityObject_City[selectCityLivePickerIndex])
        self.requestServiceGetZone(province_code: self.provinceObj.provinceObject_province_code, city_code: self.cityObj.cityObject_city_code)
        
        self.selectdistrictLivePickerIndex = 0
        
    }

    
    
    //MARK: - District Live
    
    @IBAction func districtLiveWasPickerDidSelect(_ sender: Any)
    {
        
        let swiftArray = districtArray as NSArray as! [String]
        ActionSheetStringPicker.show(withTitle: "ตำบล", rows: swiftArray, initialSelection: self.selectdistrictPickerIndex, target: self, successAction: #selector(RegisterAddressViewController.districtLiveWasSelected(selectedIndex:element:)), cancelAction: #selector(TopupSelectPriceViewController.canclePicker), origin: sender)
    }
    
    func districtLiveWasSelected(selectedIndex :NSNumber, element:Any)
    {
        self.selectdistrictLivePickerIndex = selectedIndex.intValue
        
        self.txt_Live_zone.text = "\(districtArray[self.selectdistrictLivePickerIndex])"
        
    }

    func canclePicker()
    {
        
    }
}

//MARK: EXTENSION

//MARK: - UITextfield Delaget

extension RegisterAddressViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField .resignFirstResponder()
        
        return true
    }
}
