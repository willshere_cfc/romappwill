//
//  RegisterInputPhoneAndCardIDViewController.swift
//  RomApp
//
//  Created by Administrator on 1/14/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit
import JGProgressHUD
import CFAlertViewController

class RegisterInputPhoneAndCardIDViewController: UIViewController {

    @IBOutlet weak var btn_LeftNavbar: UIButton!
    
    
    @IBOutlet weak var txt_PhoneNumber: UITextField!
    
    @IBOutlet weak var txt_CardID: UITextField!
    
    var typeRegis : String! = "N"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        print("type string = > \(self.typeRegis)")
        // Do any additional setup after loading the view.
    }
    
    //MARK: - SERVICE
    
    func requestServicePreCheckRegis()
    {
        
        let parameters: [String: Dictionary] =
            [
                "data":
                    [
                        "mobilenumber": self.txt_PhoneNumber.text!,
                        "idcard": self.txt_CardID.text!
                ]
        ]
        
        print("params => \(parameters)")
        
        let strURL = "http://stg-etopupservices.ais.co.th/dev-ROMMobileServiceAPI/ROMService.svc/precheckreg/"
        
        JGProgressHUD.loadingProgress(view: self.view)
        AFWrapper.requestPOSTURL(strURL, params: parameters as [String : AnyObject]?, headers: nil, success: { (JSONResponse) ->  Void in
            
            JGProgressHUD.dismisProgress()
            
            print(JSONResponse)
            
            print("Pre Check Regis => \(JSONResponse)")
            if JSONResponse.count != 0
            {
                if JSONResponse["status"].stringValue == "success"
                {
                    self.pushViewController()
                }
                else
                {
                    self.alertControllWith(title: JSONResponse["message"].stringValue, button: "ตกลง")
                }

            }
            
            
        }) { (error) -> Void in
            print(error)
            JGProgressHUD.dismisProgress()
        }
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - IBAction
    
    @IBAction func leftNavbarDidSelect(_ sender: Any)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }

    @IBAction func confirmDidSelect(_ sender: Any)
    {
        if self.txt_PhoneNumber.text!.length > 0 && self.txt_CardID.text!.length > 0
        {
            if self.txt_PhoneNumber.text!.length < 10
            {
                self.alertControllWith(title: "หมายเลขโทรศัพท์ไม่ถูกต้อง", button: "ตกลง")
            }
            else
            {
                if checkIDCard(idCard: self.txt_CardID.text!)
                {
                    self.requestServicePreCheckRegis()
                    
                }
                else
                {
                    self.alertControllWith(title: "รหัสบัตรประชาชนของท่านผิดค่ะ", button: "ตกลง")
                }
            }

        }
        else
        {
            if self.txt_PhoneNumber.text!.length < 10
            {
                self.alertControllWith(title: "หมายเลขโทรศัพท์ไม่ถูกต้อง", button: "ตกลง")
            }
            else if self.txt_CardID.text!.length == 0
            {
                self.alertControllWith(title: "หมายเลขบัตรประชาชนไม่ถูกต้อง", button: "ตกลง")
            }
            else
            {
                self.alertControllWith(title: "กรุณากรอกข้อมูลให้ครบถ้วน", button: "ตกลง")
            }
            
        }
        
    }
    
    //MARK: - Check ID Card
    func checkIDCard(idCard:String) -> Bool
    {
        var sum_dig : Int = 0
        var flastCheckIdCheck : Bool = false
        
        if idCard.characters.count >= 13
        {
            var factor : Int = 13
            for i in idCard.characters {
                let someString = String(i)
                
                if let someInt = Int(someString) {
                    
                    if factor > 1
                    {
                        
                        
                        sum_dig += (someInt * factor)
                        factor -= 1
                        
                        
                    }
                    
                    
                }
                //print(i)
            }
            
            //322
            let mod_11 = sum_dig % 11
            let abs_min11 = abs(mod_11 - 11)
            let mod_10 : String = "\(abs_min11 % 10)"
            
            let lastChar = idCard.characters.last!
            let last_Dig  = "\(lastChar)"
            
            print(last_Dig)
            
            if mod_10 == last_Dig
            {
                flastCheckIdCheck = true
            }
            else
            {
                flastCheckIdCheck =  false
            }
        }
        
        return flastCheckIdCheck
    }

    
    //MARK: - Push VC
    func pushViewController()
    {
        let storyboard = UIStoryboard(name: "RegisterStoryboard", bundle: nil)
        let registerInfomationViewController = storyboard.instantiateViewController(withIdentifier: "RegisterInfomationViewControllerID") as! RegisterInfomationViewController
        
        registerInfomationViewController.phoneNumber = self.txt_PhoneNumber.text!
        registerInfomationViewController.idCard = self.txt_CardID.text!
        registerInfomationViewController.typeRegis = self.typeRegis
        navigationController?.pushViewController(registerInfomationViewController, animated: true)
    }
    
    //MARK: - Alert
    func alertControllWith(title:String, button:String)
    {
        let alert = CFAlertViewController.alert(withTitle: title, message: nil, textAlignment: NSTextAlignment.center, preferredStyle: CFAlertControllerStyle.alert) {
            
            print("Dismiss")
            
        }
        
        let actionDefault = CFAlertAction.init(title: button, style: CFAlertActionStyle.default, alignment: CFAlertActionAlignment.justified, color: UIColor().hexStringToUIColor(hex: "#F7941D")) { action  in
            
            
        }
        
        alert .addAction(actionDefault!)
        
        self.present(alert, animated: true, completion: nil)
    }

}


//MARK: - EXTENSION


//MARK: - Textfield Delegate

extension RegisterInputPhoneAndCardIDViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.txt_PhoneNumber .resignFirstResponder()
        self.txt_CardID .resignFirstResponder()
        
        return true
    }
}
