//
//  RegisterSuccessViewController.swift
//  RomApp
//
//  Created by Administrator on 1/17/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit

class RegisterSuccessViewController: UIViewController {

    @IBOutlet weak var textView_Detail: UITextView!
    
    var messageStr : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

         self.textView_Detail.font = UIFont(name: "DBHelvethaicaMonX-Med", size: 26)
        self.textView_Detail.text = self.messageStr
        
        self.textView_Detail.textAlignment = NSTextAlignment.justified
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func confirmDidSelect(_ sender: Any)
    {
         _ = self.navigationController?.popToRootViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
