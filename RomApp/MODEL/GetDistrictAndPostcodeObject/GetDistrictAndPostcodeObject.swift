//
//  GetDistrictAndPostcodeObject.swift
//  RomApp
//
//  Created by Phutsita Aphilertrungchut on 1/30/2560 BE.
//  Copyright © 2560 Willshere. All rights reserved.
//

import UIKit
import SwiftyJSON

class GetDistrictAndPostcodeObject: NSObject {
    let getDistrictAndPostcodeObject_status : String
    let getDistrictAndPostcodeObject_message : String
    let getDistrictAndPostcodeObject_response_code : String
    let getDistrictAndPostcodeObject_response_message : String
    let getDistrictAndPostcodeObject_datetime : String
    let getDistrictAndPostcodeObject_district : JSON
    
    init(getDistrictAndPostcodeObject_status : String,
         getDistrictAndPostcodeObject_message : String,
         getDistrictAndPostcodeObject_response_code : String,
         getDistrictAndPostcodeObject_response_message : String,
         getDistrictAndPostcodeObject_datetime : String,
         getDistrictAndPostcodeObject_district : JSON){
        self.getDistrictAndPostcodeObject_status = getDistrictAndPostcodeObject_status
        self.getDistrictAndPostcodeObject_message = getDistrictAndPostcodeObject_message
        self.getDistrictAndPostcodeObject_response_code = getDistrictAndPostcodeObject_response_code
        self.getDistrictAndPostcodeObject_response_message = getDistrictAndPostcodeObject_response_message
        self.getDistrictAndPostcodeObject_datetime = getDistrictAndPostcodeObject_datetime
        self.getDistrictAndPostcodeObject_district = getDistrictAndPostcodeObject_district
    }
    
    required convenience init?(value:JSON){
        self.init(getDistrictAndPostcodeObject_status : value["status"].stringValue,
                  getDistrictAndPostcodeObject_message : value["message"].stringValue,
                  getDistrictAndPostcodeObject_response_code : value["response_code"].stringValue,
                  getDistrictAndPostcodeObject_response_message : value["response_message"].stringValue,
                  getDistrictAndPostcodeObject_datetime : value["datetime"].stringValue,
                  getDistrictAndPostcodeObject_district : value["district"])
    }
}

class DistrictObject : NSObject{
    let districtObject_tumbol_code : String
    let districtObject_tumbol_name : String
    let districtObject_postal_code : String
    let districtObject_postal_value : String
    let districtObject_city_code : String
    let districtObject_province_code : String
    
    init(districtObject_tumbol_code : String,
         districtObject_tumbol_name : String,
         districtObject_postal_code : String,
         districtObject_postal_value : String,
         districtObject_city_code : String,
         districtObject_province_code : String){
        self.districtObject_tumbol_code = districtObject_tumbol_code
        self.districtObject_tumbol_name = districtObject_tumbol_name
        self.districtObject_postal_code = districtObject_postal_code
        self.districtObject_postal_value = districtObject_postal_value
        self.districtObject_city_code = districtObject_city_code
        self.districtObject_province_code = districtObject_province_code
    }
    
    required convenience init?(value:JSON){
        self.init(districtObject_tumbol_code : value["tumbol_code"].stringValue,
                  districtObject_tumbol_name : value["tumbol_name"].stringValue,
                  districtObject_postal_code : value["postal_code"].stringValue,
                  districtObject_postal_value : value["postal_value"].stringValue,
                  districtObject_city_code : value["city_code"].stringValue,
                  districtObject_province_code : value["province_code"].stringValue)
    }
}
