//
//  TopupStatusViewController.swift
//  RomApp
//
//  Created by Administrator on 2/8/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit

class TopupStatusViewController: UIViewController {

    @IBOutlet weak var lbl_Status: UILabel!
    
    var statusString : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.lbl_Status.text = "\(statusString)"
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func backDidSelect(_ sender: Any)
    {
         _ = self.navigationController?.popToRootViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
