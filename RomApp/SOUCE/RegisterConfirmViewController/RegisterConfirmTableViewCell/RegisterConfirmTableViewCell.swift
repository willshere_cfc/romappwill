//
//  RegisterConfirmTableViewCell.swift
//  RomApp
//
//  Created by Administrator on 1/17/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit

class RegisterConfirmTableViewCell: UITableViewCell {

    
    @IBOutlet weak var lbl_Title: UILabel!
    
    @IBOutlet weak var lbl_Detail: UILabel!
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    open override func awakeFromNib() {
    }
    
    open func setup() {
    }
    
    
    open func setTitleCell(title:String){
        
        self.lbl_Title?.text = title
        
    }
    
    open func setDetailCell(title:String){
        
        self.lbl_Detail?.text = " \(title)"
        
    }

}
