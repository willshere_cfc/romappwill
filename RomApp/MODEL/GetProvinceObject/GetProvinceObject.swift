//
//  GetProvinceObject.swift
//  RomApp
//
//  Created by Administrator on 1/29/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit
import SwiftyJSON

class GetProvinceObject: NSObject {

    let getProvinceObject_response_code : String
    let getProvinceObject_status : String
    let getProvinceObject_response_message : String
    let getProvinceObject_message : String
    let getProvinceObject_datetime : String
    let getProvinceObject_province : JSON
    
    init(getProvinceObject_response_code : String,
     getProvinceObject_status : String,
     getProvinceObject_response_message : String,
     getProvinceObject_message : String,
     getProvinceObject_datetime : String,
     getProvinceObject_province : JSON)
    {
        self.getProvinceObject_response_code = getProvinceObject_response_code
        self.getProvinceObject_status = getProvinceObject_status
        self.getProvinceObject_response_message = getProvinceObject_response_message
        self.getProvinceObject_message = getProvinceObject_message
        self.getProvinceObject_datetime = getProvinceObject_datetime
        self.getProvinceObject_province = getProvinceObject_province
    }
    
    required convenience init?(value:JSON) {
        
        self.init(getProvinceObject_response_code : value["response_code"].stringValue,
                  getProvinceObject_status : value["status"].stringValue,
                  getProvinceObject_response_message : value["response_message"].stringValue,
                  getProvinceObject_message : value["message"].stringValue,
                  getProvinceObject_datetime : value["datetime"].stringValue,
                  getProvinceObject_province : value["province"])
        
    }
}

class ProvinceObject: NSObject {
    
    let provinceObject_province_short : String
    let provinceObject_province_name : String
    let provinceObject_region_code : String
    let provinceObject_province_code : String
    let provinceObject_region_name : String
    
    init(provinceObject_province_short : String,
     provinceObject_province_name : String,
     provinceObject_region_code : String,
     provinceObject_province_code : String,
     provinceObject_region_name : String)
    {
        self.provinceObject_province_short = provinceObject_province_short
        self.provinceObject_province_name = provinceObject_province_name
        self.provinceObject_region_code = provinceObject_region_code
        self.provinceObject_province_code = provinceObject_province_code
        self.provinceObject_region_name = provinceObject_region_name
    }
    
    
    required convenience init?(value:JSON) {
        
        self.init(provinceObject_province_short : value["province_short"].stringValue,
                  provinceObject_province_name : value["province_name"].stringValue,
                  provinceObject_region_code : value["region_code"].stringValue,
                  provinceObject_province_code : value["province_code"].stringValue,
                  provinceObject_region_name : value["region_name"].stringValue)
    }

}



































