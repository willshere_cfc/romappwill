//
//  Date.swift
//  RomApp
//
//  Created by Administrator on 1/29/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import Foundation

extension DateFormatter {
    convenience init(dateStyle: DateFormatter.Style) {
        self.init()
        self.dateStyle = dateStyle
    }
    convenience init(timeStyle: DateFormatter.Style) {
        self.init()
        self.timeStyle = timeStyle
    }
    
}

extension Date {
    static let shortDate = DateFormatter(dateStyle: .short)
    static let fullDate = DateFormatter(dateStyle: .medium)
    
    static let shortTime = DateFormatter(timeStyle: .short)
    static let fullTime = DateFormatter(timeStyle: .full)
    
    var fullDate:  String { return Date.fullDate.string(from: self) }
    var shortDate: String { return Date.shortDate.string(from: self) }
    
    var fullTime:  String { return Date.fullTime.string(from: self) }
    var shortTime: String { return Date.shortTime.string(from: self) }
    
    var previousMonth: Int {
        let cal = NSCalendar.autoupdatingCurrent
        return cal.component(.month, from: cal.date(byAdding: .month, value: -3, to: NSDate() as Date)!)
    }
}
