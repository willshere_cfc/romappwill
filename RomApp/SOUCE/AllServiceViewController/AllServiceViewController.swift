//
//  AllServiceViewController.swift
//  RomApp
//
//  Created by Administrator on 1/2/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

enum AllServiceT1T2Cell: Int {

    
    case transfer = 0
    case register
    case atm
}

enum AllServiceT3Cell: Int {
    case topup = 0
    case sellPackage
    case sellPIN
    case transfer
    case atm
}

enum AllServiceSelectTear {
    case T1T2
    case T3
}

class AllServiceViewController: UIViewController {

    
    var menus:Array = [""]
    var menusIcon:Array  = [""]
    
    var setSelectType:AllServiceSelectTear!
    
    @IBOutlet weak var allserviceTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.setSelectType == .T3
        {
            menus = ["เติมเงิน", "ขายแพ็กเกจ", "ขาย PIN เกม", "โอนเงิน", "แนะนำการเติมเงินผ่าน ATM"]
            menusIcon  = ["topup_service", "pack_service", "pin_service", "transfer_service", "atm_service"]
        }
        else
        {
            menus = ["โอนเงิน", "สมัครตัวแทน", "แนะนำการเติมเงินผ่าน ATM"]
            menusIcon  = ["transfer_service","regis_service", "atm_service"]
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func menuDidSelect(_ sender: Any)
    {
        self.slideMenuController()?.openLeft()
    }

}

extension AllServiceViewController : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if let menu = MenuHemberger(rawValue: indexPath.row)
        {
            switch menu
            {
            case .main, .allService, .history, .report, .contact, .news, .profile, .about, .logout:
                return MenuTableViewCell.height()
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if self.setSelectType == .T3
        {
            if let menu = AllServiceT3Cell(rawValue: indexPath.row)
            {
                
                switch menu
                {
                    
                case .topup:
                    let storyboard = UIStoryboard(name: "TopUpStoryBoard", bundle: nil)
                    let topUpInputNumberViewController = storyboard.instantiateViewController(withIdentifier: "TopUpInputNumberViewControllerID") as! TopUpInputNumberViewController
                    navigationController?.pushViewController(topUpInputNumberViewController,animated: true)
                    
                case .sellPackage:
                    let storyboard = UIStoryboard(name: "PackageStoryBoard", bundle: nil)
                    let packageInputNumberViewController = storyboard.instantiateViewController(withIdentifier: "PackageInputNumberViewControllerID") as! PackageInputNumberViewController
                    navigationController?.pushViewController(packageInputNumberViewController,animated: true)
                case .sellPIN:
                    let storyboard = UIStoryboard(name: "E-PINStoryBoard", bundle: nil)
                    let ePinInputNumberViewController = storyboard.instantiateViewController(withIdentifier: "EPinInputNumberViewControllerID") as! EPinInputNumberViewController
                    navigationController?.pushViewController(ePinInputNumberViewController,
                                                             animated: true)
                case .transfer:
                    let storyboard = UIStoryboard(name: "TransferStoryboard", bundle: nil)
                    let transferInputPriceViewController = storyboard.instantiateViewController(withIdentifier: "TransferInputPriceViewControllerID") as! TransferInputPriceViewController
                    navigationController?.pushViewController(transferInputPriceViewController,animated: true)
                case .atm:print("atm")
                    
                }

            }
           
       
        }
        else
        {
            if let menu = AllServiceT1T2Cell(rawValue: indexPath.row)
            {
                
                switch menu {
                case .transfer:
                    let storyboard = UIStoryboard(name: "TransferStoryboard", bundle: nil)
                    let transferInputPriceViewController = storyboard.instantiateViewController(withIdentifier: "TransferInputPriceViewControllerID") as! TransferInputPriceViewController
                    navigationController?.pushViewController(transferInputPriceViewController,animated: true)
                case .register:
                    let storyboard = UIStoryboard(name: "RegisterStoryboard", bundle: nil)
                    let registerInputPhoneAndCardIDViewController = storyboard.instantiateViewController(withIdentifier: "RegisterInputPhoneAndCardIDViewControllerID") as! RegisterInputPhoneAndCardIDViewController
                    navigationController?.pushViewController(registerInputPhoneAndCardIDViewController,animated: true)
                case .atm:
                   print("atm")
                }
            }
            
        }
        
    }
    
}

extension AllServiceViewController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return menus.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if self.setSelectType == .T1T2
        {
            if let menu = AllServiceT3Cell(rawValue: indexPath.row)
            {
                switch menu
                {
                case .topup, .sellPackage, .sellPIN, .transfer, .atm:
                    
                    let cellIdentifier = "AllServiceTableViewCellIdenttifier"
                    let cell:AllServiceTableViewCell = self.allserviceTableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! AllServiceTableViewCell
                    
                    cell .setTitleCell(title: menus[indexPath.row])
                    cell .setImageIcon(imgName: menusIcon[indexPath.row])
                    
                    return cell
                }
            }
        }
        else
        {
            if let menu = AllServiceT1T2Cell(rawValue: indexPath.row)
            {
                
                switch menu {
                case .transfer, .register, .atm:
                    
                    let cellIdentifier = "AllServiceTableViewCellIdenttifier"
                    let cell:AllServiceTableViewCell = self.allserviceTableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! AllServiceTableViewCell
                    
                    cell .setTitleCell(title: menus[indexPath.row])
                    cell .setImageIcon(imgName: menusIcon[indexPath.row])
                    
                    return cell
                }
                
            }
        }
        
        return UITableViewCell()
    }
}



extension AllServiceViewController : SlideMenuControllerDelegate {
    
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        print("SlideMenuControllerDelegate: rightDidClose")
    }
}

