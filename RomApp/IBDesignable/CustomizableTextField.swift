//
//  CustomizableTextField.swift
//  RomApp
//
//  Created by Administrator on 12/26/16.
//  Copyright © 2016 Willshere. All rights reserved.
//

import UIKit

@IBDesignable class CustomizableTextField: UITextField {

    @IBInspectable var cornerRadius: CGFloat = 0 {
        
        didSet {
            
            layer.cornerRadius = cornerRadius
        }
    }

}
