//
//  CheckReachabilityViewController.swift
//  RomApp
//
//  Created by Administrator on 1/2/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit

class CheckReachabilityViewController: UIViewController {

    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.checkStatusWifi()
        
        
        // Do any additional setup after loading the view.
    }
    
    func checkStatusWifi()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.networkStatusChanged(_:)), name: NSNotification.Name(rawValue: ReachabilityStatusChangedNotification), object: nil)
        Reach().monitorReachabilityChanges()
    }
    
    func networkStatusChanged(_ notification: Notification) {
        
        let status = Reach().connectionStatus()
        switch status {
        case .unknown, .offline:
            print("Not connected")
        case .online(.wwan):
            self.dismisPOPUP()
            print("Connected via WWAN")
        case .online(.wiFi):
            print("Connected via WiFi")
            
        }
    }
    
    func dismisPOPUP()
    {
        self.dismiss(animated: true, completion: nil)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
