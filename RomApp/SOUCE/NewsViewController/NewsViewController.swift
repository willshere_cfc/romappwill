//
//  NewsViewController.swift
//  RomApp
//
//  Created by Administrator on 1/1/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit
import JGProgressHUD

enum NewsSelectType {
    case HambergerType
    case MainType
}

class NewsViewController: UIViewController ,UITableViewDataSource, UITableViewDelegate  {

    var setSelectType:NewsSelectType!
    
    @IBOutlet weak var btn_LeftNavbar: UIButton!
    
    
    @IBOutlet weak var newsTableView: UITableView!
    
    var newsObj : NewsObject? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setingSelectType()
        self.requestServiceGetNews()
        
        NotificationCenter.default.addObserver(self, selector: #selector(NewsViewController.requestServiceGetNews), name: Notification.Name("NotificationUpdateBagdeNews"), object: nil)
        // Do any additional setup after loading the view.
    }
    
    func setingSelectType()
    {
        if setSelectType == .HambergerType
        {
            self.btn_LeftNavbar.setImage(UIImage(named: "side_bar_ic"), for: .normal)
        }
        else if setSelectType == .MainType
        {
            self.btn_LeftNavbar.setImage(UIImage(named: "arrow-left"), for: .normal)
            self.slideMenuController()?.removeLeftGestures()
            
        }
        else {}
    }
    
    //MARK: - Service
    func requestServiceGetNews()
    {
        let defult = UserDefaults.standard
        let dataDF =  defult.value(forKey: "user_phonenumber")
        
        
        let parameters: [String: Dictionary] =
            [
                "data":
                    [
                        "mobile_no": dataDF!
                    ]
            ]
        
        print("param => \(parameters)")
        
        let strURL = "http://stg-etopupservices.ais.co.th/dev-ROMMobileServiceAPI/ROMService.svc/GetNews/"
        
        JGProgressHUD.loadingProgress(view: self.view)
        AFWrapper.requestPOSTURL(strURL, params: parameters as [String : AnyObject]?, headers: nil, success: { (JSONResponse) ->  Void in
           
            print("Getnews \(JSONResponse)")
            
            JGProgressHUD.dismisProgress()
            
            if JSONResponse.count != 0
            {
                
                self.newsObj = NewsObject(value: JSONResponse)
                
                self.newsTableView.reloadData()
            }
            
            
        }) { (error) -> Void in
            print(error)
            JGProgressHUD.dismisProgress()
            
        }
        
    }
    


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: IBAction
    
    @IBAction func leftNavbarDidSelect(_ sender: Any)
    {
        if setSelectType == .HambergerType
        {
            self.slideMenuController()?.openLeft()
        }
        else if setSelectType == .MainType
        {
            _ = self.navigationController?.popViewController(animated: true)
        }
        else {}
    }

    // MARK: TABLE VIEW DELEGATE
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.newsObj != nil
        {
           return self.newsObj!.newsObject_news.count;
        }
        else
        {
            return 0
        }
        
 
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
 
                let cellIdentifier = "NewsTableViewCellIdenttifier"
                let cell:NewsTableViewCell = self.newsTableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! NewsTableViewCell
        
            let newsListObj = NewsListObject.init(value:self.newsObj!.newsObject_news[indexPath.row])
            
        
             cell .settingNewsCellWithObject(newsObj: newsListObj!)
        
        
                return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        let storyboard = UIStoryboard(name: "NewsStoryBoard", bundle: nil)
        let newsDetailViewController = storyboard.instantiateViewController(withIdentifier: "NewsDetailViewControllerID") as! NewsDetailViewController
        
         let newsListObj = NewsListObject.init(value:self.newsObj!.newsObject_news[indexPath.row])
        
        newsDetailViewController.newsListObj = newsListObj
        
        navigationController?.pushViewController(newsDetailViewController,
                                                 animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
