//
//  TransferSelectContactTableViewCell.swift
//  RomApp
//
//  Created by Administrator on 1/6/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit

protocol TransferContectDelegate {
    
    func favoriteDidSelect(row:Int, buttonImg:UIButton, secion: Int)
    func selectContactDidSelect(row:Int, buttonImg:UIButton, secion: Int)
}

class TransferSelectContactTableViewCell: UITableViewCell {

    var secionFav : Int = 0
    var secionSelectContact : Int = 0
    
    @IBOutlet weak var btn_Favorite: UIButton!
    
    @IBOutlet weak var btn_SelectContact: UIButton!
    
    @IBOutlet weak var lbl_Name: UILabel!
    
    @IBOutlet weak var lbl_Phone: UILabel!
    
    
    
    var delegate : TransferContectDelegate?
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    open override func awakeFromNib() {
    }
    
    open func setup() {
    }
    
    open class func height() -> CGFloat {
        return 60
    }
    
    open func settingContactCellWithObject(obj:ContactListObject)
    {
        self.lbl_Name.text = "\(obj.contactListObject_nick_name)"
        self.lbl_Phone.text = String().stringFormatPhoneNumber(phoneNumber: "\(obj.contactListObject_mobile)")
        
    }
    
    open func setButtonFavoriteShow() {
        
        self.btn_Favorite.setBackgroundImage(UIImage(named: "Favorite_ac_ic"), for: UIControlState.normal)
        
    }
    
    open func setButtonFavoriteHiden() {
        
        self.btn_Favorite.setBackgroundImage(UIImage(named: "Favorite_non_ac_ic"), for: UIControlState.normal)
        
    }
    
    @IBAction func favoriteDidSelect(_ sender: Any) {
        
        self.delegate? .favoriteDidSelect(row: self.tag, buttonImg: self.btn_Favorite, secion: self.secionFav)
    }
  
    @IBAction func selectContactDidSelect(_ sender: Any)
    {
         self.delegate? .selectContactDidSelect(row: self.tag, buttonImg: self.btn_SelectContact, secion: self.secionSelectContact)
    }


}
