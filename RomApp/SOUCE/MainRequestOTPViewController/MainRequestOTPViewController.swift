//
//  MainRequestOTPViewController.swift
//  RomApp
//
//  Created by Administrator on 1/23/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit

class MainRequestOTPViewController: UIViewController {

    @IBOutlet weak var lbl_PhoneNumber: UILabel!
    
    var phoneNumber:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.lbl_PhoneNumber.text = String().stringFormatPhoneNumber(phoneNumber: "\(self.phoneNumber!)")

        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK : - IBAction
    
    @IBAction func confirmDidSelect(_ sender: Any)
    {
        let storyboard = UIStoryboard(name: "RequestOTPStoryboard", bundle: nil)
        let getOTPViewController = storyboard.instantiateViewController(withIdentifier: "GetOTPViewControllerID") as! GetOTPViewController
        getOTPViewController.phoneNumber = self.phoneNumber
        navigationController?.pushViewController(getOTPViewController,animated: true)
    }
 

}
