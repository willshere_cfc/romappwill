//
//  TransferSelectContactViewController.swift
//  RomApp
//
//  Created by Administrator on 1/6/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit
import JGProgressHUD
import CFAlertViewController

class TransferSelectContactViewController: UIViewController, HeaderContactViewDelegate {

    @IBOutlet weak var txt_SearchBar: UITextField!

    @IBOutlet weak var transferContactTableView: UITableView!
    
    @IBOutlet weak var lbl_Price: UILabel!
    
    var set_expand : NSMutableSet = []
    
    var strInputPrice : String!
    var getContactObj : GetContactObject? = nil
//    var contactListObj : ContactListObject? = nil
     let arrFavorate : NSMutableArray! = []
     let arrNoFavorate : NSMutableArray! = []
     let arrChareter : NSMutableArray! = []
    
    var set_select : NSMutableSet = []
    
    var set_select1 : NSMutableSet = []
    var set_select2 : NSMutableSet = []
    
    var containCheckBox : Bool = true
    var containCheckBox2 : Bool = true
    var containCheckBox3 : Bool = true
    
    let arrSelectList : NSMutableArray = []
    override func viewDidLoad() {
        super.viewDidLoad()

        self.requestServiceGetContractList()
        
    }
    
    //MARK: - Setup UI
    func setupUI()
    {
        self.lbl_Price.text = self.strInputPrice
    }
    
    //MARK: - SERVICE
    
    func requestServiceGetContractList()
    {
        let defult = UserDefaults.standard
        let dataUser_agent_id =  defult.value(forKey: "user_agent_id")
        
        
        let parameters: [String: Dictionary] =
            [
                "data":
                    [
                        "agent_id": dataUser_agent_id!
                ]
        ]
        
       
        
        let strURL = "http://stg-etopupservices.ais.co.th/dev-ROMMobileServiceAPI/ROMService.svc/getContractList2/"
        
        JGProgressHUD.loadingProgress(view: self.view)
        AFWrapper.requestPOSTURL(strURL, params: parameters as [String : AnyObject]?, headers: nil, success: { (JSONResponse) ->  Void in
          
            
            JGProgressHUD.dismisProgress()
            
            if JSONResponse.count != 0
            {
                self.getContactObj = nil
                self.getContactObj = GetContactObject.init(value: JSONResponse)
                
          
                for i  in 0...(self.getContactObj!.getContactObject_contract_l.count) {

                    self.set_expand .add(NSNumber(value: i))
                }
                
             
                
                self.transferContactTableView.reloadData()
            }
            
            
        }) { (error) -> Void in
            print(error)
            JGProgressHUD.dismisProgress()
        }
    }
    
    //MARK: -Service
    func requestServiceInsertUpdateContact(obj:ContactListObject)
    {
        
        var fav : String = ""
        
        if obj.contactListObject_favorite == "Y"
        {
            fav = "N"
        }
        else
        {
            fav = "Y"
        }
        
        let parameters: [String: Dictionary] =
            [
                "data":
                    [
                        "id": "\(obj.contactListObject_id)",
                        "agent_id": "\(obj.contactListObject_agent_id)",
                        "mobile": "\(obj.contactListObject_mobile)",
                        "first_name": "\(obj.contactListObject_first_name)",
                        "last_name": "\(obj.contactListObject_last_name)",
                        "nick_name": "\(obj.contactListObject_nick_name)",
                        "favorite": "\(fav)"
                ]
        ]
        
        print("params fav contact => \(parameters)")
        
        let strURL = "http://stg-etopupservices.ais.co.th/dev-ROMMobileServiceAPI/ROMService.svc/insertupdateContractList/"
        
        JGProgressHUD.loadingProgress(view: self.view)
        AFWrapper.requestPOSTURL(strURL, params: parameters as [String : AnyObject]?, headers: nil, success: { (JSONResponse) ->  Void in
            print(JSONResponse)
            
            JGProgressHUD.dismisProgress()
            
            if JSONResponse.count != 0
            {
                if JSONResponse["status"].stringValue == "success"
                {
                    //                    if JSONResponse["status"].stringValue == "Y"
                    //                    {
                    //
                    //                    }
                    //                    else
                    //                    {
                    //
                    //                    }
                    
                    self.requestServiceGetContractList()
                }
                else
                {
                    self.alertControllWith(title: "\(JSONResponse["message"].stringValue)", button: "ตกลง", typeSuccess: false)
                }
            }
            
            
        }) { (error) -> Void in
            print(error)
            JGProgressHUD.dismisProgress()
        }
    }

    //MARK: - Alert
    func alertControllWith(title:String, button:String, typeSuccess:Bool)
    {
        let alert = CFAlertViewController.alert(withTitle: title, message: nil, textAlignment: NSTextAlignment.center, preferredStyle: CFAlertControllerStyle.alert) {
            
            print("Dismiss")
            
            if typeSuccess == true
            {
                
                
            }
            
        }
        
        let actionDefault = CFAlertAction.init(title: button, style: CFAlertActionStyle.default, alignment: CFAlertActionAlignment.justified, color: UIColor().hexStringToUIColor(hex: "#F7941D")) { action  in
            
            
        }
        
        alert .addAction(actionDefault!)
        
        self.present(alert, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: IBAction
    
    @IBAction func leftNavbarDidSelect(_ sender: Any)
    {
        _ = self.navigationController?.popViewController(animated: true) 
    }
    
    @IBAction func addContactDidSelect(_ sender: Any)
    {
        let storyboard = UIStoryboard(name: "TransferStoryboard", bundle: nil)
        let addContectNameViewController = storyboard.instantiateViewController(withIdentifier: "AddContectNameViewControllerID") as! AddContectNameViewController
        navigationController?.pushViewController(addContectNameViewController, animated: true)
    }
    
    @IBAction func confirmDidSelect(_ sender: Any)
    {
        print("select list array \(self.arrSelectList)")
//        let storyboard = UIStoryboard(name: "TransferStoryboard", bundle: nil)
//        let transferACViewController = storyboard.instantiateViewController(withIdentifier: "TransferACViewControllerID") as! TransferACViewController
//        navigationController?.pushViewController(transferACViewController, animated: true)
    }
    
    func didSelectHeader(section : Int) {
        
       
        
        switch section {
        case section:
            
            var containExpand : Bool
            containExpand = self.set_expand .contains(NSNumber(value: section))
            if containExpand
            {
                self.set_expand .remove(NSNumber(value: section))
                let indexSet : IndexSet = [section]
                self.transferContactTableView .reloadSections(indexSet, with: UITableViewRowAnimation.fade)
            }
            else
            {
                self.set_expand .add(NSNumber(value: section))
                let indexSet : IndexSet = [section]
                self.transferContactTableView .reloadSections(indexSet, with: UITableViewRowAnimation.fade)
            }
            
            break
        default:
            break
        }
        
    }

}

// MARK: - EXTENION

// MARK: - Tableview Delegate

extension TransferSelectContactViewController : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let viewHeader:HeaderContactView = HeaderContactView.init(frame: .init(x: 0.0, y: 0.0, width: 343, height: HeaderContactView.height()))
        
        viewHeader .delegate = self
        viewHeader .setHeader(title: ["":""])
        viewHeader .tag = section
        
        
        let titleLabel = UILabel.init(frame: .init(x: 24.0, y: 5.0, width: 200.0, height: 20.0))
        
        titleLabel.textColor = UIColor.white
        titleLabel.font = UIFont(name: "DBHelvethaicaMonX-Med", size: 21)
        
        
        if section == 0
        {
            titleLabel.text = "รายการโปรด"
            viewHeader.backgroundColor = UIColor().hexStringToUIColor(hex: "#82BF41")
        }
        else
        {
            let contact = ContactObject.init(value: self.getContactObj!.getContactObject_contract_l[section-1])
            
            titleLabel.text = "\(contact!.contactObject_text_drop)"
            viewHeader.backgroundColor = UIColor().hexStringToUIColor(hex: "#CDCDCD")
        }
        
        viewHeader .addSubview(titleLabel)
        
        return viewHeader
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return HeaderContactView.height()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        return 60
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
//        let storyboard = UIStoryboard(name: "ContactStoryboard", bundle: nil)
//        let contactDetailViewController = storyboard.instantiateViewController(withIdentifier: "ContactDetailViewControllerID") as! ContactDetailViewController
//        
//        
//        if indexPath.section == 0
//        {
//            let contactList = ContactListObject.init(value: self.getContactObj!.getContactObject_contract_f[indexPath.row])
//            
//            contactDetailViewController.contactListObj = contactList
//            
//            
//        }
//        else
//        {
//            let contact = ContactObject.init(value: self.getContactObj!.getContactObject_contract_l[indexPath.section-1])
//            
//            let contactList = ContactListObject.init(value: contact!.contactObject_contract_drop[indexPath.row])
//            
//            contactDetailViewController.contactListObj = contactList
//            
//            
//        }
//        
//        
//        navigationController?.pushViewController(contactDetailViewController,
//                                                 animated: true)
        
    }
    
}

extension TransferSelectContactViewController : UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        
        
        
        if self.getContactObj != nil
        {
            return  1 + self.getContactObj!.getContactObject_contract_l.count
        }
        else
        {
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        if self.set_expand .contains(NSNumber.init(value: section))
        {
            if section == 0
            {
                
                return self.getContactObj!.getContactObject_contract_f.count
            }
            else
            {
                let contact = ContactObject.init(value: self.getContactObj!.getContactObject_contract_l[section-1])
                
                
                return contact!.contactObject_contract_drop.count
            }
            
        }
        else
        {
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellIdentifier = "TransferSelectContactTableViewCellIdentifier"
        let cell:TransferSelectContactTableViewCell = self.transferContactTableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! TransferSelectContactTableViewCell
        
        cell .delegate = self
        cell .tag = indexPath.row
        cell .secionFav = indexPath.section
        cell .secionSelectContact = indexPath.section
        
        if indexPath.section == 0
        {
            cell .setButtonFavoriteShow()
            
            let contactList = ContactListObject.init(value: self.getContactObj!.getContactObject_contract_f[indexPath.row])
            
            
            cell .settingContactCellWithObject(obj: contactList!)
            
        }
        else
        {
            cell .setButtonFavoriteHiden()
            
            let contact = ContactObject.init(value: self.getContactObj!.getContactObject_contract_l[indexPath.section-1])
            
            let contactList = ContactListObject.init(value: contact!.contactObject_contract_drop[indexPath.row])
            
            
            cell .settingContactCellWithObject(obj: contactList!)
        }
        
        return cell
    }
}

//MARK: - CONTACT  DELEGATE
extension TransferSelectContactViewController : TransferContectDelegate {
    
    
    func favoriteDidSelect(row:Int, buttonImg:UIButton , secion:Int) {
        
        print("row \(row) secion \(secion)")
        
        if secion == 0
        {
            let contactList = ContactListObject.init(value: self.getContactObj!.getContactObject_contract_f[row])
            
            print("name fav \(contactList!.contactListObject_first_name)")
            
           // self.requestServiceInsertUpdateContact(obj: contactList!)
        }
        else
        {
            let contact = ContactObject.init(value: self.getContactObj!.getContactObject_contract_l[secion-1])
            
            let contactList = ContactListObject.init(value: contact!.contactObject_contract_drop[row])
            
            print("name un fav \(contactList!.contactListObject_first_name)")
            
           // self.requestServiceInsertUpdateContact(obj: contactList!)
        }
        
    }
    
    func selectContactDidSelect(row:Int, buttonImg:UIButton, secion: Int) {
       
        print("row \(row) secion \(secion)")
        
//        containCheckBox = self.set_select .contains(NSNumber(value: row))
//        if containCheckBox
//        {
//            self.set_select .remove(NSNumber(value: row))
//            
//            
//            buttonImg .setBackgroundImage(UIImage.init(named: "checkbox-uncheck"), for: .normal)
//        }
//        else
//        {
//            self.set_select .add(NSNumber(value: row))
//            
//            
//            buttonImg .setBackgroundImage(UIImage.init(named: "checkbox-check"), for: .normal)
//            
//            
////            print("contacts Last name \(self.contactLastName(contacts[row]))")
////            print("contacts Phones \(self.contactPhones(contacts[row]))")
//            
//        }
        
        
        if secion == 0
        {
            let contactList = ContactListObject.init(value: self.getContactObj!.getContactObject_contract_f[row])
            print("Int(contactList!.contactListObject_agent_id) \(Int(contactList!.contactListObject_id)!)")
            containCheckBox2 = self.set_select1 .contains(NSNumber(value: Int(contactList!.contactListObject_id)!))
            if containCheckBox2
            {
                
                self.set_select1 .remove(NSNumber(value: Int(contactList!.contactListObject_id)!))
                
                 arrSelectList .remove(contactList!.contactListObject_first_name)
                
                 buttonImg .setBackgroundImage(UIImage.init(named: "checkbox-uncheck"), for: .normal)
            }
            else
            {
                self.set_select1 .add(NSNumber(value: Int(contactList!.contactListObject_id)!))
                
                 arrSelectList .add(contactList!.contactListObject_first_name)
                
                buttonImg .setBackgroundImage(UIImage.init(named: "checkbox-check"), for: .normal)
 
            }


            print("set_select1 \(self.set_select1)")
           
        }
        else
        {
            let contact = ContactObject.init(value: self.getContactObj!.getContactObject_contract_l[secion-1])
            
            let contactList = ContactListObject.init(value: contact!.contactObject_contract_drop[row])
            
            
            containCheckBox3 = self.set_select2 .contains(NSNumber(value: Int(contactList!.contactListObject_id)!))
            if containCheckBox3
            {
                self.set_select2 .remove(NSNumber(value: Int(contactList!.contactListObject_id)!))
                
                arrSelectList .remove(contactList!.contactListObject_first_name)
                
                buttonImg .setBackgroundImage(UIImage.init(named: "checkbox-uncheck"), for: .normal)
            }
            else
            {
                self.set_select2 .add(NSNumber(value: Int(contactList!.contactListObject_id)!))
                
                arrSelectList .add(contactList!.contactListObject_first_name)
                
                  buttonImg .setBackgroundImage(UIImage.init(named: "checkbox-check"), for: .normal)
                
            }
            
            print("Int(contactList!.contactListObject_agent_id) \(Int(contactList!.contactListObject_id)!)")
            print("set_select1 \(self.set_select2)")
        }
        
     
//        self.mutableSet(set: self.set_select)
        

        self.transferContactTableView.reloadData()

    }
    
}

























