//
//  TopupTransactionCollectionViewCell.swift
//  RomApp
//
//  Created by Administrator on 1/6/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit

class TopupTransactionCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var img_topSellPackageImage: UIImageView!
    
    open func settingTopSellPackageCellWithObject(topsellObj:TopupTopSellPackageObject){
        let imgData = Data(base64Encoded: topsellObj.topupTopSellPackageObject_imageUrl)
        self.img_topSellPackageImage.image = UIImage(data: imgData!)
    }
    
}
