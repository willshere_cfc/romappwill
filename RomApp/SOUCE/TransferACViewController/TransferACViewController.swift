//
//  TransferACViewController.swift
//  RomApp
//
//  Created by Administrator on 1/6/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit

class TransferACViewController: UIViewController {

    @IBOutlet weak var txt_AgentCode: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - IBAction
    
    @IBAction func leftNavbarDidSelect(_ sender: Any)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func confirmDidSelect(_ sender: Any)
    {
        let storyboard = UIStoryboard(name: "TransferStoryboard", bundle: nil)
        let transferTransactionViewController = storyboard.instantiateViewController(withIdentifier: "TransferTransactionViewControllerID") as! TransferTransactionViewController
        navigationController?.pushViewController(transferTransactionViewController, animated: true)
    }
    
    @IBAction func transferListDidSelect(_ sender: Any)
    {
        let storyboard = UIStoryboard(name: "TransferStoryboard", bundle: nil)
        let transferListViewController = storyboard.instantiateViewController(withIdentifier: "TransferListViewControllerID") as! TransferListViewController
        navigationController?.pushViewController(transferListViewController, animated: true)
    }

    // MARK: - UITEXTFIELD DELEGATE
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.txt_AgentCode.resignFirstResponder()
        
        return true
    }

}
