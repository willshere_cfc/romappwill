//
//  MenuTableViewCell.swift
//  RomApp
//
//  Created by Administrator on 12/28/16.
//  Copyright © 2016 Willshere. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {

    @IBOutlet weak var lbl_Title: UILabel!
    
    @IBOutlet weak var img_MenuBar: UIImageView!
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    open override func awakeFromNib() {
    }
    
    open func setup() {
    }
    
    open class func height() -> CGFloat {
        return 70
    }
    
    open func setTitleCell(title:String){
        
        self.lbl_Title?.text = title
    }
    
    open func setImageIcon(imgName:String){
        
        self.img_MenuBar.image = UIImage(named:imgName)
    }

}
