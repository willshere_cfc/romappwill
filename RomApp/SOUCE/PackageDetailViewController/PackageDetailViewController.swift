//
//  PackageDetailViewController.swift
//  RomApp
//
//  Created by Administrator on 1/5/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit

class PackageDetailViewController: UIViewController {

    @IBOutlet weak var packageDetailTableView: UITableView!
    
    @IBOutlet weak var btn_PackageTopup1: CustomizableButton!
    
    @IBOutlet weak var btn_PackageTopup2: CustomizableButton!
    
    var flagTopupPackage : Bool?
    
    var menusStr : String!
    
    var menus: Array = [""]
    
    var packageListObj : PackageListObject?  = nil
    var topupOfferedPackageObj : TopupOfferedPackageObject? = nil
    var phoneNumber : String!
    
    @IBOutlet weak var lbl_PackageTitle: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if self.flagTopupPackage! == true
        {
            self.btn_PackageTopup1.isHidden = false
            self.btn_PackageTopup2.isHidden = false
        }
        else
        {
            self.btn_PackageTopup1.isHidden = true
            self.btn_PackageTopup2.isHidden = true
        }
        
        self.packageDetailTableView.rowHeight = UITableViewAutomaticDimension
        
        self.packageDetailTableView.estimatedRowHeight = 38
        
       
        
        print("menu ==== \(menus)")
        
        self.setupUi()
        // Do any additional setup after loading the view.
    }
    
    func setupUi()
    {
        if self.packageListObj != nil
        {
            menusStr = "\(self.packageListObj!.packageListObject_desc)"
            menus  = menusStr.components(separatedBy: "|")
            
            self.lbl_PackageTitle.text = "\(self.packageListObj!.pacakgeListObject_title)"
        }
        else
        {
            menusStr = "\(self.topupOfferedPackageObj!.topupOfferedPackageObject_longDesc)"
            menus  = menusStr.components(separatedBy: "|")
            
            self.lbl_PackageTitle.text = "\(self.topupOfferedPackageObj!.topupOfferedPackageObject_title)"
        }
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - IBAction
    
    @IBAction func packageMoreDidSelect(_ sender: Any)
    {
        let storyboard = UIStoryboard(name: "PackageStoryBoard", bundle: nil)
        let packageCagtegoryViewController = storyboard.instantiateViewController(withIdentifier: "PackageCagtegoryViewControllerID") as! PackageCagtegoryViewController
        
        packageCagtegoryViewController.phoneNumber = self.phoneNumber!
        navigationController?.pushViewController(packageCagtegoryViewController, animated: true)
    }
    
    
    
    @IBAction func leftNavbarDidSelect(_ sender: Any)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }

    @IBAction func confirmDidSelect(_ sender: Any)
    {
        let storyboard = UIStoryboard(name: "PackageStoryBoard", bundle: nil)
        let packageACViewController = storyboard.instantiateViewController(withIdentifier: "PackageACViewControllerID") as! PackageACViewController
        
        packageACViewController.phoneNumber = self.phoneNumber!
        
        if self.packageListObj != nil
        {
           packageACViewController.packageListObj = self.packageListObj
        }
        else
        {
            packageACViewController.topupOfferedPackageObj = self.topupOfferedPackageObj
            
            
        }
       
        navigationController?.pushViewController(packageACViewController, animated: true)
    }

}

// MARK: - EXTENION

// MARK: - Tableview Delegate

extension PackageDetailViewController : UITableViewDelegate
{
  
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
    
}

extension PackageDetailViewController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return menus.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellIdentifier = "PackageDetailTableViewCellIdentifier"
        let cell:PackageDetailTableViewCell = self.packageDetailTableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! PackageDetailTableViewCell
        
        cell .setDetailCell(title: menus[indexPath.row])

        
        return cell
    }
}

