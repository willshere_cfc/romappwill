//
//  RegisterConfirmViewController.swift
//  RomApp
//
//  Created by Administrator on 1/17/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit
import JGProgressHUD
import CFAlertViewController

class RegisterConfirmViewController: UIViewController {

    
    @IBOutlet weak var img1_Confirm: UIImageView!
    
    @IBOutlet weak var img2_Confirm: UIImageView!
    
    @IBOutlet weak var img3_Confirm: UIImageView!
    
    @IBOutlet weak var img4_Confirm: UIImageView!
    
    @IBOutlet weak var img5_Confirm: UIImageView!

    
    //MARK: - Pass Params
    var param_NameTH: String!
    var param_LastNameTH: String!
    var param_NameEN: String!
    var param_LastNameEN: String!
    var param_BirthDate: String!
    var param_CardID: String!
    var param_PhoneNumber: String!
    var param_Job: String!
    var param_Email: String! = ""
    
    var param_Address_address: String!
    var param_Address_road: String!
    var param_Address_province: String!
    var param_Address_district: String!
    var param_Address_zone: String!
    var param_Address_zipcode: String!
    
    var param_Live_address: String!
    var param_Live_road: String!
    var param_Live_province: String!
    var param_Live_district: String!
    var param_Live_zone: String!
    var param_Live_zipcode: String!
    
    var param_region_code: String!
    var param_province_code: String!
    var param_region_code_live: String!
    var param_province_code_live: String!
    
    var img1: UIImage!
    var img2: UIImage!
    var img3: UIImage!
    var img4: UIImage!
    var img5: UIImage? = nil
    
     var typeRegis : String!
    
    var menus: Array! = []
    
    var menusTitle : Array = ["ชื่อ-นามสกุล  (ไทย)","ชื่อ-นามสกุล  (อังกฤษ)","หมายเลขบัตรประชาชน","วัน/เดือน/ปีเกิด","อาชีพ","อีเมล์","ที่อยู่ตามทะเบียนบ้าน","ที่อยู่ที่ติดต่อได้"]
    
//    var menusTitle : Array = [""]
    
    
    
    @IBOutlet weak var registerConfirmTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.menus = ["\(self.param_NameTH!) \(self.param_LastNameTH!)",
                    "\(self.param_NameEN!) \(self.param_LastNameEN!)",
        "\(self.param_CardID!)",
        "\(self.param_BirthDate!)",
        "\(self.param_Job!)",
        "\(self.param_Email!)",
        "\(self.param_Address_address!) \(self.param_Address_road!) \(self.param_Address_zone!) \(self.param_Address_district!) \(self.param_Address_province!) \(self.param_Address_zipcode!)",
        "\(self.param_Live_address!) \(self.param_Live_road!) \(self.param_Live_zone!) \(self.param_Live_district!) \(self.param_Live_province!) \(self.param_Live_zipcode!)"]
        
//        self.menus = []
        self.setupUI()

        self.registerConfirmTableView.rowHeight = UITableViewAutomaticDimension
        
        self.registerConfirmTableView.estimatedRowHeight = 38
        // Do any additional setup after loading the view.
    }
    
    func setupUI()
    {
//        self.img1_Confirm.image = self.waterImg(img: self.img1)
//        self.img2_Confirm.image = self.waterImg(img: self.img2)
//        self.img3_Confirm.image = self.waterImg(img: self.img3)
//        self.img4_Confirm.image = self.waterImg(img: self.img4)

        self.img1_Confirm.image =  self.img1
        self.img2_Confirm.image = self.img2
        self.img3_Confirm.image =  self.img3
        self.img4_Confirm.image =  self.img4
        
        if self.img5 != nil
        {
            self.img5_Confirm.image =  self.img5!
        }
       

    }
    
    func waterImg(img:UIImage) -> UIImage
    {
        let imgWater = UIImage(named: "")
        
        
        let rect = CGRect(x: 0, y: 0, width: img.size.width, height: img.size.height)
        
        UIGraphicsBeginImageContextWithOptions(img.size, true, 0)
        let context = UIGraphicsGetCurrentContext()
        
        context!.setFillColor(UIColor.white.cgColor)
        context!.fill(rect)
        
        img.draw(in: rect, blendMode: .normal, alpha: 1)
        imgWater!.draw(in: CGRect(x: 0, y: 0, width:200 , height:200), blendMode: .normal, alpha: 1)
        
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

       // UIImageWriteToSavedPhotosAlbum(result!, nil, nil, nil)
        
        return result!
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - IBAction
    
    @IBAction func confirmDidSelect(_ sender: Any)
    {
        self.requestServiceRegis()

    }
    
    @IBAction func backDidSelect(_ sender: Any)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    
    //MARK: - SERVICE
    
    func requestServiceRegis()
    {
        
        let defult = UserDefaults.standard
        let dataDF =  defult.value(forKey: "user_phonenumber")
        let dataPIN =  defult.value(forKey: "user_pin")
        
        let myThumb1 = self.img1_Confirm.image!.resizeWith(percentage: 0.2)
        let myThumb2 = self.img2_Confirm.image!.resizeWith(percentage: 0.2)
        let myThumb3 = self.img3_Confirm.image!.resizeWith(percentage: 0.2)
        let myThumb4 = self.img4_Confirm.image!.resizeWith(percentage: 0.2)
        
        var data5 : NSData? = nil
        
        if self.img5 != nil
        {
             let myThumb5 = self.img5_Confirm.image!.resizeWith(percentage: 0.2)
              data5 = UIImagePNGRepresentation(myThumb5!) as NSData?
        }
        
       
        
        let data1 = UIImagePNGRepresentation(myThumb1!) as NSData?
        let data2 = UIImagePNGRepresentation(myThumb2!) as NSData?
        let data3 = UIImagePNGRepresentation(myThumb3!) as NSData?
        let data4 = UIImagePNGRepresentation(myThumb4!) as NSData?
       

        let str1 = self.param_PhoneNumber!
        let str2 = self.param_NameTH!
        let str3 = self.param_LastNameTH!
        let str4 = self.param_NameEN!
        let str5 = self.param_LastNameEN!
        let str6 = self.param_BirthDate!
        let str7 = self.param_CardID!
        let str8 = self.param_Job!
        let str9 = self.param_Email!
        let str10 = self.param_Address_address!
        let str11 = self.param_Address_road!
        let str12 = self.param_province_code!
        let str13 = self.param_Address_district!
        let str14 = self.param_Address_zone!
        let str15 = self.param_Address_zipcode!
        let str16 = self.param_region_code!
        let str17 = self.param_Live_address!
        let str18 = self.param_Live_road!
        let str19 = self.param_province_code_live!
        let str20 = self.param_Live_district!
        let str21 = self.param_Live_zone!
        let str22 = self.param_Live_zipcode!
       
        
//        var dict2 : Dictionary =  [ "1" : "abc", "2" : "cde"]
//        dict2.updateValue("efg", forKey: "3")
        
        if self.img5 != nil
        {
            let parameters: [String: Dictionary] =
                [
                    "data":
                        [
                            "type": "\(self.typeRegis!)",
                            "mobile_no":  "\(str1)"  ,
                            "upmobile_no": dataDF! ,
                            "firstname_th": "\(str2)",
                            "lastname_th":  "\(str3)",
                            "firstname_en": "\(str4)",
                            "lastname_en":  "\(str5)",
                            "birthdate": "\(str6)",
                            "pid": "\(str7)",
                            "occupation": "\(str8)",
                            "email": "\(str9)",
                            "address1": "\(str10)",
                            "address2": "\(str11)",
                            "provincecode": "\(str12)",
                            "district": "\(str13)",
                            "subdistrict": "\(str14)",
                            "postcode": "\(str15)",
                            "regioncode": "\(str16)",
                            "contact_address1": "\(str17)",
                            "contact_address2": "\(str18)",
                            "contact_provincecode": "\(str19)",
                            "contact_district": "\(str20)",
                            "contact_subdistrict": "\(str21)",
                            "contact_postcode": "\(str22)",
                            "pin": dataPIN!,
                            "documents": [
                                [
                                    "index": 1,
                                    "extension": "JPG",
                                    "filecontents": "\(data1!.base64EncodedString(options: []))"
                                ],
                                [
                                    "index": 2,
                                    "extension": "JPG",
                                    "filecontents": "\(data2!.base64EncodedString(options: []))"
                                ],
                                [
                                    "index": 3,
                                    "extension": "JPG",
                                    "filecontents": "\(data3!.base64EncodedString(options: []))"
                                ],
                                [
                                    "index": 4,
                                    "extension": "JPG",
                                    "filecontents": "\(data4!.base64EncodedString(options: []))"
                                ],
                                [
                                    "index": 5,
                                    "extension": "JPG",
                                    "filecontents": "\(data5!.base64EncodedString(options: []))"
                                ]
                            ]
                    ]
            ]
            
            print("params register => \(parameters)")
            
            let strURL = "http://stg-etopupservices.ais.co.th/dev-ROMMobileServiceAPI/ROMService.svc/Register/"
            
            JGProgressHUD.loadingProgress(view: self.view)
            AFWrapper.requestPOSTURL(strURL, params: parameters as [String : AnyObject]?, headers: nil, success: { (JSONResponse) ->  Void in
                
                JGProgressHUD.dismisProgress()
                
                
                
                print("regis success => \(JSONResponse)")
                
                if JSONResponse.count != 0
                {
                    let registerObj = RegisterObject(value: JSONResponse)
                    
                    if registerObj!.registerObject_status == "success"
                    {
                        self.pushViewConreoller(message:registerObj!.registerObject_message)
                    }
                    else
                    {
                        self.alertControllWith(title: registerObj!.registerObject_response_message, button: "ตกลง")
                    }
                    
                }
                
                
            }) { (error) -> Void in
                print(error)
                JGProgressHUD.dismisProgress()
            }

        }
        else
        {
            let parameters: [String: Dictionary] =
                [
                    "data":
                        [
                            "type": "\(self.typeRegis!)",
                            "mobile_no":  "\(str1)"  ,
                            "upmobile_no": dataDF! ,
                            "firstname_th": "\(str2)",
                            "lastname_th":  "\(str3)",
                            "firstname_en": "\(str4)",
                            "lastname_en":  "\(str5)",
                            "birthdate": "\(str6)",
                            "pid": "\(str7)",
                            "occupation": "\(str8)",
                            "email": "\(str9)",
                            "address1": "\(str10)",
                            "address2": "\(str11)",
                            "provincecode": "\(str12)",
                            "district": "\(str13)",
                            "subdistrict": "\(str14)",
                            "postcode": "\(str15)",
                            "regioncode": "\(str16)",
                            "contact_address1": "\(str17)",
                            "contact_address2": "\(str18)",
                            "contact_provincecode": "\(str19)",
                            "contact_district": "\(str20)",
                            "contact_subdistrict": "\(str21)",
                            "contact_postcode": "\(str22)",
                            "pin": dataPIN!,
                            "documents": [
                                [
                                    "index": 1,
                                    "extension": "JPG",
                                    "filecontents": "\(data1!.base64EncodedString(options: []))"
                                ],
                                [
                                    "index": 2,
                                    "extension": "JPG",
                                    "filecontents": "\(data2!.base64EncodedString(options: []))"
                                ],
                                [
                                    "index": 3,
                                    "extension": "JPG",
                                    "filecontents": "\(data3!.base64EncodedString(options: []))"
                                ],
                                [
                                    "index": 4,
                                    "extension": "JPG",
                                    "filecontents": "\(data4!.base64EncodedString(options: []))"
                                ],
                                [
                                    "index": 5,
                                    "extension": "JPG",
                                    "filecontents": ""
                                ]
                            ]
                    ]
            ]
            
            print("params register => \(parameters)")
            
            let strURL = "http://stg-etopupservices.ais.co.th/dev-ROMMobileServiceAPI/ROMService.svc/Register/"
            
            JGProgressHUD.loadingProgress(view: self.view)
            AFWrapper.requestPOSTURL(strURL, params: parameters as [String : AnyObject]?, headers: nil, success: { (JSONResponse) ->  Void in
                
                JGProgressHUD.dismisProgress()
                
                
                
                print("regis success => \(JSONResponse)")
                
                if JSONResponse.count != 0
                {
                    let registerObj = RegisterObject(value: JSONResponse)
                    
                    if registerObj!.registerObject_status == "success"
                    {
                        self.pushViewConreoller(message:registerObj!.registerObject_message)
                    }
                    else
                    {
                        self.alertControllWith(title: registerObj!.registerObject_response_message, button: "ตกลง")
                    }
                    
                }
                
                
            }) { (error) -> Void in
                print(error)
                JGProgressHUD.dismisProgress()
            }

        }
        
        
    }
    
    //MARK: - Push View Controller
    func pushViewConreoller(message:String)
    {
        
                let storyboard = UIStoryboard(name: "RegisterStoryboard", bundle: nil)
                let registerSuccessViewController = storyboard.instantiateViewController(withIdentifier: "RegisterSuccessViewControllerID") as! RegisterSuccessViewController
        
                registerSuccessViewController.messageStr = message
                navigationController?.pushViewController(registerSuccessViewController, animated: true)
    }
    
    //MARK: - Alert
    func alertControllWith(title:String, button:String)
    {
        let alert = CFAlertViewController.alert(withTitle: title, message: nil, textAlignment: NSTextAlignment.center, preferredStyle: CFAlertControllerStyle.alert) {
            
            print("Dismiss")
            
        }
        
        let actionDefault = CFAlertAction.init(title: button, style: CFAlertActionStyle.default, alignment: CFAlertActionAlignment.justified, color: UIColor().hexStringToUIColor(hex: "#F7941D")) { action  in
            
            
        }
        
        alert .addAction(actionDefault!)
        
        self.present(alert, animated: true, completion: nil)
    }


}


// MARK: - EXTENION

// MARK: - Tableview Delegate

extension RegisterConfirmViewController : UITableViewDelegate
{
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
}

extension RegisterConfirmViewController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return menus.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellIdentifier = "RegisterConfirmTableViewCellIdentifier"
        let cell:RegisterConfirmTableViewCell = self.registerConfirmTableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! RegisterConfirmTableViewCell
        
        cell .setTitleCell(title: menusTitle[indexPath.row])
        cell .setDetailCell(title: menus![indexPath.row] as! String)
        
        
        return cell
    }
}

