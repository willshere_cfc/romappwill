//
//  AddContectNameViewController.swift
//  RomApp
//
//  Created by Administrator on 1/10/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit
import JGProgressHUD
import CFAlertViewController

class AddContectNameViewController: UIViewController {

    @IBOutlet weak var txt_PhoneNumber: UITextField!
    @IBOutlet weak var txt_Name: UITextField!
    
    @IBOutlet weak var txt_LastName: UITextField!
    
    @IBOutlet weak var txt_NickName: UITextField!
    
    var contactListObj : ContactListObject? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - SERVICE
    
    func requestServiceInsertUpdateContact()
    {
        let defult = UserDefaults.standard
        let dataUser_agent_id =  defult.value(forKey: "user_agent_id")
        
        
        let parameters: [String: Dictionary] =
            [
                "data":
                    [
                        "id": "",
                        "agent_id": "\(dataUser_agent_id!)",
                        "mobile": "\(self.txt_PhoneNumber.text!)",
                        "first_name": "\(self.txt_Name.text!)",
                        "last_name": "\(self.txt_LastName.text!)",
                        "nick_name": "\(self.txt_NickName.text!)",
                        "favorite": "N"
                    ]
        ]
        
        print("param \(parameters)")
        
        let strURL = "http://stg-etopupservices.ais.co.th/dev-ROMMobileServiceAPI/ROMService.svc/insertupdateContractList/"
        
        JGProgressHUD.loadingProgress(view: self.view)
        AFWrapper.requestPOSTURL(strURL, params: parameters as [String : AnyObject]?, headers: nil, success: { (JSONResponse) ->  Void in
            print(JSONResponse)
            
            JGProgressHUD.dismisProgress()
            
            if JSONResponse.count != 0
            {
               if JSONResponse["status"].stringValue == "success"
                {
                    self.alertControllWith(title: "\(JSONResponse["message"].stringValue)", button: "ตกลง" ,typeSuccess: true)
                    
                }
                else
                {
                    self.alertControllWith(title: "\(JSONResponse["message"].stringValue)", button: "ตกลง", typeSuccess: false)
                }
            }
            
            
        }) { (error) -> Void in
            print(error)
            JGProgressHUD.dismisProgress()
        }
    }
    
    //MARK: - Alert
    func alertControllWith(title:String, button:String, typeSuccess:Bool)
    {
        let alert = CFAlertViewController.alert(withTitle: title, message: nil, textAlignment: NSTextAlignment.center, preferredStyle: CFAlertControllerStyle.alert) {
            
            print("Dismiss")
            
            if typeSuccess == true
            {
               
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NotificationUpdateContactList"), object: nil)
                
                _ = self.navigationController?.popViewController(animated: true)
            }
            
        }
        
        let actionDefault = CFAlertAction.init(title: button, style: CFAlertActionStyle.default, alignment: CFAlertActionAlignment.justified, color: UIColor().hexStringToUIColor(hex: "#F7941D")) { action  in
            
            
        }
        
        alert .addAction(actionDefault!)
        
        self.present(alert, animated: true, completion: nil)
    }



    //MARK: IBAction
    
    @IBAction func leftNavbarDidSelect(_ sender: Any)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func confirmDidSelect(_ sender: Any)
    {
        if self.txt_Name.text?.length == 0 || self.txt_LastName.text?.length == 0 || self.txt_NickName.text?.length == 0 || self.txt_PhoneNumber.text?.length == 0
        {
            self.alertControllWith(title: "กรุณากรอกข้อมูลให้ครบ", button: "ตกลง", typeSuccess: false)
        }
        else
        {
              self.requestServiceInsertUpdateContact()
        }
        
        
    }
    
    // MARK: - UITEXTFIELD DELEGATE
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.txt_PhoneNumber.resignFirstResponder()
        self.txt_Name.resignFirstResponder()
        self.txt_LastName.resignFirstResponder()
        self.txt_NickName.resignFirstResponder()
        
        return true
    }

}
