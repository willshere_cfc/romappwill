//
//  EPinTableViewCell.swift
//  RomApp
//
//  Created by Administrator on 1/4/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit

class EPinTableViewCell: UITableViewCell {

    @IBOutlet weak var lbl_epinPrice: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func inintWithPrice(price:String){
        lbl_epinPrice.text = price
        
    }

}
