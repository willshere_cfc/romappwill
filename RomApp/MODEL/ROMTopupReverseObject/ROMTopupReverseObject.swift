//
//  ROMTopupReverseObject.swift
//  RomApp
//
//  Created by Phutsita Aphilertrungchut on 1/30/2560 BE.
//  Copyright © 2560 Willshere. All rights reserved.
//

import UIKit
import SwiftyJSON

class ROMTopupReverseObject: NSObject {
    let romTopupReverseObject_status : String
    let romTopupReverseObject_message : String
    let romTopupReverseObject_response_code : String
    let romTopupReverseObject_response_message : String
    let romTopupReverseObject_transactionid : String
    let romTopupReverseObject_datetime : String
    
    init(romTopupReverseObject_status : String,
         romTopupReverseObject_message : String,
         romTopupReverseObject_response_code : String,
         romTopupReverseObject_response_message : String,
         romTopupReverseObject_transactionid : String,
         romTopupReverseObject_datetime : String){
        self.romTopupReverseObject_status = romTopupReverseObject_status
        self.romTopupReverseObject_message = romTopupReverseObject_message
        self.romTopupReverseObject_response_code = romTopupReverseObject_response_code
        self.romTopupReverseObject_response_message = romTopupReverseObject_response_message
        self.romTopupReverseObject_transactionid = romTopupReverseObject_transactionid
        self.romTopupReverseObject_datetime = romTopupReverseObject_datetime
    }
    
    required convenience init?(value:JSON){
        self.init(romTopupReverseObject_status : value["status"].stringValue,
                  romTopupReverseObject_message : value["message"].stringValue,
                  romTopupReverseObject_response_code : value["response_code"].stringValue,
                  romTopupReverseObject_response_message : value["response_message"].stringValue,
                  romTopupReverseObject_transactionid : value["transactionid"].stringValue,
                  romTopupReverseObject_datetime : value["datetime"].stringValue)
    }
}
