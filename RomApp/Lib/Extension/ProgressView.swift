//
//  ProgressView.swift
//  RomApp
//
//  Created by Administrator on 1/26/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import Foundation
import JGProgressHUD

private var HUD : JGProgressHUD!

extension JGProgressHUD {

    static func loadingProgress(view:UIView)
    {
        HUD = JGProgressHUD(style: JGProgressHUDStyle(rawValue: 0)!)
        HUD?.textLabel.text = "Loading..."
        HUD?.show(in: view, animated: true)
    }
    
    static func dismisProgress()
    {
        HUD.dismiss(afterDelay: 0.5)
    }
    
}
