//
//  CustomizableImage.swift
//  RomApp
//
//  Created by Administrator on 12/30/16.
//  Copyright © 2016 Willshere. All rights reserved.
//

import UIKit

@IBDesignable class CustomizableImage: UIImageView {

//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        self.setup()
//    }
//    
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//        self.setup()
//    }
    
    
    @IBInspectable var cornerRadius : CGFloat = 0 {
        
        didSet {
            
            
            layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var borderWightCircle : CGFloat = 0 {
        
        didSet {
            
            layer.borderWidth = borderWightCircle
            layer.borderColor = UIColor.lightGray.cgColor
        }
    }
    
////    @IBInspectable var cornerRadius: CGFloat = 0.0
//    @IBInspectable var borderColor: UIColor = UIColor.black
//    @IBInspectable var borderShadowWidth: CGFloat = 0.5
//    private var customBackgroundColor = UIColor.white
//    override var backgroundColor: UIColor?{
//        didSet {
//            customBackgroundColor = backgroundColor!
//            super.backgroundColor = UIColor.clear
//        }
//    }
//    
//    func setup() {
//        layer.shadowColor = UIColor.black.cgColor;
//        layer.shadowOffset = CGSize.zero;
//        layer.shadowRadius = 5.0;
//        layer.shadowOpacity = 0.5;
//        super.backgroundColor = UIColor.clear
//    }
//    
//   
//    
//    override func draw(_ rect: CGRect) {
//        customBackgroundColor.setFill()
//        UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius ).fill()
//        
//        let borderRect = bounds.insetBy(dx: borderShadowWidth/2, dy: borderShadowWidth/2)
//        let borderPath = UIBezierPath(roundedRect: borderRect, cornerRadius: cornerRadius - borderShadowWidth/2)
//        borderColor.setStroke()
//        borderPath.lineWidth = borderShadowWidth
//        borderPath.stroke()
//        
//        // whatever else you need drawn
//    }
    
}
