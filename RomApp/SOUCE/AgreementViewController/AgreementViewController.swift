//
//  AgreementViewController.swift
//  RomApp
//
//  Created by Administrator on 12/29/16.
//  Copyright © 2016 Willshere. All rights reserved.
//

import UIKit

enum AgreementTear: String {
    case T1T2
    case T3
    
}

class AgreementViewController: UIViewController {

    var setType:AgreementTear!
    
    var agreementObject: AgreementObject?
    
    @IBOutlet weak var btn_Checkbox: UIButton!
    @IBOutlet weak var btn_Confirm: CustomizableButton!
    
    @IBOutlet weak var txt_DetailAmg: UITextView!
    @IBOutlet weak var webview: UIWebView!
    
    @IBOutlet weak var activityProgress: UIActivityIndicatorView!
    
    var flagScrollWebView : Bool = true
    var phoneNumber : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()


        self.btn_Checkbox.isEnabled = false
       /* self.txt_DetailAmg.font = UIFont(name: "DBHelvethaicaMonX-Med", size: 20)

        if  !self.btn_Checkbox.isSelected
        {
            self.btn_Confirm.isEnabled = false
        }*/
        
        self.webview.scrollView.delegate = self
        self.webview.loadRequest(URLRequest(url: URL(string: self.agreementObject!.agreementObject_message)!))
        
        self.activityProgress.startAnimating()
    
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - IBAction
    
    @IBAction func checkboxDidSelect(_ sender: UIButton)
    {
        sender.isSelected = !sender.isSelected
        
        if(sender.isSelected == true)
        {
            self.btn_Confirm.isEnabled = true
            sender.setImage(UIImage(named:"checkbox-check"), for: .normal)
        }
        else
        {
            self.btn_Confirm.isEnabled = false
             sender.setImage(UIImage(named:"checkbox-uncheck"), for: .normal)
        }
    }
    
    @IBAction func confirmDidselect(_ sender: Any)
    {
        self.requestServiceUpdateAgreement()
        
    }
    
    
    //MARK : SERVICE
    
    func requestServiceUpdateAgreement()
    {
        let defult = UserDefaults.standard
        let dataDF =  defult.value(forKey: "user_auth_token")
        
        
        let parameters: [String: Dictionary] =
            [
                "data":
                    [
                        "code": self.agreementObject!.agreementObject_code,
                        "mobile_no": self.phoneNumber!,
                        "device_id": dataDF!
                    ]
        ]
        
        print("param => \(parameters)")
        
        let strURL = "http://stg-etopupservices.ais.co.th/dev-ROMMobileServiceAPI/ROMService.svc/ConfirmAgreement/"
        
        AFWrapper.requestPOSTURL(strURL, params: parameters as [String : AnyObject]?, headers: nil, success: { (JSONResponse) ->  Void in
            print(JSONResponse)
            
            print("repon agreement => \(JSONResponse)")
            
            self.settupMainview()
            
            
        }) { (error) -> Void in
            print(error)
        }
 
    }
    
    //MARK: - MAIN VIEW CONTROLLER
    func settupMainview()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let mainViewController = storyboard.instantiateViewController(withIdentifier: "MainViewControllerID") as! MainViewController
        let menuViewController = storyboard.instantiateViewController(withIdentifier: "MenuViewControllerID") as! MenuViewController
        
        let nav: UINavigationController = UINavigationController(rootViewController: mainViewController)
        nav.setNavigationBarHidden(true, animated: false)
        
        menuViewController.mainViewController = nav
        
        if setType == .T1T2
        {
            mainViewController .setType = .T1T2
            menuViewController .menuTear = .T1T2
        }
        else if setType == .T3
        {
            mainViewController .setType = .T3
            menuViewController .menuTear = .T3
        }
        else {}
        
        
        mainViewController.phoneNumber = self.phoneNumber
        let slideMenuController = ExSlideMenuController(mainViewController:nav, leftMenuViewController: menuViewController)
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        slideMenuController.delegate = mainViewController
        UIApplication.shared.keyWindow?.rootViewController = slideMenuController
        UIApplication.shared.keyWindow?.makeKeyAndVisible()
    }


}

extension AgreementViewController : UIWebViewDelegate {
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
        self.activityProgress.stopAnimating()
    }
}

extension AgreementViewController : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if(scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height))
        {
            if flagScrollWebView
            {
                NSLog("BOTTOM REACHED");
                flagScrollWebView = false
                self.btn_Checkbox.isEnabled = true
            }
            
            
        }
        if(scrollView.contentOffset.y <= 0.0)
        {
            NSLog("TOP REACHED");
        }
        
    }
}
