//
//  GetOTPViewController.swift
//  RomApp
//
//  Created by Administrator on 2/1/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit
import JGProgressHUD

class GetOTPViewController: UIViewController {

    @IBOutlet weak var txt_OTP: UITextField!
    
    var getOTPObj : GetOTPObject? = nil
    var verifyOTPObj : VerifyOTPObject? = nil
    
    var typeRegis : String?
     var phoneNumber:String!
    
    @IBOutlet weak var lbl_PhoneNumber: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.lbl_PhoneNumber.text = String().stringFormatPhoneNumber(phoneNumber: "\(self.phoneNumber!)")
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - IBAction
    
    @IBAction func requestOTPDidSelect(_ sender: Any)
    {
        self.requestServiceGetOTP()
    }

    @IBAction func confirmDidSelect(_ sender: Any)
    {
        if (self.txt_OTP.text?.length)! > 0
        {
          self.requestServiceVerifyOTP()  
        }
        
    }
    
    //MARK: - Service
    
    func requestServiceGetOTP()
    {
        let defult = UserDefaults.standard
        let dataDF =  defult.value(forKey: "user_phonenumber")
        
        let parameters: [String: Dictionary] =
            [
                "data":
                    [
                        "mobile_no": dataDF!
                    ]
            ]
        
        let strURL = "http://stg-etopupservices.ais.co.th/dev-ROMMobileServiceAPI/ROMService.svc/GetOTP/"
        
         JGProgressHUD.loadingProgress(view: self.view)
        AFWrapper.requestPOSTURL(strURL, params: parameters as [String : AnyObject]?, headers: nil, success: { (JSONResponse) ->  Void in
            print(JSONResponse)
            
            JGProgressHUD.dismisProgress()
            print("GetOTP => \(JSONResponse)")
            if JSONResponse.count != 0
            {
                 self.getOTPObj = GetOTPObject.init(value: JSONResponse)
                
                if self.getOTPObj!.getOTPObject_status == "success"
                {
                    
                }
                else
                {
                    
                }
              
            }
            
            
        }) { (error) -> Void in
            print(error)
            JGProgressHUD.dismisProgress()
        }
    }

    
    func requestServiceVerifyOTP()
    {
        let defult = UserDefaults.standard
        let dataDF =  defult.value(forKey: "user_phonenumber")
        
        let parameters: [String: Dictionary] =
            [
                "data":
                    [
                        "mobile_no": dataDF!,
                        "otp":"\(self.txt_OTP.text!)"
                    ]
            ]
        
        let strURL = "http://stg-etopupservices.ais.co.th/dev-ROMMobileServiceAPI/ROMService.svc/VerifyOTP/"
        
         JGProgressHUD.loadingProgress(view: self.view)
        AFWrapper.requestPOSTURL(strURL, params: parameters as [String : AnyObject]?, headers: nil, success: { (JSONResponse) ->  Void in
            print(JSONResponse)
            
            JGProgressHUD.dismisProgress()
            print("VerifyOTP => \(JSONResponse)")
            if JSONResponse.count != 0
            {
                self.verifyOTPObj = VerifyOTPObject.init(value: JSONResponse)
                
                if self.verifyOTPObj!.verifyOTPObject_status == "success"
                {
                    self.pushViewController()
                }
                else
                {
                    
                }
                
            }
            
            
        }) { (error) -> Void in
            print(error)
            JGProgressHUD.dismisProgress()
        }
    }

    //MARK : Push VC
    func pushViewController()
    {
        let storyboard = UIStoryboard(name: "RegisterStoryboard", bundle: nil)
        let registerInputPhoneAndCardIDViewController = storyboard.instantiateViewController(withIdentifier: "RegisterInputPhoneAndCardIDViewControllerID") as! RegisterInputPhoneAndCardIDViewController
        registerInputPhoneAndCardIDViewController.typeRegis = "I"
        navigationController?.pushViewController(registerInputPhoneAndCardIDViewController,animated: true)
    }

}
