//
//  EPinViewController.swift
//  RomApp
//
//  Created by Administrator on 1/4/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit

class EPinViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var epinTableView: UITableView!
    
    var priceArray : Array = ["50","100","300","500","1000"]
    var epinPhoneNumber : NSString = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK : UITableView DELEGATE
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.priceArray.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cellIdentifier = "EPinTableViewCellIdenttifier"
        let cell:EPinTableViewCell = self.epinTableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! EPinTableViewCell
        cell.inintWithPrice(price: priceArray[indexPath.row])
        //                cell .setTitleCell(title: menus[indexPath.row])
        return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "E-PINStoryBoard", bundle: nil)
        let ePinInputACViewController = storyboard.instantiateViewController(withIdentifier: "EPinInputACViewControllerID") as! EPinInputACViewController
        ePinInputACViewController.epinPrice = priceArray[indexPath.row] as NSString
        ePinInputACViewController.epinPhoneNumber = self.epinPhoneNumber
        navigationController?.pushViewController(ePinInputACViewController,
                                                 animated: true)
    }
    
    // MARK : IBAction
    
    @IBAction func leftNavbarDidSelect(_ sender: Any)
    {
        _ = navigationController?.popViewController(animated: true)
    }

 
    

}
