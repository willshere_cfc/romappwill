//
//  NewsViewController.swift
//  RomApp
//
//  Created by Administrator on 1/1/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit

enum Newsi5SelectType {
    case HambergerType
    case MainType
}

class Newsi5ViewController: UIViewController ,UITableViewDataSource, UITableViewDelegate  {
    
    var setSelectType:Newsi5SelectType!
    
    @IBOutlet weak var btn_LeftNavbar: UIButton!
    
    
    @IBOutlet weak var newsTableView: UITableView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setingSelectType()
        // Do any additional setup after loading the view.
    }
    
    func setingSelectType()
    {
        if setSelectType == .HambergerType
        {
            self.btn_LeftNavbar.setImage(UIImage(named: "side_bar_ic"), for: .normal)
        }
        else if setSelectType == .MainType
        {
            self.btn_LeftNavbar.setImage(UIImage(named: "arrow-left"), for: .normal)
            self.slideMenuController()?.removeLeftGestures()
            
        }
        else {}
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: IBAction
    
    @IBAction func leftNavbarDidSelect(_ sender: Any)
    {
        if setSelectType == .HambergerType
        {
            self.slideMenuController()?.openLeft()
        }
        else if setSelectType == .MainType
        {
            _ = self.navigationController?.popViewController(animated: true)
        }
        else {}
    }
    
    // MARK: TABLE VIEW DELEGATE
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cellIdentifier = "NewsTableViewCellIdenttifier"
        let cell:NewsTableViewCell = self.newsTableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! NewsTableViewCell
        
        //                cell .setTitleCell(title: menus[indexPath.row])
        return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "NewsStoryBoard", bundle: nil)
        let newsDetailViewController = storyboard.instantiateViewController(withIdentifier: "NewsDetailViewControllerID") as! NewsDetailViewController
        navigationController?.pushViewController(newsDetailViewController,
                                                 animated: true)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
