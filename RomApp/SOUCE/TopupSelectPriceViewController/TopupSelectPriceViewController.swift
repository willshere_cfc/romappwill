//
//  TopupSelectPriceViewController.swift
//  RomApp
//
//  Created by Administrator on 1/5/17.
//  Copyright © 2017 Willshere. All rights reserved.
//
import Foundation
import UIKit
import ActionSheetPicker_3_0
import Alamofire
import JGProgressHUD
import SwiftyJSON

class TopupSelectPriceViewController: UIViewController {

    var selectPickerIndex: NSInteger = 0
    var priceArray : Array = ["400","500","600","700","800","900","1000","1100","1200","1300","1400","1500","1600","1700"]
    var getFaceValueObject:GetFaceValueObject? = nil
    var topupPhoneNumber : NSString?
    var selectedFaceValue : String? = ""
    
    @IBOutlet weak var lbl_SelectPrice: UILabel!
    
    @IBOutlet weak var lbl_PhoneNumber: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        lbl_PhoneNumber.text = (self.topupPhoneNumber as! String)
        self.requestServiceGetFaceValue()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - IBAction
    
    @IBAction func leftNavbarDidSelect(_ sender: Any)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }

    @IBAction func confirmDidSelect(_ sender: Any)
    {
        let storyboard = UIStoryboard(name: "TopUpStoryBoard", bundle: nil)
        let topupACViewController = storyboard.instantiateViewController(withIdentifier: "TopupACViewControllerID") as! TopupACViewController
        
        topupACViewController.topupPhoneNumber = self.topupPhoneNumber
        topupACViewController.topupFaceValue = selectedFaceValue as NSString?
        
        navigationController?.pushViewController(topupACViewController, animated: true)
    }
    
    
    //MARK: Picker
    
    @IBAction func pricePickerDidSelect(_ sender: Any)
    {
        ActionSheetStringPicker.show(withTitle: "ราคา", rows:priceArray, initialSelection: self.selectPickerIndex, target: self, successAction: #selector(TopupSelectPriceViewController.priceWasSelected(selectedIndex:element:)), cancelAction: #selector(TopupSelectPriceViewController.canclePicker), origin: sender)
    }
   
    func priceWasSelected(selectedIndex :NSNumber, element:Any)
    {
        self.selectPickerIndex = selectedIndex.intValue
        self.selectedFaceValue  = priceArray[self.selectPickerIndex]
        self.lbl_SelectPrice.text = "\(priceArray[self.selectPickerIndex]) บาท"
        
    }
    
    func canclePicker()
    {
        
    }
    
    //MARK: Get Face Values
    
    func requestServiceGetFaceValue(){
        //let defult = UserDefaults.standard
        //let dataDF = defult.value(forKey: "user_auth_token")
        
        let strURL = "http://stg-etopupservices.ais.co.th/dev-ROMMobileServiceAPI/ROMService.svc/GetAll_Topup_facevalue/"
        
        JGProgressHUD.loadingProgress(view: self.view)
        AFWrapper.requestPOSTURL(strURL, params:nil, headers: nil, success: { (JSONResponse) ->  Void in
            print(JSONResponse)
            if JSONResponse.count != 0
            {
                
                
                self.getFaceValueObject = GetFaceValueObject.init(value: JSONResponse)
                let faceValueObj = FaceValueObject.init(value:(self.getFaceValueObject?.getFaceValueObject_faceValue[0])!)
                self.priceArray = (faceValueObj?.faceValueObject_value.components(separatedBy: ","))!
                
                print("faceValueObj?.faceValueObject_value => \(faceValueObj!.faceValueObject_value)")
                
                self.selectedFaceValue  = self.priceArray[0]
                self.lbl_SelectPrice.text = "\(self.priceArray[0]) บาท"
           }
            
            JGProgressHUD.dismisProgress()
        
            
        }) { (error) -> Void in
            print(error)
            JGProgressHUD.dismisProgress()
        }
        
    }

}
