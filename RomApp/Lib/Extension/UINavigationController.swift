//
//  UINavigationController.swift
//  RomApp
//
//  Created by Administrator on 1/2/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit


extension UINavigationController {
        func pop(animated: Bool) {
            _ = self.popViewController(animated: animated)
        }
        
        func popToRoot(animated: Bool) {
            _ = self.popToRootViewController(animated: animated)
        }
    }


