//
//  GetProfileObject.swift
//  RomApp
//
//  Created by Administrator on 1/23/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit
import SwiftyJSON

class GetProfileObject: NSObject {

    
    let getProfileObject_agreement : JSON
    let getProfileObject_agent_id : Int
    let getProfileObject_level : Int
    let getProfileObject_appl_version : String
    let getProfileObject_appl_download_link : String
    let getProfileObject_callcenter_no : String
    let getProfileObject_response_code : String
    let getProfileObject_status : String
    let getProfileObject_message : String
    let getProfileObject_agent_status : String
    
    
    init(getProfileObject_agreement : JSON,
     getProfileObject_agent_id : Int,
     getProfileObject_level : Int,
     getProfileObject_appl_version : String,
     getProfileObject_appl_download_link : String,
     getProfileObject_callcenter_no : String,
     getProfileObject_response_code : String,
     getProfileObject_status : String,
     getProfileObject_message : String,
     getProfileObject_agent_status : String)
    {
        self.getProfileObject_agreement = getProfileObject_agreement
        self.getProfileObject_agent_id = getProfileObject_agent_id
        self.getProfileObject_level = getProfileObject_level
        self.getProfileObject_appl_version = getProfileObject_appl_version
        self.getProfileObject_appl_download_link = getProfileObject_appl_download_link
        self.getProfileObject_callcenter_no = getProfileObject_callcenter_no
        self.getProfileObject_response_code = getProfileObject_response_code
        self.getProfileObject_status = getProfileObject_status
        self.getProfileObject_message = getProfileObject_message
        self.getProfileObject_agent_status = getProfileObject_agent_status
    }
    
    required convenience init?(value:JSON) {
        
        self.init(getProfileObject_agreement:value["agreement"],
        getProfileObject_agent_id : value["agent_id"].intValue,
        getProfileObject_level : value["level"].intValue,
        getProfileObject_appl_version : value["appl_version"].stringValue,
        getProfileObject_appl_download_link :value["appl_download_link"].stringValue,
        getProfileObject_callcenter_no :value["callcenter_no"].stringValue,
        getProfileObject_response_code :value["response_code"].stringValue,
        getProfileObject_status :value["status"].stringValue,
        getProfileObject_message : value["message"].stringValue,
        getProfileObject_agent_status : value["agent_status"].stringValue)
        
    }
    
}

class AgreementObject: NSObject {

    let agreementObject_mobile_accept : String
    let agreementObject_message : String
    let agreementObject_id : String
    let agreementObject_code : String
    let agreementObject_text : String
 
    init(agreementObject_mobile_accept : String,
     agreementObject_message : String,
     agreementObject_id : String,
     agreementObject_code : String,
     agreementObject_text : String)
    {
        self.agreementObject_mobile_accept = agreementObject_mobile_accept
        self.agreementObject_message = agreementObject_message
        self.agreementObject_id = agreementObject_id
        self.agreementObject_code = agreementObject_code
        self.agreementObject_text = agreementObject_text

    }
    
    required convenience init?(value:JSON) {
        
        self.init(agreementObject_mobile_accept : value["mobile_accept"].stringValue,
         agreementObject_message : value["message"].stringValue,
         agreementObject_id : value["id"].stringValue,
         agreementObject_code: value["code"].stringValue,
         agreementObject_text : value["text"].stringValue)
        
    }

}




























