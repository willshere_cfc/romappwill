//
//  HistoryListViewController.swift
//  RomApp
//
//  Created by Administrator on 1/14/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit
import JGProgressHUD

enum HistorySelectListType {
    case HambergerType
    case MainType
}

class HistoryListViewController: UIViewController {

    var setSelectType : HistorySelectListType!
    
    @IBOutlet weak var historyListTableView: UITableView!
    
    @IBOutlet weak var btn_LeftNavbar: UIButton!
    
    var getHistoryObj : GetHistoryObject? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setingSelectType()
        self.requestServiceGetHistory()
        // Do any additional setup after loading the view.
    }
    
    func setingSelectType()
    {
        if setSelectType == .HambergerType
        {
            self.btn_LeftNavbar.setImage(UIImage(named: "side_bar_ic"), for: .normal)
        }
        else if setSelectType == .MainType
        {
            self.btn_LeftNavbar.setImage(UIImage(named: "arrow-left"), for: .normal)
            self.slideMenuController()?.removeLeftGestures()
        }
        else {}
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - SERVICE
    
    func requestServiceGetHistory()
    {
        let defult = UserDefaults.standard
        let dataUser_agent_id =  defult.value(forKey: "user_agent_id")
        let dataUser_user_phonenumber =  defult.value(forKey: "user_phonenumber")
        let dataUser_user_level =  defult.value(forKey: "user_level")
        
        
        let parameters: [String: Dictionary] =
            [
                "data":
                    [
                        "agentid": "\(dataUser_agent_id!)",
                         "mobile_a": "\(dataUser_user_phonenumber!)",
                         "agent_level":"\(dataUser_user_level!)"
                    ]
            ]
        
        let strURL = "http://stg-etopupservices.ais.co.th/dev-ROMMobileServiceAPI/ROMService.svc/GetHistory/"
        
        JGProgressHUD.loadingProgress(view: self.view)
        AFWrapper.requestPOSTURL(strURL, params: parameters as [String : AnyObject]?, headers: nil, success: { (JSONResponse) ->  Void in
            print(JSONResponse)
            
            JGProgressHUD.dismisProgress()
            print("Get Main => \(JSONResponse)")
            if JSONResponse.count != 0
            {
                self.getHistoryObj = GetHistoryObject.init(value: JSONResponse)
                
                print("history count => \(self.getHistoryObj!.getHistoryObject_history.count)")
                
                self.historyListTableView.reloadData()
            }
            
            
        }) { (error) -> Void in
            print(error)
            JGProgressHUD.dismisProgress()
        }
    }

    
    //MARK: - IBAction

    @IBAction func leftNavbarDidSelect(_ sender: Any)
    {
        if setSelectType == .HambergerType
        {
             self.slideMenuController()?.openLeft()
        }
        else if setSelectType == .MainType
        {
            _ = self.navigationController?.popViewController(animated: true)
        }
        else {}
        
    }

}

// MARK: - EXTENION

// MARK: - Tableview Delegate

extension HistoryListViewController : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        return HistoryListTableViewCell.height()
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
         let historyObj = HistoryObject.init(value: self.getHistoryObj!.getHistoryObject_history[indexPath.row])
        
        print(self.getHistoryObj!.getHistoryObject_history[indexPath.row])
        
        if historyObj!.historyObject_detail.count > 0
        {
            
            
            let storyboard = UIStoryboard(name: "HistoryStoryboard", bundle: nil)
            let historyDetailViewController = storyboard.instantiateViewController(withIdentifier: "HistoryDetailViewControllerID") as! HistoryDetailViewController
            
            if indexPath.row == 0
            {
                historyDetailViewController .setType = .Topup
                historyDetailViewController.historyObj = historyObj
                navigationController?.pushViewController(historyDetailViewController, animated: true)
            }
            else if indexPath.row == 1
            {
                historyDetailViewController .setType = .SellPack
                historyDetailViewController.historyObj = historyObj
                navigationController?.pushViewController(historyDetailViewController, animated: true)
            }
            else if indexPath.row == 2
            {
                let historyTransferDetailViewController = storyboard.instantiateViewController(withIdentifier: "HistoryTransferDetailViewControllerID") as! HistoryTransferDetailViewController
                historyTransferDetailViewController.historyObj = historyObj
                navigationController?.pushViewController(historyTransferDetailViewController, animated: true)
            }
            else if indexPath.row == 3
            {
                historyDetailViewController .setType = .Transfer
                historyDetailViewController.historyObj = historyObj
                navigationController?.pushViewController(historyDetailViewController, animated: true)
            }
            else{}

        }
        
       
        
        
        
    }
    
    
}

extension HistoryListViewController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if self.getHistoryObj != nil
        {
            return self.getHistoryObj!.getHistoryObject_history.count
        }
        else
        {
            return 0
        }
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellIdentifier = "HistoryListTableViewCellIdentifier"
        let cell:HistoryListTableViewCell = self.historyListTableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! HistoryListTableViewCell
        
        let historyObj = HistoryObject.init(value: self.getHistoryObj!.getHistoryObject_history[indexPath.row])
        
        cell .settingCellWithObj(obj: historyObj!)
        
        
        return cell
    }
}

