//
//  ContactDetailViewController.swift
//  RomApp
//
//  Created by Administrator on 1/11/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit

enum ContactDetailSelectType {
    case HambergerType
    case MainType
}

class ContactDetailViewController: UIViewController {

    var setSelectType:ContactDetailSelectType!
    
    var contactListObj : ContactListObject? = nil
    
    @IBOutlet weak var lbl_PhoneNumber: UILabel!
    @IBOutlet weak var lbl_Name: UILabel!
    
    @IBOutlet weak var lbl_LastName: UILabel!
    @IBOutlet weak var lbl_Nickname: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupUI()
        // Do any additional setup after loading the view.
    }
    
    func setupUI()
    {
        self.lbl_PhoneNumber.text = "\(self.contactListObj!.contactListObject_mobile)"
        self.lbl_Name.text = "\(self.contactListObj!.contactListObject_first_name)"
        self.lbl_LastName.text = "\(self.contactListObj!.contactListObject_last_name)"
        self.lbl_Nickname.text = "\(self.contactListObj!.contactListObject_nick_name)"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func leftNavbarDidSelect(_ sender: Any)
    {
         _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func rightNavbarDidSelect(_ sender: Any)
    {
        let storyboard = UIStoryboard(name: "ContactStoryboard", bundle: nil)
        let contactEditViewController = storyboard.instantiateViewController(withIdentifier: "ContactEditViewControllerID") as! ContactEditViewController
        contactEditViewController.contactListObj = self.contactListObj
        navigationController?.pushViewController(contactEditViewController, animated: true)
    }


}
