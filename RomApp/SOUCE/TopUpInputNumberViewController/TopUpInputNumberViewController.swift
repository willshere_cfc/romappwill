//
//  TopUpInputNumberViewController.swift
//  RomApp
//
//  Created by Administrator on 1/5/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit

class TopUpInputNumberViewController: UIViewController {

    @IBOutlet weak var btn_LeftNavbar: UIButton!
    @IBOutlet weak var txt_PhoneNumber: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - IBAction
    
    @IBAction func leftNavbarDidSelect(_ sender: Any)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func confirmDidSelect(_ sender: Any)
    {
        let storyboard = UIStoryboard(name: "TopUpStoryBoard", bundle: nil)
        let topupSelectPriceViewController = storyboard.instantiateViewController(withIdentifier: "TopupSelectPriceViewControllerID") as! TopupSelectPriceViewController
        
        if(txt_PhoneNumber.text != nil && (txt_PhoneNumber.text! as String) != ""){
            topupSelectPriceViewController.topupPhoneNumber = txt_PhoneNumber.text as NSString?
            navigationController?.pushViewController(topupSelectPriceViewController, animated: true)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
