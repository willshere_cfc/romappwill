//
//  EPinInputACViewController.swift
//  RomApp
//
//  Created by Administrator on 1/4/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit
import SwiftyJSON
import JGProgressHUD

class EPinInputACViewController: UIViewController {

    @IBOutlet weak var txt_AgeanCode: UITextField!
    
    var epinPrice : NSString = "50"
    var epinPhoneNumber :NSString = ""
    
    @IBOutlet weak var lbl_phoneNumber: UILabel!
    @IBOutlet weak var lbl_epinPrice: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        lbl_phoneNumber.text = self.epinPhoneNumber as String
        lbl_epinPrice.text = self.epinPrice as String
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    
    // MARK: - IBAction

    @IBAction func leftNavbarDidSelect(_ sender: Any)
    {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func confirmDidSelect(_ sender: Any)
    {
       /* let storyboard = UIStoryboard(name: "E-PINStoryBoard", bundle: nil)
        let ePinTransactionViewController = storyboard.instantiateViewController(withIdentifier: "EPinTransactionViewControllerID") as! EPinTransactionViewController
        navigationController?.pushViewController(ePinTransactionViewController,
                                                 animated: true)*/
        self.requestServiceSellEPIN()
    }
    
    // MARK: - UITEXTFIELD DELEGATE
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.txt_AgeanCode.resignFirstResponder()
        
        return true
    }
    
    //MARK Confirm Sell EPin
    func requestServiceSellEPIN(){
        let defult = UserDefaults.standard
        let dataDF = defult.value(forKey: "user_phonenumber")
        
        let strURL = "http://stg-etopupservices.ais.co.th/dev-ROMMobileServiceAPI/ROMService.svc/ROMSellEPIN/"
        
        let parameters: [String: Dictionary] =
            [
               "data":
                    [
                        "mobileno_a": dataDF!,
                        "mobileno_b": self.epinPhoneNumber as Any,
                        "pin":txt_AgeanCode.text ?? "",
                        "amount": self.epinPrice
                ]
        ]
        
        JGProgressHUD.loadingProgress(view: self.view)
        AFWrapper.requestPOSTURL(strURL, params:parameters as [String : AnyObject]?, headers: nil, success: { (JSONResponse) ->  Void in
            print(JSONResponse)
            if JSONResponse.count != 0
            {
                let epinObj : EPinObject! = EPinObject.init(value: JSONResponse)
                
                if(epinObj.epinObject_status == "success"){
                    self.showEPINResultPage(epinObj: epinObj)
                }
            }
            
            JGProgressHUD.dismisProgress()
            
            
        }) { (error) -> Void in
            print(error)
            JGProgressHUD.dismisProgress()
        }
        
    }
    
    func showEPINResultPage(epinObj : EPinObject){
        //Goto Result Page
        let storyboard = UIStoryboard(name: "E-PINStoryBoard", bundle: nil)
        let ePinTransactionViewController = storyboard.instantiateViewController(withIdentifier: "EPinTransactionViewControllerID") as! EPinTransactionViewController
        ePinTransactionViewController.epinPhoneNumber = self.epinPhoneNumber
        ePinTransactionViewController.epinPrice = self.epinPrice
        ePinTransactionViewController.epinObject = epinObj
        navigationController?.pushViewController(ePinTransactionViewController,
                                                 animated: true)
        
    }

    
    

}
