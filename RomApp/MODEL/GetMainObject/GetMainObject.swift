//
//  GetMainObject.swift
//  RomApp
//
//  Created by Administrator on 1/25/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit
import SwiftyJSON

class GetMainObject: NSObject {

    let getMainObject_response_code : String
    let getMainObject_topupfacevalue : String
    let getMainObject_profilename : String
    let getMainObject_maxbalance : String
    let getMainObject_noti_count : String
    let getMainObject_epinfacevalue : String
    let getMainObject_agentstatus : String
    let getMainObject_transferstep : Int
    let getMainObject_balance : String
    let getMainObject_minagenttransfer : String
    let getMainObject_status  : String
    let getMainObject_transactionid   : String
    let getMainObject_response_message   : String
    let getMainObject_message   : String
    let getMainObject_datetime   : String
    
    init(getMainObject_response_code : String,
     getMainObject_topupfacevalue : String,
     getMainObject_profilename : String,
     getMainObject_maxbalance : String,
     getMainObject_noti_count : String,
     getMainObject_epinfacevalue : String,
     getMainObject_agentstatus : String,
     getMainObject_transferstep : Int,
     getMainObject_balance : String,
     getMainObject_minagenttransfer : String,
     getMainObject_status  : String,
     getMainObject_transactionid   : String,
     getMainObject_response_message   : String,
     getMainObject_message   : String,
     getMainObject_datetime   : String)
    {
        self.getMainObject_response_code = getMainObject_response_code
        self.getMainObject_topupfacevalue = getMainObject_topupfacevalue
        self.getMainObject_profilename = getMainObject_profilename
        self.getMainObject_maxbalance = getMainObject_maxbalance
        self.getMainObject_noti_count = getMainObject_noti_count
        self.getMainObject_epinfacevalue = getMainObject_epinfacevalue
        self.getMainObject_agentstatus = getMainObject_agentstatus
        self.getMainObject_transferstep = getMainObject_transferstep
        self.getMainObject_balance = getMainObject_balance
        self.getMainObject_minagenttransfer = getMainObject_minagenttransfer
        self.getMainObject_status  = getMainObject_status
        self.getMainObject_transactionid   = getMainObject_transactionid
        self.getMainObject_response_message   = getMainObject_response_message
        self.getMainObject_message   = getMainObject_message
        self.getMainObject_datetime   = getMainObject_datetime
    }
    
    required convenience init?(value:JSON) {
        
        self.init(getMainObject_response_code:value["response_code"].stringValue,
                  getMainObject_topupfacevalue:value["topupfacevalue"].stringValue,
                  getMainObject_profilename:value["profilename"].stringValue,
                  getMainObject_maxbalance:value["maxbalance"].stringValue,
                  getMainObject_noti_count:value["noti_count"].stringValue,
                  getMainObject_epinfacevalue:value["epinfacevalue"].stringValue,
                  getMainObject_agentstatus:value["agentstatus"].stringValue,
                  getMainObject_transferstep:value["transferstep"].intValue,
                  getMainObject_balance:value["balance"].stringValue,
                  getMainObject_minagenttransfer:value["minagenttransfer"].stringValue,
                  getMainObject_status:value["status"].stringValue,
                  getMainObject_transactionid:value["transactionid"].stringValue,
                  getMainObject_response_message:value["response_message"].stringValue,
                  getMainObject_message:value["message"].stringValue,
                  getMainObject_datetime:value["datetime"].stringValue)
    }
    
}

