//
//  HistoryDetailViewController.swift
//  RomApp
//
//  Created by Administrator on 1/14/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit

enum HistoryDetailType: String {
    case Topup
    case SellPack
    case SellPin
    case Transfer
    
}

class HistoryDetailViewController: UIViewController {

    var setType:HistoryDetailType!
    
    @IBOutlet weak var lbl_HistoryType: UILabel!
    
    @IBOutlet weak var txt_Search: UITextField!
    
    @IBOutlet weak var historyDetailTableView: UITableView!
    

    @IBOutlet weak var lbl_amount: UILabel!
    
    @IBOutlet weak var lbl_amont_Satang: UILabel!
    @IBOutlet weak var lbl_commission: UILabel!
    
    
    var historyObj : HistoryObject!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupUI()
        self.settingType()
        // Do any additional setup after loading the view.
    }
    
    func setupUI()
    {
        let string = "\(self.historyObj.historyObject_totalamount)"
        
        if string.range(of:".") != nil{
            print("exists")
            
            var token = string.components(separatedBy: ".")
            
            self.lbl_amount.text = token[0]
            self.lbl_amont_Satang.text = ".\(token[1])"
        }
        else
        {
            self.lbl_amount.text = "\(self.historyObj.historyObject_totalamount)"
            self.lbl_amont_Satang.text = ".00"
        }

        self.lbl_commission.text = "\(self.historyObj.historyObject_totalcommission)"
    }
    
    func settingType()
    {
        if self.setType == .Topup
        {
            self.lbl_HistoryType.text = "เติมเงิน"
        }
        else if self.setType == .SellPack
        {
            self.lbl_HistoryType.text = "ขายแพ็กเกจ"
        }
        else if self.setType == .SellPin
        {
            self.lbl_HistoryType.text = "ขาย PIN เกม"
        }
        else if self.setType == .Transfer
        {
            self.lbl_HistoryType.text = "โอนเงิน"
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - IBAction
    
    @IBAction func leftNavbarDidSelect(_ sender: Any)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }



}

//MARK: - EXTENSION

//MARK: - UITextField Delegate

extension HistoryDetailViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.txt_Search .resignFirstResponder()
        
        return true;
    }
}

// MARK: - Tableview Delegate

extension HistoryDetailViewController : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return HistoryDetailTableViewCell.height()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let historyDetailObj = HistoryDetailObject.init(value: self.historyObj!.historyObject_detail[indexPath.row])
        
                let storyboard = UIStoryboard(name: "HistoryStoryboard", bundle: nil)
                let historyTransactionViewController = storyboard.instantiateViewController(withIdentifier: "HistoryTransactionViewControllerID") as! HistoryTransactionViewController
        
        historyTransactionViewController.historyDetailObj = historyDetailObj
                navigationController?.pushViewController(historyTransactionViewController, animated: true)
    }
    
    
}

extension HistoryDetailViewController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.historyObj!.historyObject_detail.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellIdentifier = "HistoryDetailTableViewCellIdentifier"
        let cell:HistoryDetailTableViewCell = self.historyDetailTableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! HistoryDetailTableViewCell
        
         let historyDetailObj = HistoryDetailObject.init(value: self.historyObj!.historyObject_detail[indexPath.row])
        
        cell .settingCellWithObject(obj: historyDetailObj!)

        
        
        return cell
    }
}
















