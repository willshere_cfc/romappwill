//
//  HistoryListTableViewCell.swift
//  RomApp
//
//  Created by Administrator on 1/14/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit

class HistoryListTableViewCell: UITableViewCell {

    
    @IBOutlet weak var lbl_TitleType: UILabel!
    
    @IBOutlet weak var lbl_TotalCommission: UILabel!
    
    @IBOutlet weak var lbl_Totalamount: UILabel!
    
    @IBOutlet weak var lbl_Totalamount_Satang: UILabel!
    
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    open override func awakeFromNib() {
    }
    
    open func setup() {
    }
    
    open class func height() -> CGFloat {
        return 129
    }
    
    open func settingCellWithObj(obj:HistoryObject) {
        
        self.lbl_TitleType.text = "\(obj.historyObject_transname)"
        self.lbl_TotalCommission.text = "\(obj.historyObject_totalcommission)"
        
        
        let string = "\(obj.historyObject_totalamount)"
        
        if string.range(of:".") != nil{
            print("exists")
            
            var token = string.components(separatedBy: ".")
            
            self.lbl_Totalamount.text = token[0]
            self.lbl_Totalamount_Satang.text = ".\(token[1])"
        }
        else
        {
            self.lbl_Totalamount.text = "\(obj.historyObject_totalamount)"
            self.lbl_Totalamount_Satang.text = ".00"
        }
        
       
        
    }

}
