//
//  SigninObject.swift
//  RomApp
//
//  Created by Administrator on 1/25/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit
import SwiftyJSON

class SigninObject: NSObject {

    let signinObject_response_code : String
    let signinObject_status : String
    let signinObject_expires_in : String
    let signinObject_agent_status : String
    let signinObject_token_type : String
    let signinObject_agent_id : String
    let signinObject_access_token : String
    let signinObject_response_message : String
    let signinObject_message : String
    let signinObject_datetime : String
    let signinObject_menus : JSON

    init(signinObject_response_code : String,
        signinObject_status : String,
        signinObject_expires_in : String,
        signinObject_agent_status : String,
        signinObject_token_type : String,
        signinObject_agent_id : String,
        signinObject_access_token : String,
        signinObject_response_message : String,
        signinObject_message : String,
        signinObject_datetime : String,
        signinObject_menus : JSON)
    {
        self.signinObject_response_code = signinObject_response_code
        self.signinObject_status = signinObject_status
        self.signinObject_expires_in = signinObject_expires_in
        self.signinObject_agent_status = signinObject_agent_status
        self.signinObject_token_type = signinObject_token_type
        self.signinObject_agent_id = signinObject_agent_id
        self.signinObject_access_token = signinObject_access_token
        self.signinObject_response_message = signinObject_response_message
        self.signinObject_message = signinObject_message
        self.signinObject_datetime = signinObject_datetime
        self.signinObject_menus = signinObject_menus
    }
    
    required convenience init?(value:JSON) {
        
        self.init(signinObject_response_code:value["response_code"].stringValue,
                  signinObject_status:value["status"].stringValue,
                  signinObject_expires_in:value["expires_in"].stringValue,
                  signinObject_agent_status:value["agent_status"].stringValue,
                  signinObject_token_type:value["token_type"].stringValue,
                  signinObject_agent_id:value["agent_id"].stringValue,
                  signinObject_access_token:value["access_token"].stringValue,
                  signinObject_response_message:value["response_message"].stringValue,
                  signinObject_message:value["message"].stringValue,
                  signinObject_datetime:value["datetime"].stringValue,
                  signinObject_menus:value["menus"])
        
    }

}

class MenusObject : NSObject {
    
    let menusObject_enabled : String
    let menusObject_id : String
    let menusObject_name : String

    init(menusObject_enabled : String,
         menusObject_id : String,
         menusObject_name : String)
    {
        self.menusObject_enabled = menusObject_enabled
        self.menusObject_id = menusObject_id
        self.menusObject_name = menusObject_name
    }
    
    required convenience init?(value:JSON) {
        
        self.init(menusObject_enabled : value["enabled"].stringValue,
                  menusObject_id : value["id"].stringValue,
                  menusObject_name : value["name"].stringValue)
        
    }
    
}























