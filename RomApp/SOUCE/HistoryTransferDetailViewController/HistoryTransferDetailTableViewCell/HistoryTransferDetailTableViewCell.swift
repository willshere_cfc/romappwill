//
//  HistoryTransferDetailTableViewCell.swift
//  RomApp
//
//  Created by Administrator on 1/14/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit

class HistoryTransferDetailTableViewCell: UITableViewCell {

    
    @IBOutlet weak var lbl_PhoneNumber: UILabel!
    
    @IBOutlet weak var lbl_Date: UILabel!
    
    @IBOutlet weak var lbl_Price: UILabel!
    
    @IBOutlet weak var lbl_Price_Satang: UILabel!
    
    @IBOutlet weak var img_Type: UIImageView!
    
    
    
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    open override func awakeFromNib() {
    }
    
    open func setup() {
    }
    
    open class func height() -> CGFloat {
        return 80
    }
    
    open func settingCellWithObject(obj:HistoryDetailObject)
    {
        self.lbl_PhoneNumber.text = "\(String().stringFormatPhoneNumber(phoneNumber: "\(obj.historyDetailObject_mobile_b)"))"
        
        self.lbl_Date.text = "\(obj.historyDetailObject_transdate)"
        
        let string = "\(obj.historyDetailObject_amount)"
        
        if string.range(of:".") != nil{
            print("exists")
            
            var token = string.components(separatedBy: ".")
            
            self.lbl_Price.text = token[0]
            self.lbl_Price_Satang.text = ".\(token[1])"
        }
        else
        {
            self.lbl_Price.text = "\(obj.historyDetailObject_amount)"
            self.lbl_Price_Satang.text = ".00"
        }
        
        
        if obj.historyDetailObject_channel == "USSD"
        {
            self.img_Type.image = UIImage(named: "phone_his_ic")
        }
        else if obj.historyDetailObject_channel == "ATM"
        {
            self.img_Type.image = UIImage(named: "atm_his_ic")
        }
        else
        {
            self.img_Type.image = UIImage(named: "web_his_ic")
        }
        
        
    }
    


}
