//
//  PackageInputNumberViewController.swift
//  RomApp
//
//  Created by Administrator on 1/4/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit

class PackageInputNumberViewController: UIViewController {

    @IBOutlet weak var btn_LeftNavbar: UIButton!
    
    @IBOutlet weak var txt_PhoneNumber: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - IBAction
    
    @IBAction func leftNavbarDidSelect(_ sender: Any)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }

    @IBAction func confirmDidSelect(_ sender: Any)
    {
        if (self.txt_PhoneNumber.text?.length)! > 0
        {
            self.pushVC()
        }
    }
    
    //MARK: - Push VC
    func pushVC()
    {
        let storyboard = UIStoryboard(name: "PackageStoryBoard", bundle: nil)
        let packageCagtegoryViewController = storyboard.instantiateViewController(withIdentifier: "PackageCagtegoryViewControllerID") as! PackageCagtegoryViewController
        
        packageCagtegoryViewController.phoneNumber = self.txt_PhoneNumber.text!
        navigationController?.pushViewController(packageCagtegoryViewController, animated: true)
    }
    
    // MARK: - UITEXTFIELD DELEGATE
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.txt_PhoneNumber.resignFirstResponder()
        
        return true
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
