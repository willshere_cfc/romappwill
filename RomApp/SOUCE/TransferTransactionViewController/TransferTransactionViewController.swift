//
//  TransferTransactionViewController.swift
//  RomApp
//
//  Created by Administrator on 1/10/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit

class TransferTransactionViewController: UIViewController , HeaderTransferTransactionViewDelegate{

    var set_expand : NSMutableSet = []
    
    @IBOutlet weak var transferTansionTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func didSelectHeader(section : Int) {
        
        print("did Select \(section)")
        
        switch section {
        case 0:
            
            var containExpand : Bool
            containExpand = self.set_expand .contains(NSNumber(value: section))
            if containExpand
            {
                self.set_expand .remove(NSNumber(value: section))
                let indexSet : IndexSet = [section]
                self.transferTansionTableView .reloadSections(indexSet, with: UITableViewRowAnimation.fade)
            }
            else
            {
                self.set_expand .add(NSNumber(value: section))
                let indexSet : IndexSet = [section]
                self.transferTansionTableView .reloadSections(indexSet, with: UITableViewRowAnimation.fade)
            }
            
            break
        case 1:
            
            var containExpand : Bool
            containExpand = self.set_expand .contains(NSNumber(value: section))
            if containExpand
            {
                self.set_expand .remove(NSNumber(value: section))
                let indexSet : IndexSet = [section]
                self.transferTansionTableView .reloadSections(indexSet, with: UITableViewRowAnimation.fade)
            }
            else
            {
                self.set_expand .add(NSNumber(value: section))
                let indexSet : IndexSet = [section]
                self.transferTansionTableView .reloadSections(indexSet, with: UITableViewRowAnimation.fade)
            }
            
            break
        case 2:
            
            var containExpand : Bool
            containExpand = self.set_expand .contains(NSNumber(value: section))
            if containExpand
            {
                self.set_expand .remove(NSNumber(value: section))
                let indexSet : IndexSet = [section]
                self.transferTansionTableView .reloadSections(indexSet, with: UITableViewRowAnimation.fade)
            }
            else
            {
                self.set_expand .add(NSNumber(value: section))
                let indexSet : IndexSet = [section]
                self.transferTansionTableView .reloadSections(indexSet, with: UITableViewRowAnimation.fade)
            }
            break
        case 3:
            
            var containExpand : Bool
            containExpand = self.set_expand .contains(NSNumber(value: section))
            if containExpand
            {
                self.set_expand .remove(NSNumber(value: section))
                let indexSet : IndexSet = [section]
                self.transferTansionTableView .reloadSections(indexSet, with: UITableViewRowAnimation.fade)
            }
            else
            {
                self.set_expand .add(NSNumber(value: section))
                let indexSet : IndexSet = [section]
                self.transferTansionTableView .reloadSections(indexSet, with: UITableViewRowAnimation.fade)
            }
            break
        case 4:
            
            var containExpand : Bool
            containExpand = self.set_expand .contains(NSNumber(value: section))
            if containExpand
            {
                self.set_expand .remove(NSNumber(value: section))
                let indexSet : IndexSet = [section]
                self.transferTansionTableView .reloadSections(indexSet, with: UITableViewRowAnimation.fade)
            }
            else
            {
                self.set_expand .add(NSNumber(value: section))
                let indexSet : IndexSet = [section]
                self.transferTansionTableView .reloadSections(indexSet, with: UITableViewRowAnimation.fade)
            }
            break
            
        default:
            break
        }
    }

    //MARK: - IBAction

    @IBAction func confirmDidSelect(_ sender: Any)
    {
        _ = navigationController?.popToRootViewController(animated: true)
    }

}

// MARK: - EXTENION

// MARK: - Tableview Delegate

extension TransferTransactionViewController : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let viewHeader:HeaderTransferTransactionView = HeaderTransferTransactionView.init(frame: .init(x: 0.0, y: 0.0, width: 343, height: HeaderTransferTransactionView.height()))
        
        viewHeader .delegate = self
        viewHeader .setHeader(title: ["":""])
        viewHeader .tag = section
        
        return viewHeader
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return HeaderTransferTransactionView.height()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        return 89
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
}

extension TransferTransactionViewController : UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        if self.set_expand .contains(NSNumber.init(value: section))
        {
            return 1
        }
        else
        {
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellIdentifier = "TransferTransactionExpandTableViewCellIdentifier"
        let cell:TransferTransactionExpandTableViewCell = self.transferTansionTableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! TransferTransactionExpandTableViewCell
        
        
        return cell
    }
}
