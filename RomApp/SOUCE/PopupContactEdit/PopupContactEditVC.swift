//
//  DemoPopupViewController1.swift
//  PopupController
//
//  Created by 佐藤 大輔 on 2/4/16.
//  Copyright © 2016 Daisuke Sato. All rights reserved.
//

import UIKit
import PopupController
import JGProgressHUD
import CFAlertViewController

class PopupContactEditVC : UIViewController, PopupContentViewController {
    
    var idContact : String?
    var closeHandler: (() -> Void)?
    var successHandler: (() -> Void)?
    var failHandler: (() -> Void)?

    @IBOutlet weak var button: UIButton! {
        didSet {
            button.layer.borderColor = UIColor(red: 242/255, green: 105/255, blue: 100/255, alpha: 1.0).cgColor
            button.layer.borderWidth = 1.5
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.frame.size = CGSize(width: 300,height: 300)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    class func instance() -> PopupContactEditVC {
        let storyboard = UIStoryboard(name: "PopupContactEditVC", bundle: nil)
        return storyboard.instantiateInitialViewController() as! PopupContactEditVC
    }
    
    func sizeForPopup(_ popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        if (UIDevice().screenType == .iPhone5 || UIDevice().screenType == .iPhone4)
        {
            return CGSize(width: 300,height: 231)
        }
        else
        {
           return CGSize(width: 345,height: 231)
        }
        
    }
    
    @IBAction func didTapCloseButton(_ sender: AnyObject) {
        closeHandler?()
    }


    
    @IBAction func confirmDidSelect(_ sender: Any)
    {
        self.requestServiceDeleteContact()
          closeHandler?()
        
    }
    
    @IBAction func cancleDidSelect(_ sender: Any)
    {
        closeHandler?()

    }
    
    func requestServiceDeleteContact()
    {

        
        
        let parameters: [String: Dictionary] =
            [
                "data":
                    [
                        "id": "\(self.idContact!)",
                    ]
        ]
        
        print("params Delete Contact => \(parameters)")
        
        let strURL = "http://stg-etopupservices.ais.co.th/dev-ROMMobileServiceAPI/ROMService.svc/deleteContractList/"
        
        JGProgressHUD.loadingProgress(view: self.view)
        AFWrapper.requestPOSTURL(strURL, params: parameters as [String : AnyObject]?, headers: nil, success: { (JSONResponse) ->  Void in
            print(JSONResponse)
            
            JGProgressHUD.dismisProgress()
            
            if JSONResponse.count != 0
            {
                if JSONResponse["status"].stringValue == "success"
                {
                    self.alertControllWith(title: "\(JSONResponse["message"].stringValue)", button: "ตกลง" ,typeSuccess: true)
                    
                }
                else
                {
                    self.alertControllWith(title: "\(JSONResponse["message"].stringValue)", button: "ตกลง", typeSuccess: false)
                }
            }
            
            
        }) { (error) -> Void in
            print(error)
            JGProgressHUD.dismisProgress()
            
             self.alertControllWith(title: "\(error)", button: "ตกลง", typeSuccess: false)
        }
    }
    
    //MARK: - Alert
    func alertControllWith(title:String, button:String, typeSuccess:Bool)
    {
        let alert = CFAlertViewController.alert(withTitle: title, message: nil, textAlignment: NSTextAlignment.center, preferredStyle: CFAlertControllerStyle.alert) {
            
            print("Dismiss")
            
            if typeSuccess == true
            {

                self.successHandler?()
             
            }
            else
            {
                self.failHandler?()
            }
            
        }
        
        let actionDefault = CFAlertAction.init(title: button, style: CFAlertActionStyle.default, alignment: CFAlertActionAlignment.justified, color: UIColor().hexStringToUIColor(hex: "#F7941D")) { action  in
            
            
        }
        
        alert .addAction(actionDefault!)
        
        self.present(alert, animated: true, completion: nil)
    }


   
    
}
