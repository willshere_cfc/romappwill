//
//  ReportDetailTableViewCell.swift
//  RomApp
//
//  Created by Administrator on 1/13/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit

class ReportDetailTableViewCell: UITableViewCell {

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    open override func awakeFromNib() {
    }
    
    open func setup() {
    }
    
    open class func height() -> CGFloat {
        return 80
    }
}
