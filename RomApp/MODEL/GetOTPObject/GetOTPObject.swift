//
//  GetOTPObject.swift
//  RomApp
//
//  Created by Phutsita Aphilertrungchut on 1/29/2560 BE.
//  Copyright © 2560 Willshere. All rights reserved.
//

import UIKit
import SwiftyJSON

class GetOTPObject: NSObject {
    let getOTPObject_status : String
    let getOTPObject_message : String
    let getOTPObject_response_code : String
    let getOTPObject_response_message : String
    let getOTPObject_transactionid : String
    let getOTPObject_datetime : String
    
    init(getOTPObject_status : String,
         getOTPObject_message : String,
         getOTPObject_response_code : String,
         getOTPObject_response_message : String,
         getOTPObject_transactionid : String,
         getOTPObject_datetime : String) {
        self.getOTPObject_status = getOTPObject_status
        self.getOTPObject_message = getOTPObject_message
        self.getOTPObject_response_code = getOTPObject_response_code
        self.getOTPObject_response_message = getOTPObject_response_message
        self.getOTPObject_transactionid = getOTPObject_transactionid
        self.getOTPObject_datetime = getOTPObject_datetime
    }
    
    required convenience init?(value:JSON){
        self.init(getOTPObject_status : value["status"].stringValue,
                  getOTPObject_message : value["message"].stringValue,
                  getOTPObject_response_code : value["response_code"].stringValue,
                  getOTPObject_response_message : value["response_message"].stringValue,
                  getOTPObject_transactionid : value["transactionid"].stringValue,
                  getOTPObject_datetime : value["datetime"].stringValue)
    }
}
