//
//  PackageACViewController.swift
//  RomApp
//
//  Created by Administrator on 1/5/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit
import JGProgressHUD
import CFAlertViewController

class PackageACViewController: UIViewController {

    @IBOutlet weak var txt_AgentCode: UITextField!
    
    var phoneNumber : String!
    var packageListObj : PackageListObject?  = nil
    var topupOfferedPackageObj : TopupOfferedPackageObject? = nil
    var sellPackageObj : SellPackageObject? = nil
    
    @IBOutlet weak var lbl_PhoneNumber: UILabel!
    @IBOutlet weak var lbl_PackageName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupUI()
        // Do any additional setup after loading the view.
    }
    
    func setupUI()
    {
        self.lbl_PhoneNumber.text = String().stringFormatPhoneNumber(phoneNumber: "\(self.phoneNumber!)")
        
        if self.packageListObj != nil
        {
            self.lbl_PackageName.text = self.packageListObj!.packageListObject_shortDesc
        }
        else
        {
            self.lbl_PackageName.text = self.topupOfferedPackageObj!.topupOfferedPackageObject_shortDesc
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - SERVICE
    
    func requestServiceSellPackage()
    {
        let defult = UserDefaults.standard
        let dataDF =  defult.value(forKey: "user_phonenumber")
        
         var parameters: [String: Dictionary] = ["":[:]]
        
        if self.packageListObj != nil
        {
           parameters =
                [
                    "data":
                        [
                            "mobileno_a": "\(dataDF!)",
                            "mobileno_b": "\(self.phoneNumber!)",
                            "pin": "\(self.txt_AgentCode.text!)",
                            "package_key": "\(self.packageListObj!.packageListObject_key)"
                    ]
            ]
        }
        else
        {
            parameters =
                [
                    "data":
                        [
                            "mobileno_a": "\(dataDF!)",
                            "mobileno_b": "\(self.phoneNumber!)",
                            "pin": "\(self.txt_AgentCode.text!)",
                            "package_key": "\(self.topupOfferedPackageObj!.topupOfferedPackageObject_packageKey)"
                    ]
            ]
        }
        
        
        let strURL = "http://stg-etopupservices.ais.co.th/dev-ROMMobileServiceAPI/ROMService.svc/Package/"
        
        print("params => \(parameters)")
        
        JGProgressHUD.loadingProgress(view: self.view)
        AFWrapper.requestPOSTURL(strURL, params: parameters as [String : AnyObject]?, headers: nil, success: { (JSONResponse) ->  Void in
            
            print(JSONResponse)
            
            JGProgressHUD.dismisProgress()
            print("Get Main => \(JSONResponse)")
            if JSONResponse.count != 0
            {
                self.sellPackageObj = SellPackageObject.init(value: JSONResponse)
                
                if self.sellPackageObj!.sellPackageObject_status == "success"
                {
                    self.pushVC()
                }
                else
                {
                    self.alertControllWith(title: self.sellPackageObj!.sellPackageObject_message, button: "ตกลง")
                }
            }
            
            
        }) { (error) -> Void in
            print(error)
            JGProgressHUD.dismisProgress()
            self.alertControllWith(title: "\(error.localizedDescription)", button: "ตกลง")
        }
    }
    
    //MARK: - Alert
    func alertControllWith(title:String, button:String)
    {
        let alert = CFAlertViewController.alert(withTitle: title, message: nil, textAlignment: NSTextAlignment.center, preferredStyle: CFAlertControllerStyle.alert) {
            
            print("Dismiss")
            
        }
        
        let actionDefault = CFAlertAction.init(title: button, style: CFAlertActionStyle.default, alignment: CFAlertActionAlignment.justified, color: UIColor().hexStringToUIColor(hex: "#F7941D")) { action  in
            
            
        }
        
        alert .addAction(actionDefault!)
        
        self.present(alert, animated: true, completion: nil)
    }
    

    // MARK: - IBAction
    @IBAction func leftNavbarDidSelect(_ sender: Any)
    {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func confirmDidSelect(_ sender: Any)
    {
       self.requestServiceSellPackage()
    }
    
    //MARK: - Push VC
    
    func pushVC()
    {
        let storyboard = UIStoryboard(name: "PackageStoryBoard", bundle: nil)
        let packageTransactionViewController = storyboard.instantiateViewController(withIdentifier: "PackageTransactionViewControllerID") as! PackageTransactionViewController
        packageTransactionViewController.sellPackageObj = self.sellPackageObj
        
       if self.packageListObj != nil
        {
            packageTransactionViewController.packageListObj = self.packageListObj
        }
        else
        {
            packageTransactionViewController.topupOfferedPackageObj = self.topupOfferedPackageObj
        }
        
        packageTransactionViewController.phoneNumber = self.phoneNumber!
        navigationController?.pushViewController(packageTransactionViewController,
                                                 animated: true)
    }
    
    // MARK: - UITEXTFIELD DELEGATE
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.txt_AgentCode.resignFirstResponder()
        
        return true
    }

}
