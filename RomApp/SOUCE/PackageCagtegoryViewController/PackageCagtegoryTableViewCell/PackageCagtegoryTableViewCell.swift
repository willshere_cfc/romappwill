//
//  PackageCagtegoryTableViewCell.swift
//  RomApp
//
//  Created by Administrator on 1/4/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit

class PackageCagtegoryTableViewCell: UITableViewCell {

    @IBOutlet weak var lbl_Title: UILabel!
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    open override func awakeFromNib() {
    }
    
    open func setup() {
    }
    
    open class func height() -> CGFloat {
        return 65
    }
    
    open func setTitleCell(obj:PackageGroupObject){
        
        self.lbl_Title?.text = obj.packageGroupObject_name
    }


}
