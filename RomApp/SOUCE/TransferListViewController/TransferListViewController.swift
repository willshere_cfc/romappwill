//
//  TransferListViewController.swift
//  RomApp
//
//  Created by Administrator on 1/10/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit

class TransferListViewController: UIViewController {

     var menusName = ["Amada", "Bradpit", "Anda", "Danupol","Sutee"]
     var menusPhone = ["089-435-6764", "089-112-0052","089-112-0052","089-112-0052","086-220-1112"]
    
    @IBOutlet weak var transferListTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK: - IBAction
    
    @IBAction func leftNavbarDidSelect(_ sender: Any)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
}

// MARK: - EXTENION

// MARK: - Tableview Delegate

extension TransferListViewController : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        return MenuTableViewCell.height()
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let storyboard = UIStoryboard(name: "PackageStoryBoard", bundle: nil)
        let packageDetailViewController = storyboard.instantiateViewController(withIdentifier: "PackageDetailViewControllerID") as! PackageDetailViewController
        navigationController?.pushViewController(packageDetailViewController, animated: true)
    }
    
    
}

extension TransferListViewController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return menusName.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellIdentifier = "TransferListTableViewCellIdentifier"
        let cell:TransferListTableViewCell = self.transferListTableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! TransferListTableViewCell
        
        cell .setNameCell(title: menusName[indexPath.row])
        cell .setPhoneCell(title: menusPhone[indexPath.row])
        
        
        return cell
    }
}

