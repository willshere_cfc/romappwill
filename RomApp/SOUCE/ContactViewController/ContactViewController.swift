//
//  ContactViewController.swift
//  RomApp
//
//  Created by Administrator on 1/11/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit
import CFAlertViewController
import PopupController
import JGProgressHUD

enum ContactSelectType {
    case HambergerType
    case MainType
}

class ContactViewController: UIViewController ,HeaderContactViewDelegate{

    var setSelectType:ContactSelectType!
    
    var set_expand : NSMutableSet = []
    
    var getContactObj : GetContactObject? = nil
    
    
    
    @IBOutlet weak var contactTableView: UITableView!
    
    @IBOutlet weak var btn_LeftNavbar: UIButton!
    
 
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         NotificationCenter.default.addObserver(self, selector: #selector(ContactViewController.requestServiceGetContractList), name: Notification.Name("NotificationUpdateContactList"), object: nil)

        for i  in 0...5 {
            
            self.set_expand .add(NSNumber(value: i))
        }
        
         print("set expand \(self.set_expand)")
        
        self.requestServiceGetContractList()
        
       // self.setingSelectType()
        // Do any additional setup after loading the view.
    }
    
    func setingSelectType()
    {
        if setSelectType == .HambergerType
        {
            self.btn_LeftNavbar.setImage(UIImage(named: "side_bar_ic"), for: .normal)
        }
        else if setSelectType == .MainType
        {
            self.btn_LeftNavbar.setImage(UIImage(named: "arrow-left"), for: .normal)
            self.slideMenuController()?.removeLeftGestures()
            
        }
        else {}
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - SERVICE
    
    func requestServiceGetContractList()
    {
        let defult = UserDefaults.standard
        let dataUser_agent_id =  defult.value(forKey: "user_agent_id")
        
        
        let parameters: [String: Dictionary] =
            [
                "data":
                    [
                        "agent_id": dataUser_agent_id!
                    ]
            ]
        
        print("param \(parameters)")
        
        let strURL = "http://stg-etopupservices.ais.co.th/dev-ROMMobileServiceAPI/ROMService.svc/getContractList2/"
        
        JGProgressHUD.loadingProgress(view: self.view)
        AFWrapper.requestPOSTURL(strURL, params: parameters as [String : AnyObject]?, headers: nil, success: { (JSONResponse) ->  Void in
            print(JSONResponse)
            
            JGProgressHUD.dismisProgress()
    
            if JSONResponse.count != 0
            {
                self.getContactObj = nil
                self.getContactObj = GetContactObject.init(value: JSONResponse)
                
                
                self.contactTableView.reloadData()
            }
            
            
        }) { (error) -> Void in
            print(error)
            JGProgressHUD.dismisProgress()
        }
    }
    
    //MARK: -Service
    func requestServiceInsertUpdateContact(obj:ContactListObject)
    {
        
        var fav : String = ""
        
        if obj.contactListObject_favorite == "Y"
        {
            fav = "N"
        }
        else
        {
            fav = "Y"
        }
        
        let parameters: [String: Dictionary] =
            [
                "data":
                    [
                        "id": "\(obj.contactListObject_id)",
                        "agent_id": "\(obj.contactListObject_agent_id)",
                        "mobile": "\(obj.contactListObject_mobile)",
                        "first_name": "\(obj.contactListObject_first_name)",
                        "last_name": "\(obj.contactListObject_last_name)",
                        "nick_name": "\(obj.contactListObject_nick_name)",
                        "favorite": "\(fav)"
                ]
        ]
        
        print("params fav contact => \(parameters)")
        
        let strURL = "http://stg-etopupservices.ais.co.th/dev-ROMMobileServiceAPI/ROMService.svc/insertupdateContractList/"
        
        JGProgressHUD.loadingProgress(view: self.view)
        AFWrapper.requestPOSTURL(strURL, params: parameters as [String : AnyObject]?, headers: nil, success: { (JSONResponse) ->  Void in
            print(JSONResponse)
            
            JGProgressHUD.dismisProgress()
            
            if JSONResponse.count != 0
            {
                if JSONResponse["status"].stringValue == "success"
                {
//                    if JSONResponse["status"].stringValue == "Y"
//                    {
//                        
//                    }
//                    else
//                    {
//                        
//                    }
                    
                    self.requestServiceGetContractList()
                }
                else
                {
                    self.alertControllWith(title: "\(JSONResponse["message"].stringValue)", button: "ตกลง", typeSuccess: false)
                }
            }
            
            
        }) { (error) -> Void in
            print(error)
            JGProgressHUD.dismisProgress()
        }
    }
    
    //MARK: - Alert
    func alertControllWith(title:String, button:String, typeSuccess:Bool)
    {
        let alert = CFAlertViewController.alert(withTitle: title, message: nil, textAlignment: NSTextAlignment.center, preferredStyle: CFAlertControllerStyle.alert) {
            
            print("Dismiss")
            
            if typeSuccess == true
            {
                
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NotificationUpdateContactList"), object: nil)
                
                _ = self.navigationController?.popToRootViewController(animated: true)
            }
            
        }
        
        let actionDefault = CFAlertAction.init(title: button, style: CFAlertActionStyle.default, alignment: CFAlertActionAlignment.justified, color: UIColor().hexStringToUIColor(hex: "#F7941D")) { action  in
            
            
        }
        
        alert .addAction(actionDefault!)
        
        self.present(alert, animated: true, completion: nil)
    }


    
    //MARK: - IBAction
   
    @IBAction func leftNavbarDidSelect(_ sender: Any)
    {
        if setSelectType == .HambergerType
        {
            self.slideMenuController()?.openLeft()
        }
        else if setSelectType == .MainType
        {
            _ = self.navigationController?.popViewController(animated: true)
        }
        else {}
    }

    
    @IBAction func addContactDidSelect(_ sender: Any)
    {
        let popup = PopupController
            .create(self)
            .customize(
                [
                    .layout(.center),
                    .animation(.fadeIn),
                    .backgroundStyle(.blackFilter(alpha: 0.8)),
                    .dismissWhenTaps(true),
                    .scrollable(true)
                ]
            )
            .didShowHandler { popup in
                print("showed popup!")
            }
            .didCloseHandler { popup in
                print("closed popup!")
        }
        
        let container = PopupAlertViewController.instance()
        container.closeHandler = { _ in
            popup.dismiss()
        }
        
        popup.show(container)

    }

    func didSelectHeader(section : Int) {
        
        print("did Select \(section)")
        
        switch section {
        case section:
            
            var containExpand : Bool
            containExpand = self.set_expand .contains(NSNumber(value: section))
            if containExpand
            {
                self.set_expand .remove(NSNumber(value: section))
                let indexSet : IndexSet = [section]
                self.contactTableView .reloadSections(indexSet, with: UITableViewRowAnimation.fade)
            }
            else
            {
                self.set_expand .add(NSNumber(value: section))
                let indexSet : IndexSet = [section]
                self.contactTableView .reloadSections(indexSet, with: UITableViewRowAnimation.fade)
            }
            
            break
        default:
            break
        }

}
    
}

// MARK: - EXTENION

// MARK: - Tableview Delegate

extension ContactViewController : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let viewHeader:HeaderContactView = HeaderContactView.init(frame: .init(x: 0.0, y: 0.0, width: 343, height: HeaderContactView.height()))
        
        viewHeader .delegate = self
        viewHeader .setHeader(title: ["":""])
        viewHeader .tag = section
        
        
        let titleLabel = UILabel.init(frame: .init(x: 24.0, y: 5.0, width: 200.0, height: 20.0))
        
        titleLabel.textColor = UIColor.white
        titleLabel.font = UIFont(name: "DBHelvethaicaMonX-Med", size: 21)
        
        
        if section == 0
        {
            titleLabel.text = "รายการโปรด"
            viewHeader.backgroundColor = UIColor().hexStringToUIColor(hex: "#82BF41")
        }
        else
        {
            let contact = ContactObject.init(value: self.getContactObj!.getContactObject_contract_l[section-1])
            
            titleLabel.text = "\(contact!.contactObject_text_drop)"
            viewHeader.backgroundColor = UIColor().hexStringToUIColor(hex: "#CDCDCD")
        }
        
        viewHeader .addSubview(titleLabel)
        
        return viewHeader
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return HeaderContactView.height()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        return 60
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let storyboard = UIStoryboard(name: "ContactStoryboard", bundle: nil)
        let contactDetailViewController = storyboard.instantiateViewController(withIdentifier: "ContactDetailViewControllerID") as! ContactDetailViewController
        
        
        if indexPath.section == 0
        {
            let contactList = ContactListObject.init(value: self.getContactObj!.getContactObject_contract_f[indexPath.row])
           
            contactDetailViewController.contactListObj = contactList
            
          
        }
        else
        {
            let contact = ContactObject.init(value: self.getContactObj!.getContactObject_contract_l[indexPath.section-1])
            
            let contactList = ContactListObject.init(value: contact!.contactObject_contract_drop[indexPath.row])
            
            contactDetailViewController.contactListObj = contactList
            
          
        }
      
        
        navigationController?.pushViewController(contactDetailViewController,
                                                 animated: true)
        
    }
    
}

extension ContactViewController : UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
       
     
        
        if self.getContactObj != nil
        {
            return  1 + self.getContactObj!.getContactObject_contract_l.count
        }
        else
        {
            return 0
        }
      
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        if self.set_expand .contains(NSNumber.init(value: section))
        {
            if section == 0
            {
               
               return self.getContactObj!.getContactObject_contract_f.count
            }
            else
            {
                let contact = ContactObject.init(value: self.getContactObj!.getContactObject_contract_l[section-1])
                
               return contact!.contactObject_contract_drop.count
            }
            
        }
        else
        {
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellIdentifier = "ContactTableViewCellIdentifier"
        let cell:ContactTableViewCell = self.contactTableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! ContactTableViewCell
        
        cell .delegate = self
        cell .tag = indexPath.row
        cell .secion = indexPath.section
        
        if indexPath.section == 0
        {
            cell .setButtonFavoriteShow()
            
            let contactList = ContactListObject.init(value: self.getContactObj!.getContactObject_contract_f[indexPath.row])

            
            cell .settingContactCellWithObject(obj: contactList!)
            
        }
        else
        {
            cell .setButtonFavoriteHiden()
            
            let contact = ContactObject.init(value: self.getContactObj!.getContactObject_contract_l[indexPath.section-1])
            
            let contactList = ContactListObject.init(value: contact!.contactObject_contract_drop[indexPath.row])
            
            
            cell .settingContactCellWithObject(obj: contactList!)
        }
        
        return cell
    }
}

  //MARK: - CONTACT  DELEGATE
extension ContactViewController : ContectDelegate {
    
  
    func contactDidSelect(row:Int, buttonImg:UIButton , secion:Int) {
        
        print("row \(row) secion \(secion)")
        
        if secion == 0
        {
            let contactList = ContactListObject.init(value: self.getContactObj!.getContactObject_contract_f[row])
            
            print("name fav \(contactList!.contactListObject_first_name)")
            
            self.requestServiceInsertUpdateContact(obj: contactList!)
        }
        else
        {
            let contact = ContactObject.init(value: self.getContactObj!.getContactObject_contract_l[secion-1])
            
            let contactList = ContactListObject.init(value: contact!.contactObject_contract_drop[row])
            
            print("name un fav \(contactList!.contactListObject_first_name)")

            self.requestServiceInsertUpdateContact(obj: contactList!)
        }

    }

}






















