//
//  VerifyOTPObject.swift
//  RomApp
//
//  Created by Phutsita Aphilertrungchut on 1/29/2560 BE.
//  Copyright © 2560 Willshere. All rights reserved.
//

import UIKit
import SwiftyJSON

class VerifyOTPObject: NSObject {
    let verifyOTPObject_status : String
    let verifyOTPObject_message : String
    let verifyOTPObject_response_code : String
    let verifyOTPObject_response_message : String
    let verifyOTPObject_transactionid : String
    let verifyOTPObject_datetime : String
    
    init(verifyOTPObject_status : String,
         verifyOTPObject_message : String,
         verifyOTPObject_response_code : String,
         verifyOTPObject_response_message : String,
         verifyOTPObject_transactionid : String,
         verifyOTPObject_datetime : String){
        self.verifyOTPObject_status = verifyOTPObject_status
        self.verifyOTPObject_message = verifyOTPObject_message
        self.verifyOTPObject_response_code = verifyOTPObject_response_code
        self.verifyOTPObject_response_message = verifyOTPObject_response_message
        self.verifyOTPObject_transactionid = verifyOTPObject_transactionid
        self.verifyOTPObject_datetime = verifyOTPObject_datetime
    }
    
    required convenience init?(value:JSON){
        self.init(verifyOTPObject_status : value["status"].stringValue,
                  verifyOTPObject_message : value["message"].stringValue,
                  verifyOTPObject_response_code : value["response_code"].stringValue,
                  verifyOTPObject_response_message : value["response_message"].stringValue,
                  verifyOTPObject_transactionid : value["transactionid"].stringValue,
                  verifyOTPObject_datetime : value["datetime"].stringValue)
    }
}
