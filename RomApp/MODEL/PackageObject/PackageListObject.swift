//
//  PackageListObject.swift
//  RomApp
//
//  Created by Chanachon Pruchayahmaytha on 1/31/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit
import SwiftyJSON

class GetPackageListObject: NSObject {

    
    let getPackageListObject_status : String
    let getPackageListObject_message: String
    let getPackageListObject_responseCode : String
    let getPackageListObject_responseMessage : String
    let getPackageListObject_datetime : String
    let getPackageListObject_package:JSON
    
    init(
        getpackagelistobject_status : String,
        getpackagelistobject_message : String,
        getpackagelistobject_responseCode : String,
        getpackagelistobject_responseMessage:String,
        getpackagelistobject_datetime:String,
        getpackagelistobject_package:JSON
        ){
        self.getPackageListObject_status = getpackagelistobject_status
        self.getPackageListObject_message = getpackagelistobject_message
        self.getPackageListObject_responseCode = getpackagelistobject_responseCode
        self.getPackageListObject_responseMessage = getpackagelistobject_responseMessage
        self.getPackageListObject_datetime = getpackagelistobject_datetime
        self.getPackageListObject_package = getpackagelistobject_package
    }
    
    required convenience init?(value:JSON){
        self.init(getpackagelistobject_status : value["status"].stringValue,
                  getpackagelistobject_message : value["message"].stringValue,
                  getpackagelistobject_responseCode : value["response_code"].stringValue,
                  getpackagelistobject_responseMessage:value["response_message"].stringValue,
                  getpackagelistobject_datetime:value["datetime"].stringValue,
                  getpackagelistobject_package:value["package"]
        )
    }
}

class PackageListObject: NSObject {
    
    let packageListObject_key : String
    let packageListObject_code : String
    let packageListObject_name : String
    let packageListObject_price : String
    let packageListObject_desc : String
    let packageListObject_shortDesc : String
    let pacakgeListObject_title : String
    
    init(packagelistobject_key:String,
         packagelistobject_code:String,
         packagelistobject_name:String,
         packagelistobject_price:String,
         packagelistobject_desc:String,
         packagelistobject_shortDesc:String,
         packagelistobject_title:String){
        self.packageListObject_key = packagelistobject_key
        self.packageListObject_code = packagelistobject_code
        self.packageListObject_name = packagelistobject_name
        self.packageListObject_price = packagelistobject_price
        self.packageListObject_desc = packagelistobject_desc
        self.packageListObject_shortDesc = packagelistobject_shortDesc
        self.pacakgeListObject_title = packagelistobject_title
    }
    
    required convenience init?(value:JSON){
        self.init(packagelistobject_key:value["package_key"].stringValue,
                  packagelistobject_code:value["package_code"].stringValue,
                  packagelistobject_name:value["package_name_display"].stringValue,
                  packagelistobject_price:value["package_price"].stringValue,
                  packagelistobject_desc:value["package_desc_display"].stringValue,
                  packagelistobject_shortDesc:value["package_desc_short"].stringValue,
                  packagelistobject_title:value["package_title"].stringValue)
    }

}


