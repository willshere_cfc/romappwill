//
//  ProfileViewController.swift
//  RomApp
//
//  Created by Administrator on 1/10/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit
import SwiftyJSON
import JGProgressHUD

class ProfileViewController: UIViewController {

    weak var delegate: MenuHembergerProtocol?
    

    
    var menus: NSMutableArray = []
    
    var menusTitle : Array = ["ที่อยู่ที่ติดต่อได้","วัน/เดือน/ปีเกิด","ID Card","เบอร์ที่ทำรายการ","เบอร์ที่ติดต่อ","อาชีพ","ที่อยู่ที่ติดต่อได้","อีเมล์","เอกสารอัพโหลด"]
    
    @IBOutlet weak var profileTableView: UITableView!
    
    
    @IBOutlet weak var img_Profile: CustomizableImage!
    
    @IBOutlet weak var lbl_Name: UILabel!
    
    @IBOutlet weak var lbl_NickName: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.profileTableView.rowHeight = UITableViewAutomaticDimension
        self.profileTableView.estimatedRowHeight = 38
        
        // Do any additional setup after loading the view.
        
        self.requestServiceGetAbout()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: IBAction

    @IBAction func leftNavbarDidSelect(_ sender: Any)
    {
        self.slideMenuController()?.openLeft()
    }
    
    @IBAction func closeDidSelect(_ sender: Any)
    {
        delegate?.changeViewController(MenuHemberger.main)
    }
    
    //MARK GetAbout Profile
    func requestServiceGetAbout(){
        let defult = UserDefaults.standard
        let dataDF = defult.value(forKey: "user_agent_id")
        
        let strURL = "http://stg-etopupservices.ais.co.th/dev-ROMMobileServiceAPI/ROMService.svc/GetAbout/"
        
        let parameters: [String: Dictionary] =
            [
                "data":
                    [
                        "agent_id": dataDF!
                ]
        ]
        
        JGProgressHUD.loadingProgress(view: self.view)
        AFWrapper.requestPOSTURL(strURL, params:parameters as [String : AnyObject]?, headers: nil, success: { (JSONResponse) ->  Void in
            print(JSONResponse)
            if JSONResponse.count != 0
            {
                let aboutObj : GetAboutObject = GetAboutObject.init(value: JSONResponse)!
                
                if(aboutObj.getAboutObject_status == "success"){
                    

                    
                    self.lbl_Name.text = "\(aboutObj.getAboutObject_firstnName) \(aboutObj.getAboutObject_lastName)"
                    
                    self.lbl_NickName.text = "\(aboutObj.getAboutObject_shortName)"
                    
                    self.menus.add(" \(aboutObj.getAboutObject_officialAddress)")
                    self.menus.add(" \(aboutObj.getAboutObject_birthdate)")
                    self.menus.add(" \(aboutObj.getAboutObject_idCard)")
                    self.menus.add(" \(aboutObj.getAboutObject_agentMobile)")
                    self.menus.add(" \(aboutObj.getAboutObject_contactMobile)")
                    self.menus.add(" \(aboutObj.getAboutObject_occupation)")
                    self.menus.add(" \(aboutObj.getAboutObject_contactMobile)")
                    self.menus.add(" \(aboutObj.getAboutObject_email)")
                    self.menus.add("-")
            
                    
                
                    self.profileTableView.reloadData()
                }
            }
            
            JGProgressHUD.dismisProgress()
            
            
        }) { (error) -> Void in
            print(error)
            JGProgressHUD.dismisProgress()
        }
        
    }

 
    
    
}

// MARK: - EXTENION

// MARK: - Tableview Delegate

extension ProfileViewController : UITableViewDelegate
{
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
    
}

extension ProfileViewController : UITableViewDataSource
{


    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return menus.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellIdentifier = "ProfileTableViewCellIdentifier"
        let cell:ProfileTableViewCell = self.profileTableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! ProfileTableViewCell
        
        cell .setTitleCell(title: menusTitle[indexPath.row])
        cell .setDetailCell(title: menus[indexPath.row] as! String)
        
        
        return cell
    }
}

