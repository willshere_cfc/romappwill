//
//  NewsObject.swift
//  RomApp
//
//  Created by Administrator on 1/26/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit
import SwiftyJSON

class NewsObject: NSObject {

     let newsObject_status : String
     let newsObject_response_code : String
     let newsObject_transactionid : String
     let newsObject_response_message : String
     let newsObject_message : String
     let newsObject_datetime : String
     let newsObject_news : JSON
    
    init(newsObject_status : String,
     newsObject_response_code : String,
     newsObject_transactionid : String,
     newsObject_response_message : String,
     newsObject_message : String,
     newsObject_datetime : String,
     newsObject_news : JSON)
    {
        self.newsObject_status = newsObject_status
        self.newsObject_response_code = newsObject_response_code
        self.newsObject_transactionid = newsObject_transactionid
        self.newsObject_response_message = newsObject_response_message
        self.newsObject_message = newsObject_message
        self.newsObject_datetime = newsObject_datetime
        self.newsObject_news = newsObject_news
    }
    
    required convenience init?(value:JSON) {
        
        self.init(newsObject_status:value["status"].stringValue,
                  newsObject_response_code:value["response_code"].stringValue,
                  newsObject_transactionid:value["transactionid"].stringValue,
                  newsObject_response_message:value["response_message"].stringValue,
                  newsObject_message:value["message"].stringValue,
                  newsObject_datetime:value["datetime"].stringValue,
                  newsObject_news:value["news"])
    }

}

class NewsListObject: NSObject {
    
    let newsListObject_downloadlink : String
    let newsListObject_id : String
    let newsListObject_isread : String
    let newsListObject_newsurl : String
    let newsListObject_thumbimage : String
    let newsListObject_title : String
    
    init(newsListObject_downloadlink : String,
     newsListObject_id : String,
     newsListObject_isread : String,
     newsListObject_newsurl : String,
     newsListObject_thumbimage : String,
     newsListObject_title : String)
    {
        self.newsListObject_downloadlink = newsListObject_downloadlink
        self.newsListObject_id = newsListObject_id
        self.newsListObject_isread = newsListObject_isread
        self.newsListObject_newsurl = newsListObject_newsurl
        self.newsListObject_thumbimage = newsListObject_thumbimage
        self.newsListObject_title = newsListObject_title
    }
    
    required convenience init?(value:JSON) {
        
        self.init(newsListObject_downloadlink:value["downloadlink"].stringValue,
                  newsListObject_id:value["id"].stringValue,
                  newsListObject_isread:value["isread"].stringValue,
                  newsListObject_newsurl:value["newsurl"].stringValue,
                  newsListObject_thumbimage:value["thumbimage"].stringValue,
                  newsListObject_title:value["title"].stringValue)
    }
 
}
























