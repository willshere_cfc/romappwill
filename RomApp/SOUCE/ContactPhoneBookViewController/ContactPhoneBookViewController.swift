//
//  ContactPhoneBookViewController.swift
//  RomApp
//
//  Created by Administrator on 1/12/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit
import APAddressBook
import JGProgressHUD
import CFAlertViewController


class ContactPhoneBookViewController: UIViewController ,ContectPhoneBookDelegate{

    let addressBook = APAddressBook()
    var contacts = [APContact]()
    
    var containCheckBox : Bool = true
    
    @IBOutlet weak var contactPhoneBookTableView: UITableView!
    
     var set_select : NSMutableSet = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadContacts()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadContacts()
    {
        
        addressBook.loadContacts
            {
                [unowned self] (contacts: [APContact]?, error: Error?) in
                
                self.contacts = [APContact]()
                if let contacts = contacts
                {
                    self.contacts = contacts

                    self.contactPhoneBookTableView.reloadData()
                    
                }
                else if let error = error
                {
                    let alert = UIAlertView(title: "Error", message: error.localizedDescription,
                                            delegate: nil, cancelButtonTitle: "OK")
                    alert.show()
                }
        }
    }
    
    //MARK: - IBAction
    
    @IBAction func leftNavbarDidSelect(_ sender: Any)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
    @IBAction func confirmDidSelect(_ sender: Any)
    {
        if set_select.count > 0
        {
             JGProgressHUD.loadingProgress(view: self.view)
            var nInt : Int
            for number in self.set_select {
                
                nInt = number as! Int
                
                
                print("contactFirstName \(self.contactFirstName(contacts[nInt]))")
                
                self.requestServiceInsertUpdateContact(phone: "\(self.contactPhones(contacts[nInt]))",
                    firstName: "\(self.contactFirstName(contacts[nInt]))",
                    lastName: "\(self.contactLastName(contacts[nInt]))",
                    nickName: "\(self.contactFirstName(contacts[nInt]))")
                
            }
            
            JGProgressHUD.dismisProgress()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NotificationUpdateContactList"), object: nil)
            _ = self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    @IBAction func selectAllDidSelect(_ sender: Any)
    {
        for i  in 0...contacts.count-1 {
            
                self.set_select .add(NSNumber(value: i))
        }
        
            self.contactPhoneBookTableView.reloadData()
    }
    
    //MARK: - CONTACT PHONE BOOK DELEGATE
    
    func contactDidSelect(row:Int, buttonImg:UIButton) {
        
 
            containCheckBox = self.set_select .contains(NSNumber(value: row))
            if containCheckBox
            {
                self.set_select .remove(NSNumber(value: row))
  
              
                buttonImg .setBackgroundImage(UIImage.init(named: "checkbox-uncheck"), for: .normal)
            }
            else
            {
                self.set_select .add(NSNumber(value: row))

               
                buttonImg .setBackgroundImage(UIImage.init(named: "checkbox-check"), for: .normal)
                
                
                print("contacts Last name \(self.contactLastName(contacts[row]))")
                print("contacts Phones \(self.contactPhones(contacts[row]))")
                
            }
        
            self.mutableSet(set: self.set_select)

            self.contactPhoneBookTableView.reloadData()

    }
    
    func mutableSet(set:NSMutableSet)
    {
        print("set \(set)")
         print("set count \(set.count)")
        
        var nInt : Int
        for number in set {
            
            
            nInt = number as! Int

            
            print("contactFirstName \(self.contactFirstName(contacts[nInt]))")
        }
    }
    
    func contactFirstName(_ contact :APContact) -> String {
        if let firstName = contact.name?.firstName {
            return "\(firstName)"
        }
        else {
            return ""
        }
    }
    
    func contactLastName(_ contact :APContact) -> String {
         if let lastName = contact.name?.lastName {
            return "\(lastName)"
        }
        else {
            return ""
        }
    }
    
    
    
    func contactPhones(_ contact :APContact) -> String {
        if let phones = contact.phones?[0] {
            var phonesString = ""
            var replaced = ""
                if let number = phones.number {
                    phonesString = phonesString + "" + number
                
                    let string = phonesString
                    replaced = (string as NSString).replacingOccurrences(of: "-", with: "")
            }
            return replaced
        }
        return "No phone"
    }
    
    //MARK: -Service
    func requestServiceInsertUpdateContact(phone:String, firstName:String, lastName:String, nickName:String)
    {
        let defult = UserDefaults.standard
        let dataUser_agent_id =  defult.value(forKey: "user_agent_id")
        
        
        let parameters: [String: Dictionary] =
            [
                "data":
                    [
                        "id": "",
                        "agent_id": "\(dataUser_agent_id!)",
                        "mobile": "\(phone)",
                        "first_name": "\(firstName)",
                        "last_name": "\(lastName)",
                        "nick_name": "\(nickName)",
                        "favorite": "N"
                ]
        ]
        
        print("params add contact list => \(parameters)")
        
        let strURL = "http://stg-etopupservices.ais.co.th/dev-ROMMobileServiceAPI/ROMService.svc/insertupdateContractList/"
        
       
        AFWrapper.requestPOSTURL(strURL, params: parameters as [String : AnyObject]?, headers: nil, success: { (JSONResponse) ->  Void in
            print(JSONResponse)
            
            
            
            if JSONResponse.count != 0
            {
                if JSONResponse["status"].stringValue == "success"
                {
//                    self.alertControllWith(title: "\(JSONResponse["message"].stringValue)", button: "ตกลง" ,typeSuccess: true)
                    
                }
                else
                {
//                    self.alertControllWith(title: "\(JSONResponse["message"].stringValue)", button: "ตกลง", typeSuccess: false)
                }
            }
            
            
        }) { (error) -> Void in
            print(error)
         
        }
    }
    
    //MARK: - Alert
    func alertControllWith(title:String, button:String, typeSuccess:Bool)
    {
        let alert = CFAlertViewController.alert(withTitle: title, message: nil, textAlignment: NSTextAlignment.center, preferredStyle: CFAlertControllerStyle.alert) {
            
            print("Dismiss")
            
            if typeSuccess == true
            {
                
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NotificationUpdateContactList"), object: nil)
                
                _ = self.navigationController?.popToRootViewController(animated: true)
            }
            
        }
        
        let actionDefault = CFAlertAction.init(title: button, style: CFAlertActionStyle.default, alignment: CFAlertActionAlignment.justified, color: UIColor().hexStringToUIColor(hex: "#F7941D")) { action  in
            
            
        }
        
        alert .addAction(actionDefault!)
        
        self.present(alert, animated: true, completion: nil)
    }

}

// MARK: - EXTENION

// MARK: - Tableview Delegate

extension ContactPhoneBookViewController : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return ContactPhoneBookTableViewCell.height()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        self.contactDidSelect(row:indexPath.row, buttonImg:UIButton.init())
    }
    
    
}

extension ContactPhoneBookViewController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.contacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellIdentifier = "ContactPhoneBookTableViewCellIdentifier"
        let cell:ContactPhoneBookTableViewCell = self.contactPhoneBookTableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! ContactPhoneBookTableViewCell
        
        cell .delegate = self
        cell .tag = indexPath.row
        
        cell.updateContact(with: contacts[indexPath.row])

       if  self.set_select .contains(NSNumber(value: indexPath.row)) == true
       {
         cell.btn_SelectListContact .setBackgroundImage(UIImage.init(named: "checkbox-check"), for: .normal)
       }
        else
       {
        cell.btn_SelectListContact .setBackgroundImage(UIImage.init(named: "checkbox-uncheck"), for: .normal)
        }
        
        
        return cell
    }
}

