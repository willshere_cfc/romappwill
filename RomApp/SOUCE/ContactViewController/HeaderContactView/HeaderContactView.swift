//
//  HeaderContactView.swift
//  RomApp
//
//  Created by Administrator on 1/11/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit

protocol HeaderContactViewDelegate  {
    
    func didSelectHeader(section : Int)
}

class HeaderContactView: UIView {

    var delegate : HeaderContactViewDelegate?
    
    override init(frame: CGRect) {
        
        super.init(frame : frame)
        let view = Bundle.main.loadNibNamed("HeaderContactView", owner: nil, options: nil)?[0]
        self .addSubview(view as! UIView)
        self .autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    open class func height() -> CGFloat {
        return 30
    }
    
    open  func setHeader(title:Dictionary<String, String>){
        
        let tap = UITapGestureRecognizer(target: self, action:#selector(HeaderContactView.headerDidSelect(_:)))
        
        self .addGestureRecognizer(tap)
        
    }
    
    @IBAction func headerDidSelect(_ sender: Any)
    {
        self.delegate? .didSelectHeader(section: self.tag)
        
    }

}
