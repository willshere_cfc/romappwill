//
//  ReportListViewController.swift
//  RomApp
//
//  Created by Administrator on 1/13/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit

enum ReportType {
    case Topup
    case SellPack
    case SellPin
    case Transfer
    
}

enum ReportSelectType {
    case HambergerType
    case MainType
}

class ReportListViewController: UIViewController {

    var setType:ReportType!
    var setSelectType:ReportSelectType!
    
    
    @IBOutlet weak var reportListTableView: UITableView!
    
    @IBOutlet weak var btn_LeftNavbar: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setingSelectType()
//        self.setType = .SellPin
        
        // Do any additional setup after loading the view.
    }
    
    func setingSelectType()
    {
        if setSelectType == .HambergerType
        {
            self.btn_LeftNavbar.setImage(UIImage(named: "side_bar_ic"), for: .normal)
            
           
        }
        else if setSelectType == .MainType
        {
            self.btn_LeftNavbar.setImage(UIImage(named: "arrow-left"), for: .normal)
            self.slideMenuController()?.removeLeftGestures()
        }
        else {}
    }


    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: IBAction
    
    @IBAction func leftNavbarDidSelect(_ sender: Any)
    {
        if setSelectType == .HambergerType
        {
            self.slideMenuController()?.openLeft()
        }
        else if setSelectType == .MainType
        {
            _ = self.navigationController?.popViewController(animated: true)
            
        }
        else {}
    }
}

// MARK: - EXTENION

// MARK: - Tableview Delegate

extension ReportListViewController : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        return ReportListTableViewCell.height()
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let storyboard = UIStoryboard(name: "ReportStoryboard", bundle: nil)
        let reportDetailViewController = storyboard.instantiateViewController(withIdentifier: "ReportDetailViewControllerID") as! ReportDetailViewController
        
        if indexPath.row == 0
        {
            reportDetailViewController .setType = .Topup
            
            navigationController?.pushViewController(reportDetailViewController, animated: true)
        }
        else if indexPath.row == 1
        {
            reportDetailViewController .setType = .SellPack
            
            navigationController?.pushViewController(reportDetailViewController, animated: true)
        }
        else if indexPath.row == 2
        {
            reportDetailViewController .setType = .SellPin
        }
        else if indexPath.row == 3
        {
            reportDetailViewController .setType = .Transfer
            
            navigationController?.pushViewController(reportDetailViewController, animated: true)
        }
        else{}
        
        
    }
    
    
}

extension ReportListViewController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellIdentifier = "ReportListTableViewCellIdentifier"
        let cell:ReportListTableViewCell = self.reportListTableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! ReportListTableViewCell
        
//        cell .setNameCell(title: menusName[indexPath.row])
//        cell .setPhoneCell(title: menusPhone[indexPath.row])
        
        
        return cell
    }
}

