//
//  CustomizableView.swift
//  RomApp
//
//  Created by Administrator on 1/2/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit

@IBDesignable class CustomizableView: UIView {

    @IBInspectable var cornerRadius : CGFloat = 0 {
        
        didSet {
            
            
            layer.cornerRadius = cornerRadius
        }
    }

}
