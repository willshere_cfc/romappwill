//
//  TopupObject.swift
//  RomApp
//
//  Created by Chanachon Pruchayahmaytha on 1/30/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit
import SwiftyJSON



class TopUpObject: NSObject {
    
    let topupObject_status : String
    let topupObject_message : String
    let topupObject_responseCode : String
    let topupObject_transaction_date : String
    let topupObject_transaction_id : String
    let topupObject_sellpackBenefit : String
    let topupObject_offerPackage : JSON
    let topupObject_topsellPackage : JSON
    
    init(topupObject_status:String,
         topupObject_message:String,
         topupObject_responseCode : String,
         topupObject_transactiondate:String,
         topupObject_transactionid:String,
         topupObject_sellpackbenefit:String,
         topupobject_offerpackage : JSON,
         topupobject_topsellPacakge: JSON){
        self.topupObject_status = topupObject_status
        self.topupObject_message = topupObject_message
        self.topupObject_responseCode = topupObject_responseCode
        self.topupObject_transaction_date = topupObject_transactiondate
        self.topupObject_transaction_id = topupObject_transactionid
        self.topupObject_sellpackBenefit = topupObject_sellpackbenefit
        self.topupObject_offerPackage = topupobject_offerpackage
        self.topupObject_topsellPackage = topupobject_topsellPacakge
        
    }
    
    required convenience init?(value:JSON){
        self.init(topupObject_status:value["status"].stringValue,
                  topupObject_message:value["message"].stringValue,
                  topupObject_responseCode:value["response_code"].stringValue,
                  topupObject_transactiondate:value["datetime"].stringValue,
                  topupObject_transactionid:value["transactionid"].stringValue,
                  topupObject_sellpackbenefit:value["package_benefit"].stringValue,
                  topupobject_offerpackage:value["package_offer"],
                  topupobject_topsellPacakge:value["package_other"])
    }
    
}

class TopupOfferedPackageObject : NSObject{
    let topupOfferedPackageObject_code : String
    let topupOfferedPackageObject_imageUrl : String
    let topupOfferedPackageObject_name : String
    let topupOfferedPackageObject_title : String
    let topupOfferedPackageObject_amount : String
    let topupOfferedPackageObject_shortDesc : String
    let topupOfferedPackageObject_longDesc : String
    let topupOfferedPackageObject_categoryCode : String
    let topupOfferedPackageObject_packageKey : String
    
    init(topupofferedpackageobject_code : String,
         topupofferedpackageobject_imageUrl : String,
         topupofferedpackageobject_name : String,
         topupofferedpackageobject_title: String,
         topupofferedpackageobject_amount : String,
         topupofferedpackageobject_shortDesc : String,
         topupofferedpackageobject_longDesc : String,
         topupofferedpackageobject_categoryCode:String,
         topupofferedpackageobject_packageKey:String){
        
        self.topupOfferedPackageObject_code = topupofferedpackageobject_code
        self.topupOfferedPackageObject_imageUrl = topupofferedpackageobject_imageUrl
        self.topupOfferedPackageObject_name = topupofferedpackageobject_name
        self.topupOfferedPackageObject_title = topupofferedpackageobject_title
        self.topupOfferedPackageObject_amount = topupofferedpackageobject_amount
        self.topupOfferedPackageObject_shortDesc = topupofferedpackageobject_shortDesc
        self.topupOfferedPackageObject_longDesc = topupofferedpackageobject_longDesc
        self.topupOfferedPackageObject_categoryCode = topupofferedpackageobject_categoryCode
        self.topupOfferedPackageObject_packageKey = topupofferedpackageobject_packageKey
    }
    
    required convenience init?(value:JSON){
        self.init(topupofferedpackageobject_code : value["package_code"].stringValue,
                  topupofferedpackageobject_imageUrl : value["package_img_url"].stringValue,
                  topupofferedpackageobject_name : value["package_name"].stringValue,
                  topupofferedpackageobject_title: value["package_title"].stringValue,
                  topupofferedpackageobject_amount : value["package_amount"].stringValue,
                  topupofferedpackageobject_shortDesc : value["package_desc_short"].stringValue,
                  topupofferedpackageobject_longDesc : value["package_desc"].stringValue,
                  topupofferedpackageobject_categoryCode:value["category_code"].stringValue,
                  topupofferedpackageobject_packageKey:value["package_key"].stringValue)
    }
}

class TopupTopSellPackageObject : NSObject{
    let topupTopSellPackageObject_code : String
    let topupTopSellPackageObject_imageUrl : String
    let topupTopSellPackageObject_name : String
    let topupTopSellPackageObject_title : String
    let topupTopSellPackageObject_amount : String
    let topupTopSellPackageObject_shortDesc : String
    let topupTopSellPackageObject_longDesc : String
    let topupTopSellPackageObject_categoryCode : String
    let topupTopSellPackageObject_packageKey : String
    
    init(topuptopsellpackageobject_code : String,
         topuptopsellpackageobject_imageUrl : String,
         topuptopsellpackageobject_name : String,
         topuptopsellpackageobject_title: String,
         topuptopsellpackageobject_amount : String,
         topuptopsellpackageobject_shortDesc : String,
         topuptopsellpackageobject_longDesc : String,
         topuptopsellpackageobject_categoryCode:String,
         topuptopsellpackageobject_packageKey:String){
        
        self.topupTopSellPackageObject_code = topuptopsellpackageobject_code
        self.topupTopSellPackageObject_imageUrl = topuptopsellpackageobject_imageUrl
        self.topupTopSellPackageObject_name = topuptopsellpackageobject_name
        self.topupTopSellPackageObject_title = topuptopsellpackageobject_title
        self.topupTopSellPackageObject_amount = topuptopsellpackageobject_amount
        self.topupTopSellPackageObject_shortDesc = topuptopsellpackageobject_shortDesc
        self.topupTopSellPackageObject_longDesc = topuptopsellpackageobject_longDesc
        self.topupTopSellPackageObject_categoryCode = topuptopsellpackageobject_categoryCode
        self.topupTopSellPackageObject_packageKey = topuptopsellpackageobject_packageKey
    }
    
    required convenience init?(value:JSON){
        self.init(topuptopsellpackageobject_code : value["package_code"].stringValue,
                  topuptopsellpackageobject_imageUrl : value["package_img_url"].stringValue,
                  topuptopsellpackageobject_name : value["package_name"].stringValue,
                  topuptopsellpackageobject_title: value["package_title"].stringValue,
                  topuptopsellpackageobject_amount : value["package_amount"].stringValue,
                  topuptopsellpackageobject_shortDesc : value["package_desc_short"].stringValue,
                  topuptopsellpackageobject_longDesc : value["package_desc"].stringValue,
                  topuptopsellpackageobject_categoryCode:value["category_code"].stringValue,
                  topuptopsellpackageobject_packageKey:value["package_key"].stringValue)
    }
}

