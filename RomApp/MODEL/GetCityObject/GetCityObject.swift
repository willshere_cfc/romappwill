//
//  GetCityObject.swift
//  RomApp
//
//  Created by Administrator on 1/29/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit
import SwiftyJSON

class GetCityObject: NSObject {

    let getCityObject_datetime : String
    let getCityObject_message : String
    let getCityObject_response_code : String
    let getCityObject_response_message : String
    let getCityObject_status : String
    let getCityObject_City : JSON
    
    init(getCityObject_datetime : String,
     getCityObject_message : String,
     getCityObject_response_code : String,
     getCityObject_response_message : String,
     getCityObject_status : String,
     getCityObject_City : JSON)
    {
        self.getCityObject_datetime = getCityObject_datetime
        self.getCityObject_message = getCityObject_message
        self.getCityObject_response_code = getCityObject_response_code
        self.getCityObject_response_message = getCityObject_response_message
        self.getCityObject_status = getCityObject_status
        self.getCityObject_City = getCityObject_City
    }
    
    required convenience init?(value:JSON) {
        
        self.init(getCityObject_datetime : value["datetime"].stringValue,
                  getCityObject_message : value["message"].stringValue,
                  getCityObject_response_code : value["response_code"].stringValue,
                  getCityObject_response_message : value["response_message"].stringValue,
                  getCityObject_status : value["status"].stringValue,
                  getCityObject_City : value["City"])
        
    }
    
}


class CityObject : NSObject {
    
    let cityObject_city_code : String
    let cityObject_city_name : String
    let cityObject_province_code : String
    
    init(cityObject_city_code : String,
     cityObject_city_name : String,
     cityObject_province_code : String)
    {
        self.cityObject_city_code = cityObject_city_code
        self.cityObject_city_name = cityObject_city_name
        self.cityObject_province_code = cityObject_province_code
    }
   
    required convenience init?(value:JSON) {
        
        self.init(cityObject_city_code : value["city_code"].stringValue,
                  cityObject_city_name : value["city_name"].stringValue,
                  cityObject_province_code : value["province_code"].stringValue)
        
    }
}



