//
//  EPinObject.swift
//  RomApp
//
//  Created by Chanachon Pruchayahmaytha on 1/30/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit
import SwiftyJSON

class EPinObject: NSObject {
    let epinObject_status : String
    let epinObject_message : String
    let epinObject_responseCode : String
    let epinObject_responseMessage:String
    let epinObject_transaction_date : String
    let epinObject_transaction_id : String
    
    init(epinobject_status : String,
         epinobject_message: String,
         epinobject_responsecode : String,
         epinobject_responsemessage : String,
         epinobject_transaction_date:String,
         epinobject_transaction_id:String){
        
        self.epinObject_status = epinobject_status
        self.epinObject_message = epinobject_message
        self.epinObject_responseCode = epinobject_responsecode
        self.epinObject_responseMessage = epinobject_responsemessage
        self.epinObject_transaction_date = epinobject_transaction_date
        self.epinObject_transaction_id = epinobject_transaction_id
    }
    
    required convenience init?(value:JSON){
        self.init(epinobject_status:value["status"].stringValue,
                  epinobject_message:value["message"].stringValue,
                  epinobject_responsecode:value["response_code"].stringValue,
                  epinobject_responsemessage:value["response_message"].stringValue,
                  epinobject_transaction_date:value["datetime"].stringValue,
                  epinobject_transaction_id:value["transactionid"].stringValue
                  )
    }
}
