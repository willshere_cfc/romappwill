//
//  ForgotPasswordViewController.swift
//  RomApp
//
//  Created by Administrator on 12/29/16.
//  Copyright © 2016 Willshere. All rights reserved.
//

import UIKit

enum ForgotT: Int {
    case T1T2 = 0
    case T3
}


class ForgotPasswordViewController: UIViewController {

    var setType:ForgotT!
    
    @IBOutlet weak var lbl_TitleForgot: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        if setType == ForgotT.T1T2
        {
            self.lbl_TitleForgot.text = "สามารถเข้าไปทำการ ตั้งรหัสผ่านใหม่ได้ที่ Web MA";
        }
        else
        {
            self.lbl_TitleForgot.text = "กรุณาติดต่อ แม่ข่ายของคุณ\r\n เพื่อขอรหัสผ่านใหม่";
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - IBAction
    @IBAction func dismisVCDidselect(_ sender : Any)
    {
        self.dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
