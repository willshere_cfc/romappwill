//
//  TopupFaceValueObject.swift
//  RomApp
//
//  Created by Chanachon Pruchayahmaytha on 1/31/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit
import SwiftyJSON


class GetFaceValueObject : NSObject{
    
    let getFaceValueObject_status : String
    let getFaceValueObject_message : String
    let getFaceValueObject_responseCode : String
    let getFaceValueObject_responseMessage : String
    let getFaceValueObject_datetime : String
    let getFaceValueObject_faceValue : JSON
    
    init(
        getfacevalueobject_status : String,
        getfacevalueobject_message : String,
        getfacevalueobject_responseCode : String,
        getfacevalueobject_responseMessage : String,
        getfacevalueobject_datetime : String,
        getfacevalueobject_faceValue : JSON
        ){
        self.getFaceValueObject_status = getfacevalueobject_status
        self.getFaceValueObject_message = getfacevalueobject_message
        self.getFaceValueObject_responseCode = getfacevalueobject_responseCode
        self.getFaceValueObject_responseMessage = getfacevalueobject_responseMessage
        self.getFaceValueObject_datetime = getfacevalueobject_datetime
        self.getFaceValueObject_faceValue = getfacevalueobject_faceValue
    }
    
    required convenience init?(value:JSON){
        self.init(
            getfacevalueobject_status : value["status"].stringValue,
            getfacevalueobject_message : value["message"].stringValue,
            getfacevalueobject_responseCode : value["response_code"].stringValue,
            getfacevalueobject_responseMessage : value["response_message"].stringValue,
            getfacevalueobject_datetime : value["datetime"].stringValue,
            getfacevalueobject_faceValue : value["facevalue"]
        )
    }
}

class FaceValueObject: NSObject {
    
    let faceValueObject_internalCode : String
    let faceValueObject_product:String
    let faceValueObject_value : String
    
    init(facevalueobject_internalCode:String,facevalueobject_product:String,facevalueobject_value : String){
        self.faceValueObject_internalCode = facevalueobject_internalCode;
        self.faceValueObject_product = facevalueobject_product
        self.faceValueObject_value = facevalueobject_value
    }
    
    required convenience init?(value:JSON){
        self.init(
            facevalueobject_internalCode:value["internal_code"].stringValue,
            facevalueobject_product:value["product"].stringValue,
            facevalueobject_value:value["topup_facevalue"].stringValue
        )
    }
    
}
