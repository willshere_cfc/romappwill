//
//  TransferInputPriceViewController.swift
//  RomApp
//
//  Created by Administrator on 1/6/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit
import CFAlertViewController

class TransferInputPriceViewController: UIViewController {

  
    
    @IBOutlet weak var btn_LeftNavbar: UIButton!
    @IBOutlet weak var txt_NumberOfPrice: UITextField!
    
    var getMainObj : GetMainObject?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - IBAction
    
    @IBAction func leftNavbarDidSelect(_ sender: Any)
    {
       _ = self.navigationController?.popViewController(animated: true)  
    }

    @IBAction func confirmDidSelect(_ sender: Any)
    {//transferstep //minagenttransfer
        
        if self.txt_NumberOfPrice.text!.length > 0
        {
            let stringNumber = self.txt_NumberOfPrice.text
            let numberFromString : Float = Float(stringNumber!)!
 
            if numberFromString < 400
            {
                self.alertControllWith(title: "เติมเงินเริ่มต้นราคา 400 บาท", button: "ตกลง")
            }
            else if numberFromString >= 400
            {
                
                let strPrice = "\(numberFromString/200)"
                var tokenPrice = strPrice.components(separatedBy: ".")
 
               if tokenPrice[1] == "0"
                {
                    self.pushVC(inputPrice: "\(numberFromString)")
                }
                else
                {
                    self.alertControllWith(title: "เติมเงินเริ่มต้นราคา 400 บาท เพิ่มได้ครั้งละ 200 บาท", button: "ตกลง")
                }
            }
            
        }
       
        
    }
    
    //Mark : Alert
    func alertControllWith(title:String, button:String)
    {
        let alert = CFAlertViewController.alert(withTitle: title, message: nil, textAlignment: NSTextAlignment.center, preferredStyle: CFAlertControllerStyle.alert) {
            
            print("Dismiss")
            
        }
        
        let actionDefault = CFAlertAction.init(title: button, style: CFAlertActionStyle.default, alignment: CFAlertActionAlignment.justified, color: UIColor().hexStringToUIColor(hex: "#F7941D")) { action  in
            
            
        }
        
        alert .addAction(actionDefault!)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: - Push View Controller
    func pushVC(inputPrice : String)
    {
        let storyboard = UIStoryboard(name: "TransferStoryboard", bundle: nil)
        let transferSelectContactViewController = storyboard.instantiateViewController(withIdentifier: "TransferSelectContactViewControllerID") as! TransferSelectContactViewController
        
        transferSelectContactViewController.strInputPrice = inputPrice
        
        navigationController?.pushViewController(transferSelectContactViewController, animated: true)
    }


}
