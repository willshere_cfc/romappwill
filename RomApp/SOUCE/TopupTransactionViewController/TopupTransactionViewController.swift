//
//  TopupTransactionViewController.swift
//  RomApp
//
//  Created by Administrator on 1/6/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit
import SwiftyJSON

class TopupTransactionViewController: UIViewController {

    @IBOutlet weak var packageBestSellCollection: UICollectionView!
    
    let cellIdentifier = "TopupTransactionCollectionViewCellIdentifier"
    
    var topupObj : TopUpObject?
    var topupPhoneNumber : NSString?
    var topupFaceValue : NSString?
    
    @IBOutlet weak var lbl_topupPhoneNumber: UILabel!
    @IBOutlet weak var lbl_topupAmount: UILabel!
    @IBOutlet weak var lbl_transactionDate: UILabel!
    @IBOutlet weak var lbl_transactionID: UILabel!
    @IBOutlet weak var lbl_sellPackBenefit: UILabel!
   
    @IBOutlet weak var img_offeredPackage: UIImageView!
    @IBOutlet weak var view_OfferedPackage: CustomizableView!
    
    @IBOutlet weak var view_TopSell: UIView!

    @IBOutlet weak var view_Main: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
                self.initUI()
        
        self.packageBestSellCollection.reloadData()
    }
    
    func initUI(){
        lbl_topupPhoneNumber.text = self.topupPhoneNumber as String?
        
        
      
        let string = "\(self.topupFaceValue!)"

        
        if string.range(of:".") != nil{
            print("exists")
            
            lbl_topupAmount.text = "\(self.topupFaceValue!)"
            
        }
        else
        {
            lbl_topupAmount.text = "\(self.topupFaceValue!).00"
        }

    }
    
    
   
    override func viewDidLayoutSubviews() {
        
        super.viewDidLayoutSubviews()
       
        if(topupObj != nil){
            lbl_transactionDate.text = topupObj?.topupObject_transaction_date
            lbl_transactionID.text = topupObj?.topupObject_transaction_id
            lbl_sellPackBenefit.text = topupObj?.topupObject_sellpackBenefit
            
            if(topupObj?.topupObject_offerPackage != nil && topupObj?.topupObject_offerPackage != "")
            {
                let topUpOfferedPackObj = TopupOfferedPackageObject.init(value: (topupObj?.topupObject_offerPackage)!)
                
                if(topUpOfferedPackObj?.topupOfferedPackageObject_imageUrl != nil)
                {
                    let imgData = Data(base64Encoded:(topUpOfferedPackObj?.topupOfferedPackageObject_imageUrl)!)!
                    
                    if(imgData.base64EncodedString().length > 0)
                    {
                        self.img_offeredPackage.image = UIImage(data:imgData)
                    }
                    else
                    {
                        view_OfferedPackage.isHidden = true
                        
                        self.view_TopSell.frame = CGRect(x: 5, y: 379, width: self.view_TopSell.frame.size.width, height: self.view_TopSell.frame.size.height)
                    }
                }
                else
                {
                    view_OfferedPackage.isHidden = true
                    
                    self.view_TopSell.frame = CGRect(x: 5, y: 379, width: self.view_TopSell.frame.size.width, height: self.view_TopSell.frame.size.height)
                }
                
            }
            
        }
        
        self.packageBestSellCollection.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Push VC
    func pushVC()
    {
        let storyboard = UIStoryboard(name: "PackageStoryBoard", bundle: nil)
        let packageCagtegoryViewController = storyboard.instantiateViewController(withIdentifier: "PackageCagtegoryViewControllerID") as! PackageCagtegoryViewController
        
        packageCagtegoryViewController.phoneNumber = self.lbl_topupPhoneNumber.text!
        navigationController?.pushViewController(packageCagtegoryViewController, animated: true)
    }
    
    //MARK: - IBAction
    
    @IBAction func selectOfferedPackage(_ sender: Any) {
        
        let topUpOfferedPackObj = TopupOfferedPackageObject.init(value: (topupObj?.topupObject_offerPackage)!)
        print((topupObj?.topupObject_offerPackage)!)
        
        let storyboard = UIStoryboard(name: "PackageStoryBoard", bundle: nil)
        let packageDetailViewController = storyboard.instantiateViewController(withIdentifier: "PackageDetailViewControllerID") as! PackageDetailViewController
        
    
        packageDetailViewController.topupOfferedPackageObj = topUpOfferedPackObj
        packageDetailViewController.phoneNumber = self.lbl_topupPhoneNumber.text!
        
        navigationController?.pushViewController(packageDetailViewController, animated: true)
    }
    
    
    @IBAction func confirmDidSelect(_ sender: Any)
    {
        _ = navigationController?.popToRootViewController(animated: true)
    }
   
    @IBAction func seeMoreDidSelect(_ sender: Any)
    {
        let topSellObj = TopupTopSellPackageObject.init(value: (self.topupObj?.topupObject_topsellPackage[0])!)
        print(topSellObj?.topupTopSellPackageObject_title ?? "Non-Package")
        
        self.pushVC()
    }
}

// MARK: - EXTENION

// MARK: - UICollection Delegate

extension TopupTransactionViewController : UICollectionViewDataSource
{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        
        return (self.topupObj?.topupObject_topsellPackage.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath as IndexPath) as! TopupTransactionCollectionViewCell
        
        let topSellObj = TopupTopSellPackageObject.init(value: (self.topupObj?.topupObject_topsellPackage[indexPath.row])!)
        cell.settingTopSellPackageCellWithObject(topsellObj: topSellObj!)
        

        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        
        

    }
    
    
}


extension TopupTransactionViewController : UICollectionViewDelegate
{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        print("index => \(indexPath.row)")
        
                let topUpOfferedPackObj = TopupOfferedPackageObject.init(value: (self.topupObj?.topupObject_topsellPackage[indexPath.row])!)
        
                let storyboard = UIStoryboard(name: "PackageStoryBoard", bundle: nil)
                let packageDetailViewController = storyboard.instantiateViewController(withIdentifier: "PackageDetailViewControllerID") as! PackageDetailViewController
        
        
        
                packageDetailViewController.topupOfferedPackageObj = topUpOfferedPackObj
                packageDetailViewController.phoneNumber = self.lbl_topupPhoneNumber.text!
                packageDetailViewController.flagTopupPackage = true
                navigationController?.pushViewController(packageDetailViewController, animated: true)
    }
}




