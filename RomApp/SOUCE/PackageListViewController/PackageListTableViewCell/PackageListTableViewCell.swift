//
//  PackageListTableViewCell.swift
//  RomApp
//
//  Created by Administrator on 1/5/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit

class PackageListTableViewCell: UITableViewCell {

    @IBOutlet weak var lbl_TitlePackage: UILabel!
    @IBOutlet weak var lbl_SubTitlePackage: UILabel!
    @IBOutlet weak var lbl_PricePackage: UILabel!
    
    
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    open override func awakeFromNib() {
    }
    
    open func setup() {
    }
    
    open class func height() -> CGFloat {
        return 72
    }
    
    open func setingPackageCell(obj:PackageListObject){
        
        self.lbl_TitlePackage?.text = obj.packageListObject_name
        self.lbl_SubTitlePackage?.text = obj.packageListObject_shortDesc
        self.lbl_PricePackage?.text = obj.packageListObject_price
    }
    
 

}
