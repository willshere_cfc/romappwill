//
//  AboutAgreementViewController.swift
//  RomApp
//
//  Created by Administrator on 1/10/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit

class AboutAgreementViewController: UIViewController {

    
    @IBOutlet weak var web_Agreement: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        /*self.textView_DetailAm.font = UIFont(name: "DBHelvethaicaMonX-Med", size: 24)*/
        // Do any additional setup after loading the view.
        let defult = UserDefaults.standard
        let dataDF = defult.value(forKey: "business_agreement")

        //loadRequest(URLRequest(url: URL(string: self.agreementObject!.agreementObject_message)!))
        web_Agreement.loadRequest(URLRequest(url:URL(string:dataDF as! String)!))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func leftNavbarDidSelect(_ sender: Any)
    {
         _ = self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    

}
