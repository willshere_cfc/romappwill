//
//  AllServiceTableViewCell.swift
//  RomApp
//
//  Created by Administrator on 1/2/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit

class AllServiceTableViewCell: UITableViewCell {

    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var img_Icon: UIImageView!
   
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    open override func awakeFromNib() {
    }
    
    open func setup() {
    }
    
    open class func height() -> CGFloat {
        return 60
    }
    
    open func setTitleCell(title:String){
        
        self.lbl_Title?.text = title
    }
    
    open func setImageIcon(imgName:String){
        
        self.img_Icon.image = UIImage(named:imgName)
    }

}
