//
//  PackageListViewController.swift
//  RomApp
//
//  Created by Administrator on 1/5/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit
import JGProgressHUD

class PackageListViewController: UIViewController {

    var menus = ["โทรเหมาๆ", "โทรหาเบอร์พิเศษ", "โทรจุใจ", "โทรไม่อั้น", "โทรทัั้งวัน"]
    var menusSub = ["โทรวินาทีละ 1.6 สต.", "โทร 30 วินาทีแรก 40 สต.", "โทร 30 วินาทีแรก 40 สต.", "โทร 30 วินาทีแรก 40 สต.", "โทร 30 วินาทีแรก 40 สต."]
    var menusPrice = ["29", "49", "49", "69", "99"]
    
    var params_categorycode : String!
    var params_systemtype : String!
    var params_paymentmode : String!
    var params_networktype : String!
   
     var phoneNumber : String!
    
    var getPackageListObj : GetPackageListObject? = nil

    @IBOutlet weak var packageListTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.requestServicePackageList()
        // Do any additional setup after loading the view.
    }
    
    //MARK: - SERVICE
    
    func requestServicePackageList()
    {
        
        let parameters: [String: Dictionary] =
            [
                "data":
                    [
                        "categorycode": "\(self.params_categorycode!)",
                        "systemtype": "\(self.params_systemtype!)",
                        "paymentmode": "\(self.params_paymentmode!)",
                        "networktype": "\(self.params_networktype!)"
                    ]
            ]
        
        let strURL = "http://stg-etopupservices.ais.co.th/dev-ROMMobileServiceAPI/ROMService.svc/GetListPackageProfile/"
        
        print("params => \(parameters)")
        
        JGProgressHUD.loadingProgress(view: self.view)
        AFWrapper.requestPOSTURL(strURL, params: parameters as [String : AnyObject]?, headers: nil, success: { (JSONResponse) ->  Void in
            
            print(JSONResponse)
            
            JGProgressHUD.dismisProgress()
            print("Get Main => \(JSONResponse)")
            if JSONResponse.count != 0
            {
                self.getPackageListObj = GetPackageListObject.init(value: JSONResponse)
                
                self.packageListTableView.reloadData()
            }
            
            
        }) { (error) -> Void in
            print(error)
            JGProgressHUD.dismisProgress()
        }
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- IBAcion
    
    @IBAction func leftNavbarDidSelect(_ sender: Any)
    {
         _ = self.navigationController?.popViewController(animated: true)
    }
}

// MARK: - EXTENION

// MARK: - Tableview Delegate

extension PackageListViewController : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        return MenuTableViewCell.height()
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let storyboard = UIStoryboard(name: "PackageStoryBoard", bundle: nil)
        let packageDetailViewController = storyboard.instantiateViewController(withIdentifier: "PackageDetailViewControllerID") as! PackageDetailViewController
        
         let packageListObj = PackageListObject.init(value: self.getPackageListObj!.getPackageListObject_package[indexPath.row])
        packageDetailViewController.packageListObj = packageListObj
        packageDetailViewController.phoneNumber = self.phoneNumber!
        packageDetailViewController.flagTopupPackage = false
        
        navigationController?.pushViewController(packageDetailViewController, animated: true)
    }
    
    
}

extension PackageListViewController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if self.getPackageListObj != nil
        {
           return self.getPackageListObj!.getPackageListObject_package.count
        }
        else
        {
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellIdentifier = "PackageListTableViewCellIdentifier"
        let cell:PackageListTableViewCell = self.packageListTableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! PackageListTableViewCell
        
        let packageListObj = PackageListObject.init(value: self.getPackageListObj!.getPackageListObject_package[indexPath.row])
        
        cell .setingPackageCell(obj: packageListObj!)
        
        return cell
    }
}

