//
//  RegisterInfomationViewController.swift
//  RomApp
//
//  Created by Administrator on 1/16/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import CFAlertViewController
import JGProgressHUD

class RegisterInfomationViewController: UIViewController {

    @IBOutlet weak var txt_NameTH: UITextField!
    @IBOutlet weak var txt_LastNameTH: UITextField!
    @IBOutlet weak var txt_NameEN: UITextField!
    @IBOutlet weak var txt_LastNameEN: UITextField!
    @IBOutlet weak var txt_BirthDate: UITextField!
    @IBOutlet weak var txt_CardID: UITextField!
    @IBOutlet weak var txt_PhoneNumber: UITextField!
    
    @IBOutlet weak var txt_Job: UITextField!
    @IBOutlet weak var txt_Email: UITextField!
    
    var typeRegis : String!
    var phoneNumber : String!
    var idCard : String!
    
    var occupationArray : NSMutableArray = []
    var selectPickerIndex: NSInteger = 0
    
    var dateStr : String!
    var mountStr : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupUI()
        self.requestServiceGetOccupation()
        // Do any additional setup after loading the view.
    }
    
    func requestServiceGetOccupation()
    {
        
        //        print("params => \(parameters)")
        
        let strURL = "http://stg-etopupservices.ais.co.th/dev-ROMMobileServiceAPI/ROMService.svc/GetOccupation/"
        
        JGProgressHUD.loadingProgress(view: self.view)
        AFWrapper.requestPOSTURL(strURL, params: nil, headers: nil, success: { (JSONResponse) ->  Void in
            
            JGProgressHUD.dismisProgress()
            
            print(JSONResponse)
            
            print("GetProvince => \(JSONResponse)")
            if JSONResponse.count != 0
            {
                let getOccupationObj = GetOccupationObject.init(value: JSONResponse)
                
                
                for i in 1...getOccupationObj!.getOccupationObject_occupation.count-1
                {
                    let occupationObj = OccupationObject.init(value: getOccupationObj!.getOccupationObject_occupation[i])
                    print("occupationObj  = > \(occupationObj!.occupationObject_occupation_name)")
                    self.occupationArray.add(occupationObj!.occupationObject_occupation_name)
                    
                }
                
                print("occupationArray \(self.occupationArray)")
                
            }
            
            
        }) { (error) -> Void in
            print(error)
            JGProgressHUD.dismisProgress()
        }
    }

    
    func setupUI()
    {
        self.txt_PhoneNumber.text = self.phoneNumber
        self.txt_CardID.text = self.idCard
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - IBAction
    
    @IBAction func leftNavbarDidSelect(_ sender: Any)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }

    @IBAction func confirmDidSelect(_ sender: Any)
    {
        if self.txt_NameTH.text?.length == 0
        {
            self.alertControllWith(title: "กรุณากรอกชื่อภาษาไทย", button: "ตกลง")
        }
        else if self.txt_LastNameTH.text?.length == 0
        {
           self.alertControllWith(title: "กรุณากรอกนามสกุลภาษาไทย", button: "ตกลง")
        }
        else if self.txt_NameEN.text?.length == 0
        {
           self.alertControllWith(title: "กรุณากรอกชื่อภาษาอังกฤษ", button: "ตกลง")
        }
        else if self.txt_LastNameEN.text?.length == 0
        {
            self.alertControllWith(title: "กรุณากรอกนามสกุลภาษาอังกฤษ", button: "ตกลง")
        }
        else if self.txt_BirthDate.text?.length == 0
        {
           self.alertControllWith(title: "กรุณาเลือกวันที่", button: "ตกลง")
        }
        else if self.txt_Job.text?.length == 0
        {
            self.alertControllWith(title: "กรุณากรอกอาชีพ", button: "ตกลง")
        }
        else
        {
          self.pushViewController()  
        }
        
    }
    
    //MARK: - Alert
    func alertControllWith(title:String, button:String)
    {
        let alert = CFAlertViewController.alert(withTitle: title, message: nil, textAlignment: NSTextAlignment.center, preferredStyle: CFAlertControllerStyle.alert) {
            
            print("Dismiss")
            
        }
        
        let actionDefault = CFAlertAction.init(title: button, style: CFAlertActionStyle.default, alignment: CFAlertActionAlignment.justified, color: UIColor().hexStringToUIColor(hex: "#F7941D")) { action  in
            
            
        }
        
        alert .addAction(actionDefault!)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    //MARK: - Picker
    
    @IBAction func datePIckerDidSelect(_ sender: Any)
    {
        ActionSheetDatePicker.show(withTitle: "วัน/เดือน/ปี", datePickerMode: UIDatePickerMode.date, selectedDate:  Date(), target: self, action: #selector(RegisterInfomationViewController.dateWasSelected(dateWasSelected:element:)), origin: sender)
    }
    
    func dateWasSelected(dateWasSelected :Date, element:Any)
    {

        print(dateWasSelected.fullDate)
        print(dateWasSelected.shortDate)
        let dateCompo =  "\(dateWasSelected.shortDate)".components(separatedBy: "/")
        print("dateCompo \(dateCompo)")
        
        let date1 = Int(dateCompo[0])
        let _ : String!
        let _ : String!
        if date1! < 10 {
            
            dateStr = "0\(Int(dateCompo[0])!)"
            
             print("intt => \(date1!)")
        }
        else
        {
            dateStr = "\(Int(dateCompo[0])!)"
        }
        
        let date2 = Int(dateCompo[1])
        if date2! < 10 {
            
            mountStr = "0\(Int(dateCompo[1])!)"
        }
        else
        {
            mountStr = "\(Int(dateCompo[1])!)"
        }
        
        let year = (Int(dateCompo[2])!+2500) - 543
        print("year => \(year)")
        
        
       let dateStrEx = "\(dateStr!)/\(mountStr!)/\(year)"
        
        print("me => \(dateStr!)/\(mountStr!)/\(year)")
        
        self.dateStr = dateStrEx
        print("dateStr \(self.dateStr)")
        self.txt_BirthDate.text = "\(dateWasSelected.fullDate)"
        
    }
    
    @IBAction func jobPickerDidSelect(_ sender: Any)
    {
        let swiftArray = occupationArray as NSArray as! [String]
        ActionSheetStringPicker.show(withTitle: "อาชีพ", rows: swiftArray, initialSelection: self.selectPickerIndex, target: self, successAction: #selector(RegisterAddressViewController.districtLiveWasSelected(selectedIndex:element:)), cancelAction: #selector(TopupSelectPriceViewController.canclePicker), origin: sender)
    }
    
    func districtLiveWasSelected(selectedIndex :NSNumber, element:Any)
    {
        self.selectPickerIndex = selectedIndex.intValue
//
        self.txt_Job.text = "\(occupationArray[self.selectPickerIndex])"
        
    }
    
    
    //MARK: - Push View Controller
    func pushViewController()
    {

        let storyboard = UIStoryboard(name: "RegisterStoryboard", bundle: nil)
        let registerAddressViewController = storyboard.instantiateViewController(withIdentifier: "RegisterAddressViewControllerID") as! RegisterAddressViewController
        
        registerAddressViewController.param_NameTH = self.txt_NameTH.text
        registerAddressViewController.param_LastNameTH = self.txt_LastNameTH.text
        registerAddressViewController.param_NameEN = self.txt_NameEN.text
        registerAddressViewController.param_LastNameEN = self.txt_LastNameEN.text
        registerAddressViewController.param_BirthDate = self.dateStr
        registerAddressViewController.param_CardID = self.txt_CardID.text
        registerAddressViewController.param_PhoneNumber = self.txt_PhoneNumber.text
        registerAddressViewController.param_Job = self.txt_Job.text
        registerAddressViewController.param_Email = self.txt_Email.text
        registerAddressViewController.typeRegis = self.typeRegis
        navigationController?.pushViewController(registerAddressViewController, animated: true)
    }

}

//MARK: EXTENSION

//MARK: - UITextfield Delaget

extension RegisterInfomationViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField .resignFirstResponder()
        
        return true
    }
}










