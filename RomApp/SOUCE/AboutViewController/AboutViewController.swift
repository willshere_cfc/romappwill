//
//  AboutViewController.swift
//  RomApp
//
//  Created by Administrator on 1/10/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {

  
    
    @IBOutlet weak var lbl_appVersion: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            self.lbl_appVersion.text = version
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - IBAction
    
    @IBAction func leftNavbarDidSelect(_ sender: Any)
    {
        self.slideMenuController()?.openLeft()
    }
    @IBAction func romCallDidSelect(_ sender: Any)
    {
        
        
        let alertController = UIAlertController(title: "ท่านต้องการติดต่อ Call Center ใช่หรือไม่", message: nil, preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: "โทร", style: UIAlertActionStyle.default)
        {
            (result : UIAlertAction) -> Void in
            
            if let url = URL(string: "tel://\(1175)") {
                UIApplication.shared.openURL(url)
            }
        }
        let cancleAction = UIAlertAction(title: "ยกเลิก", style: UIAlertActionStyle.cancel)
        {
            (result : UIAlertAction) -> Void in
            
            print("You pressed Cancle")
        }
        alertController.addAction(okAction)
        alertController.addAction(cancleAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    

    @IBAction func agreemantDidSelect(_ sender: Any)
    {
        let storyboard = UIStoryboard(name: "AboutStoryboard", bundle: nil)
        let aboutAgreementViewController = storyboard.instantiateViewController(withIdentifier: "AboutAgreementViewControllerID") as! AboutAgreementViewController
        navigationController?.pushViewController(aboutAgreementViewController, animated: true)
    }


}
