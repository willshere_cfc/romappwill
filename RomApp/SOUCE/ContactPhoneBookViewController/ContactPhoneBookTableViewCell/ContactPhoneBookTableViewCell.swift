//
//  ContactPhoneBookTableViewCell.swift
//  RomApp
//
//  Created by Administrator on 1/12/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit
import APAddressBook

protocol ContectPhoneBookDelegate {
    
    func contactDidSelect(row:Int, buttonImg:UIButton)
}

class ContactPhoneBookTableViewCell: UITableViewCell {

    @IBOutlet weak var btn_SelectListContact: UIButton!
    
    @IBOutlet weak var lbl_Title: UILabel!
    
    @IBOutlet weak var lbl_Phone: UILabel!
    
    var delegate : ContectPhoneBookDelegate?
    
   
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    open override func awakeFromNib()
    {
        
    }
    
    open func setup() {
    }
    
    open class func height() -> CGFloat {
        return 60
    }
    
     func updateContact(with model: APContact) {
        
        lbl_Title.text = contactName(model)
        lbl_Phone.text = contactPhones(model)
        
    }
    
    
    
    func contactName(_ contact :APContact) -> String {
        if let firstName = contact.name?.firstName, let lastName = contact.name?.lastName {
            return "\(firstName) \(lastName)"
        }
        else if let firstName = contact.name?.firstName {
            return "\(firstName)"
        }
        else if let lastName = contact.name?.lastName {
            return "\(lastName)"
        }
        else {
            return "Unnamed contact"
        }
    }
    
    func contactPhones(_ contact :APContact) -> String {
        if let phones = contact.phones {
            var phonesString = ""
            for phone in phones {
                if let number = phone.number {
                    phonesString = phonesString + " " + number
                }
            }
            return phonesString
        }
        return "No phone"
    }

    @IBAction func selectListContactDidSelect(_ sender: Any)
    {
        self.delegate? .contactDidSelect(row: self.tag, buttonImg: self.btn_SelectListContact)
    }
}
