//
//  MainViewController.swift
//  RomApp
//
//  Created by Administrator on 12/28/16.
//  Copyright © 2016 Willshere. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift
import JGProgressHUD

enum MainTear: String {
    case T1T2
    case T3
  
}

class MainViewController: UIViewController {
    
    var setType:MainTear!
    
    @IBOutlet weak var scrollView_T1: UIScrollView!
    @IBOutlet weak var scrollView_T2T3: UIScrollView!
    
    @IBOutlet weak var view_T1: UIView!
    @IBOutlet weak var view_T2T3: UIView!
    
    var getMainObj:GetMainObject!
    var phoneNumber:String?
    
    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var lbl_PhoneNumber: UILabel!
    @IBOutlet weak var img_Profile: CustomizableImage!
    @IBOutlet weak var lbl_Balance: UILabel!
    @IBOutlet weak var lbl_SatangBath: UILabel!
    @IBOutlet weak var lbl_NotiNumber: UILabel!
    
    @IBOutlet weak var viewNoti: CustomizableView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

       UIApplication.shared.statusBarStyle = .lightContent
    
        NotificationCenter.default.addObserver(self, selector: #selector(MainViewController.requestServiceGetMain), name: Notification.Name("NotificationUpdateBagdeNews"), object: nil)
        // Do any additional setup after loading the view.
    }
    
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(true)
        
        if self.setType == .T3
        {
            setScrollViewT1() //สลับ Tear กัน
            setViewT1SHOW()
        }
        else if self.setType == .T1T2
        {
            setScrollViewT2T3() //สลับ Tear กัน
            setViewT2T3SHOW()
        }
        else {}
        
         self.slideMenuController()?.addLeftGestures()
        
        self.requestServiceGetMain()
        
    }
    
    // MARK: - SET VIEW T1 , T2T3
    
    func setViewT1SHOW()
    {
        self.view_T1.isHidden = false
    }
    
    func setViewT2T3SHOW()
    {
        self.view_T2T3.isHidden = false
    }
    
    // MARK: - SET SCROLL VIEW
    
    func setScrollViewT1()
    {
        if !(UIDevice().screenType == .iPhone6Plus)
        {
            self.scrollView_T1 .setContentOffset(CGPoint(x:0,y:20), animated: false)
            
        }
        
        if (UIDevice().screenType == .iPhone6Plus) || (UIDevice().screenType == .iPhone6)
        {
            self.scrollView_T1.isScrollEnabled = false
        }
    }
    
    func setScrollViewT2T3()
    {
        if (UIDevice().screenType == .iPhone6Plus)
        {
            self.scrollView_T2T3 .setContentOffset(CGPoint(x:0,y:-40), animated: false)
        }
        
        if (UIDevice().screenType == .iPhone6Plus) || (UIDevice().screenType == .iPhone6)
        {
            self.scrollView_T2T3.isScrollEnabled = false
        }
    }
    
    //MARK: - SERVICE
    
    func requestServiceGetMain()
    {
        let defult = UserDefaults.standard
        let dataUser_agent_id =  defult.value(forKey: "user_agent_id")
        
        
        let parameters: [String: Dictionary] =
            [
                "data":
                    [
                        "agent_id": dataUser_agent_id!
                    ]
            ]
        
        let strURL = "http://stg-etopupservices.ais.co.th/dev-ROMMobileServiceAPI/ROMService.svc/GetMain/"
        
        JGProgressHUD.loadingProgress(view: self.view)
        AFWrapper.requestPOSTURL(strURL, params: parameters as [String : AnyObject]?, headers: nil, success: { (JSONResponse) ->  Void in
            print(JSONResponse)
            
            JGProgressHUD.dismisProgress()
            print("Get Main => \(JSONResponse)")
            if JSONResponse.count != 0
            {
               self.getMainObj = GetMainObject.init(value: JSONResponse)
                self.setupUI()
            }
            
            
        }) { (error) -> Void in
            print(error)
            JGProgressHUD.dismisProgress()
        }
    }
    
    //MARK: - Setup UI
    func setupUI()
    {
           // self.img_Profile: CustomizableImage!
        self.lbl_Name.text = self.getMainObj.getMainObject_profilename
        self.lbl_PhoneNumber.text = String().stringFormatPhoneNumber(phoneNumber: self.phoneNumber!)
        
        self.lbl_NotiNumber.text = self.getMainObj.getMainObject_noti_count
        
        if self.getMainObj.getMainObject_noti_count == "0"
        {
            self.viewNoti.isHidden = true
        }
        else
        {
            self.viewNoti.isHidden = false
        }
        
        let string = self.getMainObj.getMainObject_balance
        
        if string.range(of:".") != nil{
            print("exists")
            
            var token = string.components(separatedBy: ".")
            
            self.lbl_Balance.text = token[0]
            self.lbl_SatangBath.text = ".\(token[1]) บาท"
        }
        else
        {
            self.lbl_Balance.text = self.getMainObj.getMainObject_balance
            self.lbl_SatangBath.text = ".00 บาท"
        }
        
 
    }
    
   
  
    

    // MARK: - IBAction
    
    @IBAction func menuDidselect(_ sender: Any)
    {
      self.slideMenuController()?.openLeft()
    }
    
    @IBAction func newsDidselect(_ sender: Any)
    {
        
        if UIDevice().screenType == .iPhone5 || UIDevice().screenType == .iPhone4
        {
            let storyboard = UIStoryboard(name: "NewsStoryBoard", bundle: nil)
            let newsViewController = storyboard.instantiateViewController(withIdentifier: "Newsi5ViewControllerID") as! NewsViewController
            newsViewController.setSelectType = .MainType
            navigationController?.pushViewController(newsViewController,
                                                     animated: true)
        }
        else
        {
            let storyboard = UIStoryboard(name: "NewsStoryBoard", bundle: nil)
            let newsViewController = storyboard.instantiateViewController(withIdentifier: "NewsViewControllerID") as! NewsViewController
            newsViewController.setSelectType = .MainType
            navigationController?.pushViewController(newsViewController,
                                                     animated: true)
        }
        
    }
    
    // MARK: IBAction Tabbar
    
    @IBAction func historyDidSelect(_ sender: Any)
    {
        let storyboard = UIStoryboard(name: "HistoryStoryboard", bundle: nil)
        let historyListViewController = storyboard.instantiateViewController(withIdentifier: "HistoryListViewControllerID") as! HistoryListViewController
        historyListViewController.setSelectType = .MainType
        navigationController?.pushViewController(historyListViewController,
                                                 animated: true)
    }
    
    @IBAction func reportDidSelect(_ sender: Any)
    {
        let storyboard = UIStoryboard(name: "ReportStoryboard", bundle: nil)
        let reportListViewController = storyboard.instantiateViewController(withIdentifier: "ReportListViewControllerID") as! ReportListViewController
        reportListViewController.setSelectType = .MainType
        navigationController?.pushViewController(reportListViewController,animated: true)
    }
    
    // MARK: IBAction  T1
 
    @IBAction func topupDidSelect(_ sender: Any)
    {
        let storyboard = UIStoryboard(name: "TopUpStoryBoard", bundle: nil)
        let topUpInputNumberViewController = storyboard.instantiateViewController(withIdentifier: "TopUpInputNumberViewControllerID") as! TopUpInputNumberViewController
        navigationController?.pushViewController(topUpInputNumberViewController,animated: true)
    }
    
    @IBAction func transferDidSelect(_ sender: Any)
    {
        let storyboard = UIStoryboard(name: "TransferStoryboard", bundle: nil)
        let transferInputPriceViewController = storyboard.instantiateViewController(withIdentifier: "TransferInputPriceViewControllerID") as! TransferInputPriceViewController
        transferInputPriceViewController.getMainObj = self.getMainObj
        navigationController?.pushViewController(transferInputPriceViewController,animated: true)
    }
    
    @IBAction func sellPackageDidSelect(_ sender: Any)
    {
        let storyboard = UIStoryboard(name: "PackageStoryBoard", bundle: nil)
        let packageInputNumberViewController = storyboard.instantiateViewController(withIdentifier: "PackageInputNumberViewControllerID") as! PackageInputNumberViewController
        navigationController?.pushViewController(packageInputNumberViewController,animated: true)
    }
    
    @IBAction func sellPINDidSelect(_ sender: Any)
    {
        let storyboard = UIStoryboard(name: "E-PINStoryBoard", bundle: nil)
        let ePinInputNumberViewController = storyboard.instantiateViewController(withIdentifier: "EPinInputNumberViewControllerID") as! EPinInputNumberViewController
        navigationController?.pushViewController(ePinInputNumberViewController,
                                                 animated: true)
    }
    
    
    //MARK: IBAction T2T3
    
    
    @IBAction func transferDidSelectT2T3(_ sender: Any)
    {
        let storyboard = UIStoryboard(name: "TransferStoryboard", bundle: nil)
        let transferInputPriceViewController = storyboard.instantiateViewController(withIdentifier: "TransferInputPriceViewControllerID") as! TransferInputPriceViewController
        navigationController?.pushViewController(transferInputPriceViewController,animated: true)
    }
    
    @IBAction func registerDidSelect(_ sender: Any)
    {
        let storyboard = UIStoryboard(name: "RegisterStoryboard", bundle: nil)
        let registerInputPhoneAndCardIDViewController = storyboard.instantiateViewController(withIdentifier: "RegisterInputPhoneAndCardIDViewControllerID") as! RegisterInputPhoneAndCardIDViewController
        navigationController?.pushViewController(registerInputPhoneAndCardIDViewController,animated: true)
    }
    
    @IBAction func sellPackageT2T3(_ sender: Any)
    {
         let contactViewController = storyboard?.instantiateViewController(withIdentifier: "ContactViewControllerID") as! ContactViewController
        contactViewController.setSelectType = .MainType
        navigationController?.pushViewController(contactViewController,animated: true)
    }
    
    //MARK: - IBAction Camera Libery
    
    @IBAction func imageDidSelect(_ sender: Any)
    {
        let alertController =  UIAlertController.alertWithTitle(title: "", message: "", buttonTitle: "")
        
        alertController.addAction(.cancel)
        
        alertController.addAction(.cameraPhoto
        { action in
            self.cameraPick()
        })
        
        alertController.addAction(.libraryPhoto
        { action in
            
            self.liberyPick()
        })
        
        alertController.modalPresentationStyle = UIModalPresentationStyle.popover
        
        // set up the popover presentation controller
        alertController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.up
        alertController.popoverPresentationController?.delegate = self
        alertController.popoverPresentationController?.sourceView = sender as? UIView // button
        alertController.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
        
        // present the popover
        self.present(alertController, animated: true, completion: nil)
            
    }
    
    //MARK: - UIImage Picker Controller
    func cameraPick()
    {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .camera
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    func liberyPick()
    {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    
}

// MARK: - Extansion ============

//MARK: - UIPopoverPresentationController Delegate

extension MainViewController : UIPopoverPresentationControllerDelegate {
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        // return UIModalPresentationStyle.FullScreen
        return UIModalPresentationStyle.none
    }
}

//MARK: - UIImagePickerController Delegate

extension MainViewController : UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage {

            self.img_Profile.image = pickedImage
        }
        
         picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        dismiss(animated: true, completion: nil)
    }
    
}

extension MainViewController : UINavigationControllerDelegate {
    
    
}

    // MARK: - SlideMenuController Delegate

extension MainViewController : SlideMenuControllerDelegate {
    
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        print("SlideMenuControllerDelegate: rightDidClose")
    }
}

