//
//  LoginViewController.swift
//  RomApp
//
//  Created by Administrator on 12/26/16.
//  Copyright © 2016 Willshere. All rights reserved.
//

import UIKit
import CFAlertViewController
import Alamofire
import JGProgressHUD

enum GetProfile_Stasus : String {
    case success = "success"
    case fail = "fail"
}

class LoginViewController: UIViewController ,UITextFieldDelegate{

    @IBOutlet weak var btn_Confirm: CustomizableButton!
    
    @IBOutlet weak var txt_Pin: CustomizableTextField!
    
    @IBOutlet weak var lbl_PhoneNumber: UILabel!
    
    var getProfileType : GetProfile_Stasus!
    
    var getProfileObj : GetProfileObject? = nil
    
    var numberwifi : Int = 0
    var wann : Int = 0
    
    var phoneNumber = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.slideMenuController()?.removeLeftGestures()
        self.checkStatusWifi()
        
       
        
        self.hideKeyboard()
    }
    
    func checkStatusWifi()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.networkStatusChanged(_:)), name: NSNotification.Name(rawValue: ReachabilityStatusChangedNotification), object: nil)
        Reach().monitorReachabilityChanges()
    }
    
    func networkStatusChanged(_ notification: Notification) {
        
        let status = Reach().connectionStatus()
        switch status {
        case .unknown, .offline:
            self.numberwifi = 0
            self.wann = 0
//            print("Not connected")
        case .online(.wwan):
//            print("Connected via WWAN")
            self.wann += 1
            if self.wann == 1
            {
                print("fuck you")
              self.requestServiceCheckVerion()
            }
            
            self.numberwifi = 0
        case .online(.wiFi):
//            print("Connected via WiFi")
            self.wann = 0
            self.numberwifi += 1
            self.presentViewPopUpWiFi()
        }
    }
    
    func presentViewPopUpWiFi()
    {
        if numberwifi == 1
        {
            let storyboard = UIStoryboard(name: "PopupAfterSplash", bundle: nil)
            
            let checkReachabilityViewController = storyboard.instantiateViewController(withIdentifier: "CheckReachabilityViewControllerID") as! CheckReachabilityViewController
            
            self.present(checkReachabilityViewController, animated: true, completion: nil)
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - SERVICE
    
    func requestServiceCheckVerion()
    {
        let defult = UserDefaults.standard
        let dataDF =  defult.value(forKey: "user_auth_token")
        
        
        let parameters: [String: Dictionary] =
            [
                "data":
                    [
                        "clientversion": "1.0.1",
                        "deviceid": dataDF!
                    ]
            ]
        
            print("param => \(parameters)")
            
             let strURL = "http://www.myrom.ais.co.th/api/ClientInfoService/CheckVersion/"
        
            JGProgressHUD.loadingProgress(view: self.view)
            AFWrapper.requestPOSTURL(strURL, params: parameters as [String : AnyObject]?, headers: nil, success: { (JSONResponse) ->  Void in
                print(JSONResponse)
                
                if JSONResponse.count != 0
                {
                    let checkVerionObj = CheckVerionObject.init(value:JSONResponse)
                    
                    if checkVerionObj!.checkVerionObject_mobilenumber.isEmpty == false
                    {
                        UserDefaults.standard.setValue(checkVerionObj!.checkVerionObject_mobilenumber, forKey: "user_phonenumber")
                        
                        self.phoneNumber = checkVerionObj!.checkVerionObject_mobilenumber
                        self.lbl_PhoneNumber.text = String().stringFormatPhoneNumber(phoneNumber: checkVerionObj!.checkVerionObject_mobilenumber)
                        
                        self.requestServiceGetProfile()
                    }
                    
                }
               
                
            }) { (error) -> Void in
                print(error)
                JGProgressHUD.dismisProgress()
            }
    }
    
    

    func requestServiceGetProfile()
    {
      
        let defult = UserDefaults.standard
        let dataDF =  defult.value(forKey: "user_auth_token")
        
        let parameters: [String: Dictionary] =
            [
                "data":
                    [
                        "mobile_no": self.phoneNumber,
                        "Application_name": "ROM Mobile App",
                        "device_id": dataDF!
                    ]
            ]

        print("params \(parameters)")
        let strURL = "http://stg-etopupservices.ais.co.th/dev-ROMMobileServiceAPI/ROMService.svc/getprofile/"
        
        AFWrapper.requestPOSTURL(strURL, params: parameters as [String : AnyObject]?, headers: nil, success: { (JSONResponse) ->  Void in
          
            JGProgressHUD.dismisProgress()
            
            if JSONResponse.count != 0
            {
                 self.getProfileObj = GetProfileObject.init(value:JSONResponse)
                
                UserDefaults.standard.setValue(self.getProfileObj!.getProfileObject_agent_id, forKey: "user_agent_id")
                
                UserDefaults.standard.setValue(self.getProfileObj!.getProfileObject_level, forKey: "user_level")
                
//                print("getProfile => \(self.getProfileObj!.getProfileObject_agreement)")
         
                if self.getProfileObj!.getProfileObject_status == "success"
                {
                    if self.getProfileObj!.getProfileObject_level == 1
                    {
                        
                    }
                    else if self.getProfileObj!.getProfileObject_level == 2
                    {
                        
                    }
                    else if self.getProfileObj!.getProfileObject_level == 3
                    {
                        
                    }
                    else {}
                }
                else if self.getProfileObj!.getProfileObject_status == "fail"
                {
                    if self.getProfileObj!.getProfileObject_response_code == "BTWS034"
                    {
                        let storyboard = UIStoryboard(name: "RequestOTPStoryboard", bundle: nil)
                        let mainRequestOTPViewController = storyboard.instantiateViewController(withIdentifier: "MainRequestOTPViewControllerID") as! MainRequestOTPViewController
                        
                        mainRequestOTPViewController.phoneNumber = "\(self.phoneNumber)"
                        self.present(mainRequestOTPViewController, animated: true, completion: nil)
                    }
                    else {
                        
                        self.requestServiceGetProfile()
                    
                    }

                }
                
            }
            
            
        }) { (error) -> Void in
            print(error)
            JGProgressHUD.dismisProgress()
        }
    }

    

    
    func requestServiceLogin()
    {
        let defult = UserDefaults.standard
        let dataDF =  defult.value(forKey: "user_auth_token")
        
        
        let parameters: [String: Dictionary] =
            [
                "data":
                    [
                        "mobilenumber": self.phoneNumber,
                        "pin": self.txt_Pin.text!,
                        "deviceid": dataDF!
                    ]
        ]
        
        print("param => \(parameters)")
        
        let strURL = "http://stg-etopupservices.ais.co.th/dev-ROMMobileServiceAPI/ROMService.svc/signin/"
        
        JGProgressHUD.loadingProgress(view: self.view)
        AFWrapper.requestPOSTURL(strURL, params: parameters as [String : AnyObject]?, headers: nil, success: { (JSONResponse) ->  Void in
            print(JSONResponse)
            
            JGProgressHUD.dismisProgress()
            
            if JSONResponse.count != 0
            {
                
                let signinObj = SigninObject(value: JSONResponse)
                
                if signinObj!.signinObject_status == "success"
                {
                    let agreementObj = AgreementObject.init(value:self.getProfileObj!.getProfileObject_agreement[2])
                    
                    UserDefaults.standard.setValue(agreementObj?.agreementObject_message, forKey: "business_agreement")
                    
                    if agreementObj!.agreementObject_mobile_accept == "N"
                    {
                        if self.getProfileObj!.getProfileObject_level == 1
                        {
                            self.loginSeccess(successTear: "T1T2" ,agreementObj: agreementObj!, phoneNumber: self.phoneNumber)
                        }
                        else if self.getProfileObj!.getProfileObject_level == 2
                        {
                             self.loginSeccess(successTear: "T1T2",agreementObj: agreementObj!, phoneNumber: self.phoneNumber)
                        }
                        else if self.getProfileObj!.getProfileObject_level == 3
                        {
                             self.loginSeccess(successTear: "T3",agreementObj: agreementObj!, phoneNumber: self.phoneNumber)
                        }
                        else{}
                        
                        
                    }
                    else if agreementObj!.agreementObject_mobile_accept == "Y"
                    {
                        if self.getProfileObj!.getProfileObject_level == 1
                        {
                            self.loginAgreementAccept_Yes(setTear: .T1T2)//TestRegis
                        }
                        else if self.getProfileObj!.getProfileObject_level == 2
                        {
                            self.loginAgreementAccept_Yes(setTear: .T1T2)
                        }
                        else if self.getProfileObj!.getProfileObject_level == 3
                        {
                            self.loginAgreementAccept_Yes(setTear: .T3)
                        }
                        else{}
                    }
                    else{}
                    
                }
                else
                {
                    self.alertControllWith(title: signinObj!.signinObject_message, button: "ลองใหม่อีกครั้ง")
                }
    
            }
            
            
        }) { (error) -> Void in
            print(error)
            JGProgressHUD.dismisProgress()
            self.alertControllWith(title: error.localizedDescription, button: "ลองใหม่อีกครั้ง")
        }

        
    }

    //MARK: - LOGIN Status
    
    func loginAgreementAccept_Yes(setTear:MainTear)
    {
        
        UserDefaults.standard.setValue(self.txt_Pin.text!, forKey: "user_pin")
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let mainViewController = storyboard.instantiateViewController(withIdentifier: "MainViewControllerID") as! MainViewController
        let menuViewController = storyboard.instantiateViewController(withIdentifier: "MenuViewControllerID") as! MenuViewController
        
        let nav: UINavigationController = UINavigationController(rootViewController: mainViewController)
        nav.setNavigationBarHidden(true, animated: false)
        
        menuViewController.mainViewController = nav
        
        if setTear == .T1T2
        {
            mainViewController .setType = .T1T2
            menuViewController .menuTear = .T1T2
        }
        else if setTear == .T3
        {
            mainViewController .setType = .T3
            menuViewController .menuTear = .T3
        }
        else {}
        
        
        mainViewController.phoneNumber = self.phoneNumber
        let slideMenuController = ExSlideMenuController(mainViewController:nav, leftMenuViewController: menuViewController)
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        slideMenuController.delegate = mainViewController
        UIApplication.shared.keyWindow?.rootViewController = slideMenuController
        UIApplication.shared.keyWindow?.makeKeyAndVisible()
    }
    
    // MARK: - IBAction
    @IBAction func confirmDidSelect(_ sender: Any)
    {
        if (self.txt_Pin.text?.length)! > 0
        {
             let agreementObj = AgreementObject.init(value:self.getProfileObj!.getProfileObject_agreement[2])
            if self.txt_Pin.text == "1"
            {
                self.loginSeccess(successTear: "T1T2",agreementObj: agreementObj!, phoneNumber: "")
            }
            else
            {
             self.requestServiceLogin()
            }
        }
        else
        {
            self.alertControllWith(title: "ใส่รหัส PIN ไม่ถูกต้อง", button: "ลองใหม่อีกครั้ง")
        }
//        let storyboard = UIStoryboard(name: "RequestOTPStoryboard", bundle: nil)
//        let mainRequestOTPViewController = storyboard.instantiateViewController(withIdentifier: "MainRequestOTPViewControllerID") as! MainRequestOTPViewController
//        
//        mainRequestOTPViewController.phoneNumber = self.phoneNumber
//        let nav: UINavigationController = UINavigationController(rootViewController: mainRequestOTPViewController)
//        nav.setNavigationBarHidden(true, animated: false)
//        
//        self.present(nav,animated: true)
    }
    
    @IBAction func forgotDidSelect(_ sender: Any)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let forgotViewController = storyboard.instantiateViewController(withIdentifier: "ForgotViewControllerID") as! ForgotPasswordViewController
        
        forgotViewController .setType = ForgotT.T1T2
        self.present(forgotViewController, animated: true, completion: nil)
    }
    
    
// MARK: - UITEXTFIELD DELEGATE
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.txt_Pin.resignFirstResponder()
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        let maxLength = 4
        let currentString: NSString = textField.text! as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
// MARK: - PUSH VIEW CONTROLLER
    
    func loginSeccess(successTear tear:String , agreementObj:AgreementObject , phoneNumber:String)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let agreementViewController = storyboard.instantiateViewController(withIdentifier: "AgreementViewControllerID") as! AgreementViewController
        
        if tear == "T1"
        {
          agreementViewController .setType = .T1T2
        }
        else
        {
           agreementViewController .setType = .T3
        }
        
        agreementViewController.phoneNumber = phoneNumber
        agreementViewController.agreementObject = agreementObj
        self.present(agreementViewController, animated: true, completion: nil)
        
    }
    
    
    //MARK: - Alert
    func alertControllWith(title:String, button:String)
    {
        let alert = CFAlertViewController.alert(withTitle: title, message: nil, textAlignment: NSTextAlignment.center, preferredStyle: CFAlertControllerStyle.alert) {
            
            print("Dismiss")
            
        }
        
        let actionDefault = CFAlertAction.init(title: button, style: CFAlertActionStyle.default, alignment: CFAlertActionAlignment.justified, color: UIColor().hexStringToUIColor(hex: "#F7941D")) { action  in
            
            
        }
        
        alert .addAction(actionDefault!)
        
        self.present(alert, animated: true, completion: nil)
    }

}

extension LoginViewController
{
    func hideKeyboard()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(LoginViewController.dismissKeyboard))
        
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard()
    {
        view.endEditing(true)
    }
}








