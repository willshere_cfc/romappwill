//
//  RegisterObject.swift
//  RomApp
//
//  Created by Phutsita Aphilertrungchut on 1/29/2560 BE.
//  Copyright © 2560 Willshere. All rights reserved.
//

import UIKit
import SwiftyJSON

class RegisterObject: NSObject {
    let registerObject_status : String
    let registerObject_message : String
    let registerObject_response_code : String
    let registerObject_response_message : String
    let registerObject_transactionid : String
    let registerObject_datetime : String
    
    init(registerObject_status : String,
         registerObject_message : String,
         registerObject_response_code : String,
         registerObject_response_message : String,
         registerObject_transactionid : String,
         registerObject_datetime : String){
        self.registerObject_status = registerObject_status
        self.registerObject_message = registerObject_message
        self.registerObject_response_code = registerObject_response_code
        self.registerObject_response_message = registerObject_response_message
        self.registerObject_transactionid = registerObject_transactionid
        self.registerObject_datetime = registerObject_datetime
    }
    
    required convenience init?(value:JSON){
        self.init(registerObject_status : value["status"].stringValue,
                  registerObject_message : value["message"].stringValue,
                  registerObject_response_code : value["response_code"].stringValue,
                  registerObject_response_message : value["response_message"].stringValue,
                  registerObject_transactionid : value["transactionid"].stringValue,
                  registerObject_datetime : value["datetime"].stringValue)
    }
}
