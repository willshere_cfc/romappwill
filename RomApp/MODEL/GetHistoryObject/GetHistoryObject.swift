//
//  GetHistoryObject.swift
//  RomApp
//
//  Created by Phutsita Aphilertrungchut on 1/30/2560 BE.
//  Copyright © 2560 Willshere. All rights reserved.
//

import UIKit
import SwiftyJSON

class GetHistoryObject: NSObject {
    let getHistoryObject_status : String
    let getHistoryObject_message : String
    let getHistoryObject_response_code : String
    let getHistoryObject_response_message : String
    let getHistoryObject_datetime : String
    let getHistoryObject_history : JSON
    
    init(getHistoryObject_status : String,
         getHistoryObject_message : String,
         getHistoryObject_response_code : String,
         getHistoryObject_response_message : String,
         getHistoryObject_datetime : String,
         getHistoryObject_history : JSON){
        self.getHistoryObject_status = getHistoryObject_status
        self.getHistoryObject_message = getHistoryObject_message
        self.getHistoryObject_response_code = getHistoryObject_response_code
        self.getHistoryObject_response_message = getHistoryObject_response_message
        self.getHistoryObject_datetime = getHistoryObject_datetime
        self.getHistoryObject_history = getHistoryObject_history
    }
    
    required convenience init?(value:JSON){
        self.init(getHistoryObject_status : value["status"].stringValue,
                  getHistoryObject_message : value["message"].stringValue,
                  getHistoryObject_response_code : value["response_code"].stringValue,
                  getHistoryObject_response_message : value["response_message"].stringValue,
                  getHistoryObject_datetime : value["datetime"].stringValue,
                  getHistoryObject_history : value["history"])
    }
}

class HistoryObject : NSObject{
    let historyObject_transcode : String
    let historyObject_transname : String
    let historyObject_totalamount : Double
    let historyObject_totalcommission : Double
    let historyObject_detail : JSON
    
    init(historyObject_transcode : String,
         historyObject_transname : String,
         historyObject_totalamount : Double,
         historyObject_totalcommission : Double,
         historyObject_detail : JSON){
        self.historyObject_transcode = historyObject_transcode
        self.historyObject_transname = historyObject_transname
        self.historyObject_totalamount = historyObject_totalamount
        self.historyObject_totalcommission = historyObject_totalcommission
        self.historyObject_detail = historyObject_detail
    }
    
    required convenience init?(value:JSON){
        self.init(historyObject_transcode : value["transcode"].stringValue,
                  historyObject_transname : value["transname"].stringValue,
                  historyObject_totalamount : value["totalamount"].doubleValue,
                  historyObject_totalcommission : value["totalcommission"].doubleValue,
                  historyObject_detail : value["detail"])
    }
}

class HistoryDetailObject : NSObject{
    let historyDetailObject_transdate : String
    let historyDetailObject_transid : String
    let historyDetailObject_mobile_b : String
    let historyDetailObject_amount : Double
    let historyDetailObject_channel : String
    let historyDetailObject_transstatus : String
    let historyDetailObject_is_reverse : String
    
    init(historyDetailObject_transdate : String,
         historyDetailObject_transid : String,
         historyDetailObject_mobile_b : String,
         historyDetailObject_amount : Double,
         historyDetailObject_channel : String,
         historyDetailObject_transstatus : String,
         historyDetailObject_is_reverse : String){
        self.historyDetailObject_transdate = historyDetailObject_transdate
        self.historyDetailObject_transid = historyDetailObject_transid
        self.historyDetailObject_mobile_b = historyDetailObject_mobile_b
        self.historyDetailObject_amount = historyDetailObject_amount
        self.historyDetailObject_channel = historyDetailObject_channel
        self.historyDetailObject_transstatus = historyDetailObject_transstatus
        self.historyDetailObject_is_reverse = historyDetailObject_is_reverse
    }
    
    required convenience init?(value:JSON){
        self.init(historyDetailObject_transdate : value["transdate"].stringValue,
                  historyDetailObject_transid : value["transid"].stringValue,
                  historyDetailObject_mobile_b : value["mobile_b"].stringValue,
                  historyDetailObject_amount : value["amount"].doubleValue,
                  historyDetailObject_channel : value["channel"].stringValue,
                  historyDetailObject_transstatus : value["transstatus"].stringValue,
                  historyDetailObject_is_reverse : value["is_reverse"].stringValue)
    }
}
