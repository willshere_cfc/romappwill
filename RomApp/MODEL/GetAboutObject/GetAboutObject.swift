//
//  GetAboutObject.swift
//  RomApp
//
//  Created by Chanachon Pruchayahmaytha on 2/6/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit
import SwiftyJSON

class GetAboutObject: NSObject {
   
    
    let getAboutObject_status : String
    let getAboutObject_message : String
    let getAboutObject_responseCode : String
    let getAboutObject_firstnName : String
    let getAboutObject_lastName : String
    let getAboutObject_shortName: String
    let getAboutObject_officialAddress : String
    let getAboutObject_contactAddress : String
    let getAboutObject_birthdate : String
    let getAboutObject_idCard : String
    let getAboutObject_agentMobile: String
    let getAboutObject_contactMobile : String
    let getAboutObject_email : String
    let getAboutObject_occupation : String
    
    init(getaboutobject_status:String,
         getaboutobject_message:String,
         getaboutobject_resonseCode:String,
         getaboutobject_firstname:String,
         getaboutobject_lastname:String,
         getaboutobject_shortname:String,
         getaboutobject_officialAddress:String,
         getaboutobject_contactAddress:String,
         getaboutobject_birthdate : String,
         getaboutobject_idcard : String,
         getaboutobject_agentMobile:String,
         getaboutobject_contactMobile:String,
         getaboutobject_email : String,
         getaboutobject_occupation : String
         ){
        self.getAboutObject_status = getaboutobject_status
        self.getAboutObject_message = getaboutobject_message
        self.getAboutObject_responseCode = getaboutobject_resonseCode
        self.getAboutObject_firstnName = getaboutobject_firstname
        self.getAboutObject_lastName = getaboutobject_lastname
        self.getAboutObject_shortName = getaboutobject_shortname
        self.getAboutObject_officialAddress = getaboutobject_officialAddress
        self.getAboutObject_contactAddress = getaboutobject_contactAddress
        self.getAboutObject_birthdate = getaboutobject_birthdate
        self.getAboutObject_idCard = getaboutobject_idcard
        self.getAboutObject_agentMobile = getaboutobject_agentMobile
        self.getAboutObject_contactMobile = getaboutobject_contactMobile
        self.getAboutObject_email = getaboutobject_email
        self.getAboutObject_occupation = getaboutobject_occupation
    }
    
    required convenience init?(value:JSON){
 
        self.init(getaboutobject_status:value["status"].stringValue,
                  getaboutobject_message:value["message"].stringValue,
                  getaboutobject_resonseCode:value["response_code"].stringValue,
                  getaboutobject_firstname:value["first_name"].stringValue,
                  getaboutobject_lastname:value["last_name"].stringValue,
                  getaboutobject_shortname:value["short_name"].stringValue,
                  getaboutobject_officialAddress:value["official_address"].stringValue,
                  getaboutobject_contactAddress:value["contact_address"].stringValue,
                  getaboutobject_birthdate : value["birthdate"].stringValue,
                  getaboutobject_idcard : value["idcard"].stringValue,
                  getaboutobject_agentMobile:value["agent_mobile"].stringValue,
                  getaboutobject_contactMobile:value["contact_mobile"].stringValue,
                  getaboutobject_email : value["email"].stringValue,
                  getaboutobject_occupation : value["occupation"].stringValue)
    }
}
