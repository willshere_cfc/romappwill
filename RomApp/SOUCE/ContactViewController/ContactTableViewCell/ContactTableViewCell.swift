//
//  ContactTableViewCell.swift
//  RomApp
//
//  Created by Administrator on 1/11/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit

protocol ContectDelegate {
    
    func contactDidSelect(row:Int, buttonImg:UIButton, secion: Int)
}

class ContactTableViewCell: UITableViewCell {

    @IBOutlet weak var btn_Favorite: UIButton!
    
    @IBOutlet weak var lbl_Title: UILabel!
    
    @IBOutlet weak var lbl_Phone: UILabel!
    
    var delegate : ContectDelegate?
    
    var secion : Int = 0
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    open override func awakeFromNib() {
    }
    
    open func setup() {
    }
    
    open func setButtonFavoriteShow() {
        
        self.btn_Favorite.setBackgroundImage(UIImage(named: "Favorite_ac_ic"), for: UIControlState.normal)

    }
    
    open func setButtonFavoriteHiden() {
        
        self.btn_Favorite.setBackgroundImage(UIImage(named: "Favorite_non_ac_ic"), for: UIControlState.normal)
        
    }
    
    open func settingContactCellWithObject(obj:ContactListObject)
    {
        self.lbl_Title.text = "\(obj.contactListObject_nick_name)"
        self.lbl_Phone.text = String().stringFormatPhoneNumber(phoneNumber: "\(obj.contactListObject_mobile)")
        
    }

    @IBAction func favoriteDidSelect(_ sender: Any)
    {
        self.delegate? .contactDidSelect(row: self.tag, buttonImg: self.btn_Favorite, secion: self.secion)
    }
    

}
