//
//  GetOccupationObject.swift
//  RomApp
//
//  Created by Phutsita Aphilertrungchut on 1/29/2560 BE.
//  Copyright © 2560 Willshere. All rights reserved.
//

import UIKit
import SwiftyJSON

class GetOccupationObject: NSObject {
    let getOccupationObject_status : String
    let getOccupationObject_message : String
    let getOccupationObject_response_code : String
    let getOccupationObject_response_message : String
    let getOccupationObject_datetime : String
    let getOccupationObject_occupation : JSON
    
    init(getOccupationObject_status : String,
         getOccupationObject_message : String,
         getOccupationObject_response_code : String,
         getOccupationObject_response_message : String,
         getOccupationObject_datetime : String,
         getOccupationObject_occupation : JSON){
        self.getOccupationObject_status = getOccupationObject_status
        self.getOccupationObject_message = getOccupationObject_message
        self.getOccupationObject_response_code = getOccupationObject_response_code
        self.getOccupationObject_response_message = getOccupationObject_response_message
        self.getOccupationObject_datetime = getOccupationObject_datetime
        self.getOccupationObject_occupation = getOccupationObject_occupation
    }
    
    required convenience init?(value:JSON){
        self.init(getOccupationObject_status : value["status"].stringValue,
                  getOccupationObject_message : value["message"].stringValue,
                  getOccupationObject_response_code : value["response_code"].stringValue,
                  getOccupationObject_response_message : value["response_message"].stringValue,
                  getOccupationObject_datetime : value["datetime"].stringValue,
                  getOccupationObject_occupation : value["occupation"])
    }
}

class OccupationObject:NSObject{
    let occupationObject_occupation_code : String
    let occupationObject_occupation_name : String
    let occupationObject_source_income : String
    
    init(occupationObject_occupation_code : String,
         occupationObject_occupation_name : String,
         occupationObject_source_income : String){
        self.occupationObject_occupation_code = occupationObject_occupation_code
        self.occupationObject_occupation_name = occupationObject_occupation_name
        self.occupationObject_source_income = occupationObject_source_income
    }
    
    required convenience init?(value:JSON){
        self.init(occupationObject_occupation_code : value["occupation_code"].stringValue,
                  occupationObject_occupation_name : value["occupation_name"].stringValue,
                  occupationObject_source_income : value["source_income"].stringValue)
    }
}
