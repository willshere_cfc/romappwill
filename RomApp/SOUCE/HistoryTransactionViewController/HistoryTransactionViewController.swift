//
//  HistoryTransactionViewController.swift
//  RomApp
//
//  Created by Administrator on 1/14/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit
import JGProgressHUD
import CFAlertViewController

class HistoryTransactionViewController: UIViewController {

    @IBOutlet weak var btn_Reverse: UIButton!
    
    @IBOutlet weak var txt_AgentCode: UITextField!
    
    @IBOutlet weak var view_AgentCode: UIView!
    
    @IBOutlet weak var view_BackToMain: UIView!
    
    @IBOutlet weak var img_UnderReverse: UIImageView!
    
    @IBOutlet weak var view_AgentAndBack: UIView!
    
    @IBOutlet weak var view_SeccessAgentCode: UIView!
    
    //View Check Reverse
    @IBOutlet weak var view_Reverse: UIView!
    @IBOutlet weak var view_Close: UIView!
    //============
    
    @IBOutlet weak var lbl_Phone: UILabel!
    @IBOutlet weak var lbl_Price: UILabel!
    @IBOutlet weak var lbl_Date: UILabel!
    @IBOutlet weak var lbl_Transion: UILabel!
    @IBOutlet weak var lbl_Status: UILabel!
    
    var historyDetailObj : HistoryDetailObject? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupUIReverse()
        self.setupUI()
        
        self.view_AgentCode.isHidden = true
        self.view_SeccessAgentCode.isHidden = true
        self.view_BackToMain.isHidden = false
        self.view_AgentAndBack.isHidden = false
        
        
        // Do any additional setup after loading the view.
    }
    
    func setupUIReverse()
    {
        if self.historyDetailObj!.historyDetailObject_is_reverse == "Y"
        {
            self.view_Reverse.isHidden = false
            self.view_Close.isHidden = true
        }
        else
        {
            self.view_Reverse.isHidden = true
            self.view_Close.isHidden = false
        }
    }
    
    func setupUI()
    {
        self.lbl_Phone.text = "\(self.historyDetailObj!.historyDetailObject_mobile_b)"
        self.lbl_Price.text = "\(self.historyDetailObj!.historyDetailObject_amount)"
        self.lbl_Date.text = "\(self.historyDetailObj!.historyDetailObject_transdate)"
        self.lbl_Transion.text = "\(self.historyDetailObj!.historyDetailObject_transid)"
        self.lbl_Status.text = "\(self.historyDetailObj!.historyDetailObject_transstatus)"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - SERVICE
    
    func requestServiceROMTopupReverse()
    {
        let defult = UserDefaults.standard
        let dataDF =  defult.value(forKey: "user_phonenumber")
        
        let str = "\(self.historyDetailObj!.historyDetailObject_transid)"
        
        let index = str.index(str.startIndex, offsetBy: 8)
        
      let substring1 =  str.substring(from: index)
        
        
        let parameters: [String: Dictionary] =
            [
                "data":
                    [
                        "mobileno_a": "\(dataDF!)",
                        "mobileno_b": "\(self.historyDetailObj!.historyDetailObject_mobile_b)",
                        "pin": "\(self.txt_AgentCode.text!)",
                        "refno": "\(substring1)"
                ]
        ]
        
        let strURL = "http://stg-etopupservices.ais.co.th/dev-ROMMobileServiceAPI/ROMService.svc/ROMTopupReverse/"
        
        print("params => \(parameters)")
        
        JGProgressHUD.loadingProgress(view: self.view)
        AFWrapper.requestPOSTURL(strURL, params: parameters as [String : AnyObject]?, headers: nil, success: { (JSONResponse) ->  Void in
            
            print(JSONResponse)
            
            JGProgressHUD.dismisProgress()
            print("ROMTopupReverse => \(JSONResponse)")
            if JSONResponse.count != 0
            {
                let romTopupReverseObj = ROMTopupReverseObject.init(value: JSONResponse)
                
                if romTopupReverseObj!.romTopupReverseObject_status == "success"
                {
                     self.alertControllWith(title: romTopupReverseObj!.romTopupReverseObject_message, button: "ตกลง" , ststus: "success")
                }
                else
                {
                    self.alertControllWith(title: romTopupReverseObj!.romTopupReverseObject_message, button: "ตกลง", ststus: "fail")
                }
            }
            
            
        }) { (error) -> Void in
            print(error)
            JGProgressHUD.dismisProgress()
            self.alertControllWith(title: "\(error.localizedDescription)", button: "ตกลง", ststus: "")
        }
    }


    //MARK: - Alert
    func alertControllWith(title:String, button:String, ststus:String)
    {
        let alert = CFAlertViewController.alert(withTitle: title, message: nil, textAlignment: NSTextAlignment.center, preferredStyle: CFAlertControllerStyle.alert) {
            
            if ststus == "success"
            {
                self.view_AgentCode.isHidden = true
                self.view_SeccessAgentCode.isHidden = false
                self.view_BackToMain.isHidden = true
                self.view_AgentAndBack.isHidden = true
                
            }
            else if ststus == "fail"
            {
                self.reverseDidSelect(Any.self)
            }
            else {}
            
        }
        
        let actionDefault = CFAlertAction.init(title: button, style: CFAlertActionStyle.default, alignment: CFAlertActionAlignment.justified, color: UIColor().hexStringToUIColor(hex: "#F7941D")) { action  in
            
            
        }
        
        alert .addAction(actionDefault!)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: IBAction
    
    @IBAction func reverseDidSelect(_ sender: Any)
    {
        self.btn_Reverse.isSelected = !self.btn_Reverse.isSelected
        
        if(self.btn_Reverse.isSelected == true)
        {
            self.view_AgentCode.isHidden = false
            self.view_SeccessAgentCode.isHidden = true
            self.view_BackToMain.isHidden = true
            self.view_AgentAndBack.isHidden = false
            self.img_UnderReverse.image = UIImage(named: "up_ic")
            
        }
        else
        {
            self.view_AgentCode.isHidden = true
            self.view_SeccessAgentCode.isHidden = true
            self.view_BackToMain.isHidden = false
            self.view_AgentAndBack.isHidden = false
            self.img_UnderReverse.image = UIImage(named: "down_ic")
        }
        
       
    }
    
    @IBAction func confirmAgentCodeDidSelect(_ sender: Any)
    {
      self.requestServiceROMTopupReverse()
    }
    
    
    @IBAction func backToMainDidSelect(_ sender: Any)
    {
        _ = self.navigationController?.popToRootViewController(animated: true)
    }
    
    
    @IBAction func successDidSelect(_ sender: Any)
    {
        _ = self.navigationController?.popToRootViewController(animated: true)
    }
    
 

    
    
    
}
