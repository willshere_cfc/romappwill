//
//  RegisterCameraViewController.swift
//  RomApp
//
//  Created by Administrator on 1/17/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit

public enum Type :Int{
    
    case img1
    case img2
    case img3
    case img4
    case img5
    case null
    
}

class RegisterCameraViewController: UIViewController {

    var img_Type:Type = .null
    
    
    @IBOutlet weak var img_1: UIImageView!
    @IBOutlet weak var img_2: UIImageView!
    @IBOutlet weak var img_3: UIImageView!
    @IBOutlet weak var img_4: UIImageView!
    @IBOutlet weak var img_5: UIImageView!
    
    @IBOutlet weak var lbl_1: UILabel!
    @IBOutlet weak var lbl_2: UILabel!
    @IBOutlet weak var lbl_3: UILabel!
    @IBOutlet weak var lbl_4: UILabel!
    @IBOutlet weak var lbl_5: UILabel!
    
    @IBOutlet weak var btn_Confirm: CustomizableButton!
    
    //MARK: - Pass Params
    var param_NameTH: String!
    var param_LastNameTH: String!
    var param_NameEN: String!
    var param_LastNameEN: String!
    var param_BirthDate: String!
    var param_CardID: String!
    var param_PhoneNumber: String!
    var param_Job: String!
    var param_Email: String! = ""
    
    var param_Address_address: String!
    var param_Address_road: String!
    var param_Address_province: String!
    var param_Address_district: String!
    var param_Address_zone: String!
    var param_Address_zipcode: String!
    
    var param_Live_address: String!
    var param_Live_road: String!
    var param_Live_province: String!
    var param_Live_district: String!
    var param_Live_zone: String!
    var param_Live_zipcode: String!
    
    var param_region_code: String!
    var param_province_code: String!
    var param_region_code_live: String!
    var param_province_code_live: String!
    
     var typeRegis : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        print("live => \(param_region_code_live)")
        print("live => \(param_province_code_live)")
        
        self.btn_Confirm.isHidden = true
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
       
        // Dispose of any resources that can be recreated.
    }
    
    func pushViewController()
    {
        let storyboard = UIStoryboard(name: "RegisterStoryboard", bundle: nil)
        let registerConfirmViewController = storyboard.instantiateViewController(withIdentifier: "RegisterConfirmViewControllerID") as! RegisterConfirmViewController
        
        registerConfirmViewController.param_NameTH = self.param_NameTH
        registerConfirmViewController.param_LastNameTH = self.param_LastNameTH
        registerConfirmViewController.param_NameEN = self.param_NameEN
        registerConfirmViewController.param_LastNameEN = self.param_LastNameEN
        registerConfirmViewController.param_BirthDate = self.param_BirthDate
        registerConfirmViewController.param_CardID = self.param_CardID
        registerConfirmViewController.param_PhoneNumber = self.param_PhoneNumber
        registerConfirmViewController.param_Job = self.param_Job
        registerConfirmViewController.param_Email = self.param_Email
        registerConfirmViewController.param_region_code = self.param_region_code
        registerConfirmViewController.param_province_code = self.param_province_code
        registerConfirmViewController.param_region_code_live = self.param_region_code_live
        registerConfirmViewController.param_province_code_live = self.param_province_code_live
        
        registerConfirmViewController.param_Address_address = self.param_Address_address
        registerConfirmViewController.param_Address_road = self.param_Address_road
        registerConfirmViewController.param_Address_province = self.param_Address_province
        registerConfirmViewController.param_Address_district = self.param_Address_district
        registerConfirmViewController.param_Address_zone = self.param_Address_zone
        registerConfirmViewController.param_Address_zipcode = self.param_Address_zipcode
        
        registerConfirmViewController.param_Live_address = self.param_Live_address
        registerConfirmViewController.param_Live_road = self.param_Live_road
        registerConfirmViewController.param_Live_province = self.param_Live_province
        registerConfirmViewController.param_Live_district = self.param_Live_district
        registerConfirmViewController.param_Live_zone = self.param_Live_zone
        registerConfirmViewController.param_Live_zipcode = self.param_Live_zipcode
        
        registerConfirmViewController.img1 = self.img_1.image
        registerConfirmViewController.img2 = self.img_2.image
        registerConfirmViewController.img3 = self.img_3.image
        registerConfirmViewController.img4 = self.img_4.image
        registerConfirmViewController.img5 = self.img_5.image
        
         registerConfirmViewController.typeRegis = self.typeRegis
        
        navigationController?.pushViewController(registerConfirmViewController, animated: true)
        
  
    }
    
    //MARK: - IBAction
    
    @IBAction func confirmDidSelect(_ sender: Any)
    {
       self.pushViewController()
        
    }

    @IBAction func leftNavbarDidSelect(_ sender: Any)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - Image leftNavbarDidSelect
    
    @IBAction func image1DidSlect(_ sender: Any)
    {
        self.img_Type = .img1
        self.imagePicker(sender)
    }
    
    @IBAction func image2DidSlect(_ sender: Any)
    {
       self.img_Type = .img2
        self.imagePicker(sender)
    }
    
    @IBAction func image3DidSlect(_ sender: Any)
    {
        self.img_Type = .img3
        self.imagePicker(sender)
    }
    
    @IBAction func image4DidSlect(_ sender: Any)
    {
        self.img_Type = .img4
        self.imagePicker(sender)
    }
    
    @IBAction func image5DidSlect(_ sender: Any)
    {
       self.img_Type = .img5
        self.imagePicker(sender)
    }
    
    
    
    
    
    
    //MARK: - UIAlert Controller
    
    func imagePicker(_ sender: Any)
    {
        let alertController =  UIAlertController.alertWithTitle(title: "", message: "", buttonTitle: "")
        
        alertController.addAction(.cancel)
        
        alertController.addAction(.cameraPhoto
            { action in
                self.cameraPick()
            })
        
        alertController.addAction(.libraryPhoto
            { action in
                
                self.liberyPick()
            })
        
        alertController.modalPresentationStyle = UIModalPresentationStyle.popover
        
        // set up the popover presentation controller
        alertController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.up
        alertController.popoverPresentationController?.delegate = self
        alertController.popoverPresentationController?.sourceView = sender as? UIView // button
        alertController.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
        
        // present the popover
        self.present(alertController, animated: true, completion: nil)
    }
    
    //MARK: - UIImage Picker Controller
    func cameraPick()
    {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .camera
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    func liberyPick()
    {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary
        
        present(imagePicker, animated: true, completion: nil)
    }

}

// MARK: - Extansion ============

//MARK: - UIPopoverPresentationController Delegate

extension RegisterCameraViewController : UIPopoverPresentationControllerDelegate {
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        // return UIModalPresentationStyle.FullScreen
        return UIModalPresentationStyle.none
    }
}

//MARK: - UIImagePickerController Delegate

extension RegisterCameraViewController : UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            
            switch self.img_Type {
            case .img1:
                self.img_1.image = pickedImage
                self.lbl_1.isHidden = true
            case .img2:
                self.img_2.image = pickedImage
                self.lbl_2.isHidden = true
            case .img3:
                self.img_3.image = pickedImage
                self.lbl_3.isHidden = true
            case .img4:
                self.img_4.image = pickedImage
                self.lbl_4.isHidden = true
            case .img5:
                self.img_5.image = pickedImage
                self.lbl_5.isHidden = true
            default:
                break
            }
        }
        
        if self.img_1.image != nil && self.img_2.image != nil && self.img_3.image != nil && self.img_4.image != nil
        {
            self.btn_Confirm.isHidden = false
        }
        
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        dismiss(animated: true, completion: nil)
    }
    
}

extension RegisterCameraViewController : UINavigationControllerDelegate {
    
    
}

