//
//  TopupACViewController.swift
//  RomApp
//
//  Created by Administrator on 1/5/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit
import JGProgressHUD
import SwiftyJSON
import CFAlertViewController

class TopupACViewController: UIViewController {

    @IBOutlet weak var txt_AgentCode: UITextField!
    
    @IBOutlet weak var lbl_PhoneNumber: UILabel!
    @IBOutlet weak var lbl_FaceValue: UILabel!
    
    var topupPhoneNumber : NSString?
    var topupFaceValue : NSString?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        lbl_PhoneNumber.text = topupPhoneNumber as String?
        lbl_FaceValue.text = topupFaceValue as String?
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - IBAction
    
    @IBAction func leftNavbarDidSelect(_ sender: Any)
    {
         _ = self.navigationController?.popViewController(animated: true)
    }

    @IBAction func confirmDidSelect(_ sender: Any)
    {
        //Try Send Topup Before passing object to result page
        if(txt_AgentCode.text?.length == 4){
            self.requestServiceTopup()
        }
    }
    
    func showTopupResultPage(topupObj : TopUpObject){
        //Goto Result Page
        let storyboard = UIStoryboard(name: "TopUpStoryBoard", bundle: nil)
        let topupTransactionViewController = storyboard.instantiateViewController(withIdentifier: "TopupTransactionViewControllerID") as! TopupTransactionViewController
        topupTransactionViewController.topupPhoneNumber = self.topupPhoneNumber
        topupTransactionViewController.topupFaceValue = self.topupFaceValue
        topupTransactionViewController.topupObj = topupObj
        navigationController?.pushViewController(topupTransactionViewController, animated: true)
    }
    
    // MARK: - UITEXTFIELD DELEGATE
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.txt_AgentCode.resignFirstResponder()
        
        return true
    }
    
    //MARK Confirm Topup
    func requestServiceTopup(){
        let defult = UserDefaults.standard
        let dataDF = defult.value(forKey: "user_phonenumber")
        
        let strURL = "http://stg-etopupservices.ais.co.th/dev-ROMMobileServiceAPI/ROMService.svc/Topup/"
        
        let parameters: [String: Dictionary] =
            [
                "data":
                    [
                        "mobileno_a": dataDF!,
                        "mobileno_b": self.topupPhoneNumber as Any,
                        "pin": txt_AgentCode.text!,
                        "amount":self.topupFaceValue ?? "0"
                ]
        ]
        
        JGProgressHUD.loadingProgress(view: self.view)
        AFWrapper.requestPOSTURL(strURL, params:parameters as [String : AnyObject]?, headers: nil, success: { (JSONResponse) ->  Void in
            print("top up => \(JSONResponse)")
            if JSONResponse.count != 0
            {
                let topupObj:TopUpObject! = TopUpObject.init(value: JSONResponse)
                
                if(topupObj.topupObject_status == "success"){
                    self.showTopupResultPage(topupObj: topupObj)
                }
                else
                {
                   if topupObj.topupObject_responseCode == "BTWS070" ||   topupObj.topupObject_responseCode == "BTWS034"
                   {
                    self.alertControllWith(title: "\(topupObj.topupObject_message)", button: "ลองใหม่อีกครั้ง")
                   }
                    else
                   {
                     self.pushVC(topupObj:topupObj)
                    }
                   
                }
            }
            
            JGProgressHUD.dismisProgress()
            
            
        }) { (error) -> Void in
            print(error)
            JGProgressHUD.dismisProgress()
            self.alertControllWith(title: "\(error)", button: "ตกลง")
        }
        
    }
    
    func pushVC(topupObj:TopUpObject)
    {
        let storyboard = UIStoryboard(name: "TopUpStoryBoard", bundle: nil)
        let topupStatusViewController = storyboard.instantiateViewController(withIdentifier: "TopupStatusViewControllerID") as! TopupStatusViewController
        
        topupStatusViewController.statusString = "\(topupObj.topupObject_message)"
        
        navigationController?.pushViewController(topupStatusViewController, animated: true)
    }
    
    //MARK: - Alert
    func alertControllWith(title:String, button:String)
    {
        let alert = CFAlertViewController.alert(withTitle: title, message: nil, textAlignment: NSTextAlignment.center, preferredStyle: CFAlertControllerStyle.alert) {
            
            print("Dismiss")
            
        }
        
        let actionDefault = CFAlertAction.init(title: button, style: CFAlertActionStyle.default, alignment: CFAlertActionAlignment.justified, color: UIColor().hexStringToUIColor(hex: "#F7941D")) { action  in
            
            
        }
        
        alert .addAction(actionDefault!)
        
        self.present(alert, animated: true, completion: nil)
    }



}
