//
//  GetContactObject.swift
//  RomApp
//
//  Created by Administrator on 1/27/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit
import SwiftyJSON

class GetContactObject: NSObject {

    
    let getContactObject_status : String
    let getContactObject_message : String
    let getContactObject_response_code : String
    let getContactObject_response_message : String
    let getContactObject_transactionid : String
    let getContactObject_datetime : String
    let getContactObject_contract_l : JSON
    let getContactObject_contract_f : JSON
    
    init(getContactObject_status : String,
     getContactObject_message : String,
     getContactObject_response_code : String,
     getContactObject_response_message : String,
     getContactObject_transactionid : String,
     getContactObject_datetime : String,
     getContactObject_contract_l : JSON,
     getContactObject_contract_f : JSON)
    {
        self.getContactObject_status = getContactObject_status
        self.getContactObject_message = getContactObject_message
        self.getContactObject_response_code = getContactObject_response_code
        self.getContactObject_response_message = getContactObject_response_message
        self.getContactObject_transactionid = getContactObject_transactionid
        self.getContactObject_datetime = getContactObject_datetime
        self.getContactObject_contract_l = getContactObject_contract_l
        self.getContactObject_contract_f = getContactObject_contract_f
    }
    
    required convenience init?(value:JSON) {
        
        self.init(getContactObject_status : value["status"].stringValue,
                  getContactObject_message : value["message"].stringValue,
                  getContactObject_response_code : value["response_code"].stringValue,
                  getContactObject_response_message : value["response_message"].stringValue,
                  getContactObject_transactionid : value["transactionid"].stringValue,
                  getContactObject_datetime : value["datetime"].stringValue,
                  getContactObject_contract_l : value["contract_l"],
                  getContactObject_contract_f : value["contract_f"])
    }
}

class ContactObject: NSObject {
    
     let contactObject_text_drop : String
     let contactObject_contract_drop : JSON
    
    init(contactObject_text_drop : String,
     contactObject_contract_drop : JSON) {
        self.contactObject_text_drop = contactObject_text_drop
        self.contactObject_contract_drop = contactObject_contract_drop
    }
    
    required convenience init?(value:JSON) {
        
    self.init(contactObject_text_drop : value["text_drop"].stringValue,
              contactObject_contract_drop : value["contract_drop"])
    }
}

class ContactListObject : NSObject {
    
    let contactListObject_id : String
    let contactListObject_agent_id : String
    let contactListObject_favorite : String
    let contactListObject_first_name : String
    let contactListObject_last_name : String
    let contactListObject_mobile : String
    let contactListObject_nick_name : String
    
    init(contactListObject_id : String,
     contactListObject_agent_id : String,
     contactListObject_favorite : String,
     contactListObject_first_name : String,
     contactListObject_last_name : String,
     contactListObject_mobile : String,
     contactListObject_nick_name : String)
    {
        self.contactListObject_id = contactListObject_id
        self.contactListObject_agent_id = contactListObject_agent_id
        self.contactListObject_favorite = contactListObject_favorite
        self.contactListObject_first_name = contactListObject_first_name
        self.contactListObject_last_name = contactListObject_last_name
        self.contactListObject_mobile = contactListObject_mobile
        self.contactListObject_nick_name = contactListObject_nick_name
    }
    
    required convenience init?(value:JSON) {
        
        self.init(contactListObject_id : value["id"].stringValue,
                  contactListObject_agent_id : value["agent_id"].stringValue,
                  contactListObject_favorite : value["favorite"].stringValue,
                  contactListObject_first_name : value["first_name"].stringValue,
                  contactListObject_last_name : value["last_name"].stringValue,
                  contactListObject_mobile : value["mobile"].stringValue,
                  contactListObject_nick_name : value["nick_name"].stringValue)
    }
}

















