//
//  HistoryTransferDetailViewController.swift
//  RomApp
//
//  Created by Administrator on 1/14/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit

class HistoryTransferDetailViewController: UIViewController {

    @IBOutlet weak var txt_Search: UITextField!
    
    @IBOutlet weak var historyTransferTableView: UITableView!
    
    @IBOutlet weak var lbl_Title: UILabel!
    
    @IBOutlet weak var lbl_Total: UILabel!
    
    @IBOutlet weak var lbl_totalamount: UILabel!
    @IBOutlet weak var lbl_totalamount_Satang: UILabel!
    
    @IBOutlet weak var lbl_commission: UILabel!
    
    var historyObj : HistoryObject!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        print("amou => \(self.historyObj!.historyObject_totalamount)")
        self.setupUI()
        // Do any additional setup after loading the view.
    }
    
    func setupUI()
    {
        let string = "\(self.historyObj!.historyObject_totalamount)"
        
        if string.range(of:".") != nil{
            print("exists")
            
            var token = string.components(separatedBy: ".")
            
            self.lbl_totalamount.text = token[0]
            self.lbl_totalamount_Satang.text = ".\(token[1])"
        }
        else
        {
            self.lbl_totalamount.text = "\(self.historyObj.historyObject_totalamount)"
            self.lbl_totalamount_Satang.text = ".00"
        }
        
        self.lbl_commission.text = "\(self.historyObj.historyObject_totalcommission)"
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func leftNavbarDidSelect(_ sender: Any)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
}


//MARK: - EXTENSION

//MARK: - UITextField Delegate

extension HistoryTransferDetailViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.txt_Search .resignFirstResponder()
        
        return true;
    }
}

// MARK: - Tableview Delegate

extension HistoryTransferDetailViewController : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return HistoryTransferDetailTableViewCell.height()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let historyDetailObj = HistoryDetailObject.init(value: self.historyObj!.historyObject_detail[indexPath.row])
        
        let storyboard = UIStoryboard(name: "HistoryStoryboard", bundle: nil)
        let historyTransactionViewController = storyboard.instantiateViewController(withIdentifier: "HistoryTransactionViewControllerID") as! HistoryTransactionViewController
        
        historyTransactionViewController.historyDetailObj = historyDetailObj
        navigationController?.pushViewController(historyTransactionViewController, animated: true)
    }
    
    
}

extension HistoryTransferDetailViewController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.historyObj!.historyObject_detail.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellIdentifier = "HistoryTransferDetailTableViewCellIdentifier"
        let cell:HistoryTransferDetailTableViewCell = self.historyTransferTableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! HistoryTransferDetailTableViewCell
        
        let historyDetailObj = HistoryDetailObject.init(value: self.historyObj!.historyObject_detail[indexPath.row])
        
        cell .settingCellWithObject(obj: historyDetailObj!)
        
        
        return cell
    }
}
