//
//  EPinInputNumberViewController.swift
//  RomApp
//
//  Created by Administrator on 1/4/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit

class EPinInputNumberViewController: UIViewController {

    @IBOutlet weak var txt_PhoneNumber: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - IBAction
    
    @IBAction func leftNavBarDidSelect(_ sender: Any)
    {
         _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func confirmDidSelect(_ sender: Any)
    {
        let storyboard = UIStoryboard(name: "E-PINStoryBoard", bundle: nil)
        let ePinViewController = storyboard.instantiateViewController(withIdentifier: "EPinViewControllerID") as! EPinViewController
        ePinViewController.epinPhoneNumber = (txt_PhoneNumber.text as NSString?)!
        navigationController?.pushViewController(ePinViewController,
                                                 animated: true)
    }
    
    // MARK: - UITEXTFIELD DELEGATE
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.txt_PhoneNumber.resignFirstResponder()
        
        return true
    }
 

}
