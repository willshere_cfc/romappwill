//
//  AppDelegate.swift
//  RomApp
//
//  Created by Administrator on 12/26/16.
//  Copyright © 2016 Willshere. All rights reserved.
//

import UIKit
import CoreTelephony
import IQKeyboardManagerSwift
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?

    

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {
         IQKeyboardManager.sharedManager().enable = true
        
        
        //========= GET TOKEN
        
         if #available(iOS 10.0, *) {
            
            UserDefaults.standard.setValue("fCgzw4x3jWVscXDikOJCdyfwbh8FaYrzaaXQTpHlXs8=", forKey: "user_auth_token")
            
            initViewControllerLoginVC()
            
           
        }
        else if  #available(iOS 8.0, *) {
            
            UserDefaults.standard.setValue("fCgzw4x3jWVscXDikOJCdyfwbh8FaYrzaaXQTpHlXs8=", forKey: "user_auth_token")
            
            initViewControllerLoginVC()
        }
        else
        {
            let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            
            application.registerUserNotificationSettings(settings)
            application.registerForRemoteNotifications()
        }
        
        
           // registerForPushNotifications(application: application) ios 10
     
        
        //=========
        
        UIApplication.shared.statusBarStyle = .default
        
        // Get carrier name
        let networkInfo = CTTelephonyNetworkInfo()
        let carrier = networkInfo.subscriberCellularProvider
        
        let carrierName = carrier?.carrierName
        print("CarrierName \(carrierName)")
        //======================================
        
        // CHECK  Jailbreak
        let filePath = "/Applications/Cydia.app";
        if FileManager.default.fileExists(atPath: filePath)
        {
            print("Applications/Cydia.app")
        }
        //======================================
       
        NotificationCenter.default.addObserver(self, selector: #selector(AppDelegate.initViewControllerLoginVC), name: Notification.Name("NotificationLogout"), object: nil)
        
      
    
        return true
    }
    
    func registerForPushNotifications(application: UIApplication) {
        
        if #available(iOS 10.0, *){
            UNUserNotificationCenter.current().delegate = self
            UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert], completionHandler: {(granted, error) in
                if (granted)
                {
                    UIApplication.shared.registerForRemoteNotifications()
                }
                else{
                    //Do stuff if unsuccessful...
                }
            })
        }
            
        else{ //If user is not on iOS 10 use the old methods we've been using
            let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
          
                      application.registerUserNotificationSettings(settings)
                      application.registerForRemoteNotifications()
        }
        
    }
    
    func initViewControllerLoginVC()
    {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        self.window?.rootViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewControllerID")
        
        self.window?.makeKeyAndVisible()
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        print("3333")
        
        let token = String(data: deviceToken.base64EncodedData(), encoding: .utf8)?.trimmingCharacters(in: CharacterSet.whitespaces).trimmingCharacters(in: CharacterSet(charactersIn: "<>"))
        
         UserDefaults.standard.setValue(token, forKey: "user_auth_token")
        
         initViewControllerLoginVC()
       
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        //Handle the notification
        
        print("111")
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        //Handle the notification
        
        print("222")
    }

}

