//
//  PackageCagtegoryViewController.swift
//  RomApp
//
//  Created by Administrator on 1/4/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit
import JGProgressHUD

//enum PackageCagegoryCell: Int {
//    case topup = 0
//    case sellPackage
//    case sellPIN
//    case transfer
//    case atm
//}

class PackageCagtegoryViewController: UIViewController {

    var phoneNumber:String!
    
    var  getPackageGroupObj : GetPackageGroupObject? = nil
    
    
    
    @IBOutlet weak var packageTableView: UITableView!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        self.requestServicePackage()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - SERVICE
    
    func requestServicePackage()
    {
    
        let parameters: [String: Dictionary] =
            [
                "data":
                    [
                        "mobile_no": "\(self.phoneNumber!)"
                    ]
            ]
        
        let strURL = "http://stg-etopupservices.ais.co.th/dev-ROMMobileServiceAPI/ROMService.svc/GetListPackageCategory/"
        
        print("params => \(parameters)")
        
        JGProgressHUD.loadingProgress(view: self.view)
        AFWrapper.requestPOSTURL(strURL, params: parameters as [String : AnyObject]?, headers: nil, success: { (JSONResponse) ->  Void in
            print(JSONResponse)
            
            JGProgressHUD.dismisProgress()
            print("Get Main => \(JSONResponse)")
            if JSONResponse.count != 0
            {
                self.getPackageGroupObj = GetPackageGroupObject.init(value: JSONResponse)
               
                self.packageTableView.reloadData()
            }
            
            
        }) { (error) -> Void in
            print(error)
            JGProgressHUD.dismisProgress()
        }
    }

    
    // MARK: - IBAction
    
    @IBAction func leftNavbarDidSelect(_ sender: Any)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }

}

// MARK: - EXTENION

// MARK: - Tableview Delegate

extension PackageCagtegoryViewController : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
       
        return MenuTableViewCell.height()
       
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let storyboard = UIStoryboard(name: "PackageStoryBoard", bundle: nil)
        let packageListViewController = storyboard.instantiateViewController(withIdentifier: "PackageListViewControllerID") as! PackageListViewController
        
        let packageListObj = PackageGroupObject.init(value: self.getPackageGroupObj!.getPackageGroupObject_category[indexPath.row])
        

        packageListViewController.params_categorycode = packageListObj!.packageGroupObject_code
            packageListViewController.params_systemtype = self.getPackageGroupObj!.getPackageGroupObject_systemType
            packageListViewController.params_paymentmode = self.getPackageGroupObj!.getPackageGroupObject_paymentMethod
            packageListViewController.params_networktype = self.getPackageGroupObj!.getPackageGroupObject_networkType
        packageListViewController.phoneNumber = self.phoneNumber!
        navigationController?.pushViewController(packageListViewController, animated: true)
    }
    
    
}

extension PackageCagtegoryViewController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if self.getPackageGroupObj != nil
        {
            return (self.getPackageGroupObj?.getPackageGroupObject_category.count)!
        }
        else
        {
            return 0
        }

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
                let cellIdentifier = "PackageCagtegoryTableViewCellIdentifier"
                let cell:PackageCagtegoryTableViewCell = self.packageTableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! PackageCagtegoryTableViewCell
        
                let packageListObj = PackageGroupObject.init(value: self.getPackageGroupObj!.getPackageGroupObject_category[indexPath.row])
        
                cell .setTitleCell(obj: packageListObj!)

                return cell
    }
}







