//
//  CheckVerionObject.swift
//  RomApp
//
//  Created by Administrator on 1/24/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit
import SwiftyJSON

class CheckVerionObject: NSObject {


    let checkVerionObject_mobilenumber: String
    let checkVerionObject_status: String
    let checkVerionObject_response_code: String
    let checkVerionObject_currentversion: String
    let checkVerionObject_response_message: String
    let checkVerionObject_downloadurl: String
    let checkVerionObject_message: String

    
    
    init(checkVerionObject_mobilenumber: String,
          checkVerionObject_status: String,
          checkVerionObject_response_code: String,
          checkVerionObject_currentversion: String,
          checkVerionObject_response_message: String,
          checkVerionObject_downloadurl: String,
          checkVerionObject_message: String)
    {
        self.checkVerionObject_mobilenumber = checkVerionObject_mobilenumber
        self.checkVerionObject_status = checkVerionObject_status
        self.checkVerionObject_response_code = checkVerionObject_response_code
        self.checkVerionObject_currentversion = checkVerionObject_currentversion
        self.checkVerionObject_response_message = checkVerionObject_response_message
        self.checkVerionObject_downloadurl = checkVerionObject_downloadurl
        self.checkVerionObject_message = checkVerionObject_message
    }
    
    required convenience init?(value:JSON) {
        
        self.init(checkVerionObject_mobilenumber:value["mobilenumber"].stringValue,
            checkVerionObject_status:value["status"].stringValue,
            checkVerionObject_response_code:value["response_code"].stringValue,
            checkVerionObject_currentversion:value["currentversion"].stringValue,
            checkVerionObject_response_message:value["response_message"].stringValue,
            checkVerionObject_downloadurl:value["downloadurl"].stringValue,
            checkVerionObject_message:value["message"].stringValue)
        
    }

    
}
