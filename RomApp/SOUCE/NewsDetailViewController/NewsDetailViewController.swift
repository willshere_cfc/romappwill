//
//  NewsDetailViewController.swift
//  RomApp
//
//  Created by Administrator on 1/2/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit
import JGProgressHUD

class NewsDetailViewController: UIViewController ,UIScrollViewDelegate{

    @IBOutlet weak var webview: UIWebView!
    
    var flagScrollWebView : Bool = true
    
    var newsListObj : NewsListObject?
    
    @IBOutlet weak var activityProgess: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.requestServiceUpdateNews()

        self.activityProgess.startAnimating()
        
        self.webview.scrollView.delegate = self
        webview.loadRequest(URLRequest(url: URL(string: self.newsListObj!.newsListObject_newsurl)!))
        // Do any additional setup after loading the view.
    }
    
    //MARK: - Service
    func requestServiceUpdateNews()
    {
        let defult = UserDefaults.standard
        let dataDF =  defult.value(forKey: "user_phonenumber")
        
        
        let parameters: [String: Dictionary] =
            [
                "data":
                    [
                        "mobile_no": dataDF!,
                        "news_id":self.newsListObj!.newsListObject_id
                    ]
        ]
        
        print("param => \(parameters)")
        
        let strURL = "http://stg-etopupservices.ais.co.th/dev-ROMMobileServiceAPI/ROMService.svc/UpdateReadNews/"
        
       
        AFWrapper.requestPOSTURL(strURL, params: parameters as [String : AnyObject]?, headers: nil, success: { (JSONResponse) ->  Void in
            
            print("Getnews \(JSONResponse)")
            
           
            
            if JSONResponse.count != 0
            {
                // Post notification
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NotificationUpdateBagdeNews"), object: nil)
                
                // Stop listening notification
                NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "NotificationUpdateBagdeNews"), object: nil);
            }
            
            
        }) { (error) -> Void in
            print(error)
           
            
        }
        
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
   
        if(scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height))
        {
            if flagScrollWebView
            {
               NSLog("BOTTOM REACHED");
                flagScrollWebView = false
            }
           
            
        }
        if(scrollView.contentOffset.y <= 0.0)
        {
            NSLog("TOP REACHED");
        }
        
    }

    // MARK: IBAction
    
    @IBAction func leftNavbarDidSelect(_ sender: Any)
    {
        _ = navigationController?.popViewController(animated: true)
    }
    @IBAction func backToMainDidSelect(_ sender: Any)
    {
        _ = navigationController?.popToRootViewController(animated: true)
    }

}

extension NewsDetailViewController : UIWebViewDelegate {
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
        self.activityProgess.stopAnimating()
    }
    
}


