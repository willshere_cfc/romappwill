//
//  MenuViewController.swift
//  RomApp
//
//  Created by Administrator on 12/28/16.
//  Copyright © 2016 Willshere. All rights reserved.
//

import UIKit

enum MenuHemberger: Int {
    case main = 0
    case allService
    case history
    case report
    case contact
    case news
    case profile
    case about
    case logout
}

enum MenuTear: String {
    case T1T2
    case T3
    
}

protocol MenuHembergerProtocol : class {
    func changeViewController(_ menu: MenuHemberger)
}

class MenuViewController: UIViewController, MenuHembergerProtocol {

    var menuTear:MenuTear!
    // MARK: var and
    var mainViewController: UIViewController!
    var allServiceViewController: UINavigationController!
    var aboutViewController: UINavigationController!
    var profileViewController: UINavigationController!
    var contactViewController: UINavigationController!
    var reportListViewController: UINavigationController!
    var historyListViewController: UINavigationController!
    var newsViewController: UINavigationController!
    var newsi5ViewController: UINavigationController!
    var loginViewController: UINavigationController!
   
    var menus = ["หน้าหลัก", "บริการทั้งหมด", "ประวัติการทำรายการ", "รายงานสรุป", "รายชื่อผู้ติดต่อ", "ข่าวสาร", "ข้อมูลส่วนตัว", "เกี่ยวกับ", "ออกจากระบบ", "NonMenu"]
    
    var menusIcon = ["home_ic", "all_service_ic", "his_side_ic", "report_side_ic", "contact_ic_nav", "news_ic", "profile_ic", "about_ic", "logout", "NonMenu"]
    
    
    @IBOutlet weak var menuTableView: UITableView!
    
    //MARK: viewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let allServiceViewController = storyboard?.instantiateViewController(withIdentifier: "AllServiceViewControllerID") as! AllServiceViewController
        if self.menuTear == .T1T2
        {
            allServiceViewController .setSelectType = .T1T2
        }
        else
        {
            allServiceViewController .setSelectType = .T3
        }
        self.allServiceViewController = UINavigationController(rootViewController: allServiceViewController)
        self.allServiceViewController.setNavigationBarHidden(true, animated: false)
        
        let aboutViewController = storyboard?.instantiateViewController(withIdentifier: "AboutViewControllerID") as! AboutViewController
        self.aboutViewController = UINavigationController(rootViewController: aboutViewController)
        self.aboutViewController.setNavigationBarHidden(true, animated: false)
        
        let profileViewController = storyboard?.instantiateViewController(withIdentifier: "ProfileViewControllerID") as! ProfileViewController
        profileViewController.delegate = self
        self.profileViewController = UINavigationController(rootViewController: profileViewController)
        self.profileViewController.setNavigationBarHidden(true, animated: false)
        
        
        let contactViewController = storyboard?.instantiateViewController(withIdentifier: "ContactViewControllerID") as! ContactViewController
        contactViewController.setSelectType = .HambergerType
        self.contactViewController = UINavigationController(rootViewController: contactViewController)
        self.contactViewController.setNavigationBarHidden(true, animated: false)
        
        
        let reportStoryboard = UIStoryboard(name: "ReportStoryboard", bundle: nil)
        let reportListViewController = reportStoryboard.instantiateViewController(withIdentifier: "ReportListViewControllerID") as! ReportListViewController
        reportListViewController.setSelectType = .HambergerType
        self.reportListViewController = UINavigationController(rootViewController: reportListViewController)
        self.reportListViewController.setNavigationBarHidden(true, animated: false)
        
        let historyStoryboard = UIStoryboard(name: "HistoryStoryboard", bundle: nil)
        let historyListViewController = historyStoryboard.instantiateViewController(withIdentifier: "HistoryListViewControllerID") as! HistoryListViewController
        historyListViewController.setSelectType = .HambergerType
        self.historyListViewController = UINavigationController(rootViewController: historyListViewController)
        self.historyListViewController.setNavigationBarHidden(true, animated: false)
        
        
        let newsStoryboard = UIStoryboard(name: "NewsStoryBoard", bundle: nil)
        if UIDevice().screenType == .iPhone5 || UIDevice().screenType == .iPhone4
        {
           let newsViewController = newsStoryboard.instantiateViewController(withIdentifier: "Newsi5ViewControllerID") as! NewsViewController
            newsViewController.setSelectType = .HambergerType
            self.newsViewController = UINavigationController(rootViewController: newsViewController)
            self.newsViewController.setNavigationBarHidden(true, animated: false)
        }
        else
        {
           let newsViewController = newsStoryboard.instantiateViewController(withIdentifier: "NewsViewControllerID") as! NewsViewController
            newsViewController.setSelectType = .HambergerType
            self.newsViewController = UINavigationController(rootViewController: newsViewController)
            self.newsViewController.setNavigationBarHidden(true, animated: false)
        }
  
        
        let loginStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let loginViewController = loginStoryboard.instantiateViewController(withIdentifier: "LoginViewControllerID") as! LoginViewController
        self.loginViewController = UINavigationController(rootViewController: loginViewController)
        self.loginViewController.setNavigationBarHidden(true, animated: false)
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK: changeViewController
    func changeViewController(_ menu: MenuHemberger) {
        switch menu {
            
        case .main:
            self.slideMenuController()?.changeMainViewController(self.mainViewController, close: true)
        case .allService:
            self.slideMenuController()?.changeMainViewController(self.allServiceViewController, close: true)
        case .history:
                self.slideMenuController()?.changeMainViewController(self.historyListViewController, close: true)
        case .report:
            self.slideMenuController()?.changeMainViewController(self.reportListViewController, close: true)
        case .contact:
            self.slideMenuController()?.changeMainViewController(self.contactViewController, close: true)
        case .news:
            self.slideMenuController()?.changeMainViewController(self.newsViewController, close: true)
        case .profile:
            self.slideMenuController()?.changeMainViewController(self.profileViewController, close: true)
        case .about:
            self.slideMenuController()?.changeMainViewController(self.aboutViewController, close: true)
        case .logout:
            self.slideMenuController()?.changeMainViewController(self.loginViewController, close: true)

        }
    }
    
   
}


//MARK: - EXTENSION
//MARK: - TABLEVIEW DELEGATE

extension MenuViewController : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if let menu = MenuHemberger(rawValue: indexPath.row)
        {
            switch menu
            {
            case .main, .allService, .history, .report, .contact, .news, .profile, .about, .logout:
                return MenuTableViewCell.height()
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if let menu = MenuHemberger(rawValue: indexPath.row)
        {
            self.changeViewController(menu)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        if self.menuTableView == scrollView
        {
            
        }
    }
}

extension MenuViewController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return menus.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if let menu = MenuHemberger(rawValue: indexPath.row)
        {
            switch menu
            {
                case .main, .allService, .history, .report, .contact, .news, .profile, .about, .logout:
                
                    let cellIdentifier = "MenuTableViewCellIdenttifier"
                    let cell:MenuTableViewCell = self.menuTableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! MenuTableViewCell
                
                    cell .setTitleCell(title: menus[indexPath.row])
                    cell .setImageIcon(imgName: menusIcon[indexPath.row])
                    
                    return cell
            }
        }
        return UITableViewCell()
    }
}



















