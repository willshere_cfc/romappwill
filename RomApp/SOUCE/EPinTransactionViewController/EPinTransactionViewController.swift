//
//  EPinTransactionViewController.swift
//  RomApp
//
//  Created by Administrator on 1/4/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit

class EPinTransactionViewController: UIViewController {
    
    var epinPrice : NSString = "50"
    var epinPhoneNumber :NSString = ""
    var epinObject : EPinObject?

    @IBOutlet weak var lbl_epinPhoneNumber: UILabel!
    @IBOutlet weak var lbl_epinPrice: UILabel!
    @IBOutlet weak var lbl_transactionDate: UILabel!
    @IBOutlet weak var lbl_transactionID: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        lbl_epinPhoneNumber.text = self.epinPhoneNumber as String
        lbl_epinPrice.text = self.epinPrice as String
        lbl_transactionID.text = self.epinObject?.epinObject_transaction_id
        lbl_transactionDate.text = self.epinObject?.epinObject_transaction_date
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        if(epinObject != nil){
            
        }
    }
    
   // MARK : - IBAction
    
    @IBAction func confirmDidSelect(_ sender: Any)
    {
         _ = navigationController?.popToRootViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
