//
//  NewsTableViewCell.swift
//  RomApp
//
//  Created by Administrator on 1/1/17.
//  Copyright © 2017 Willshere. All rights reserved.
//

import UIKit

class NewsTableViewCell: UITableViewCell {

    @IBOutlet weak var img_News: UIImageView!
    @IBOutlet weak var lbl_Title: UILabel!
    
    @IBOutlet weak var img_cardBG: CustomizableImageNewsList!
    @IBOutlet weak var img_Next: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    open func settingNewsCellWithObject(newsObj:NewsListObject){
        
       
        self.lbl_Title?.text = newsObj.newsListObject_title
        
     
        self.img_News.image = UIImage(data: Data(base64Encoded: newsObj.newsListObject_thumbimage)!)

        if newsObj.newsListObject_isread == "1"
        {
            self.img_Next.isHidden = true
            self.img_cardBG.image = UIImage(named: "card_noti_read")
        }
        else
        {
            self.img_Next.isHidden = false
            self.img_cardBG.image = UIImage(named: "card_noti")
        }

    }

}
